# Master (will become release 2.11)

- ...

# Release 2.10

- The class `ConvexFunctional` has lost its method `evaluate`.
  Only use `operator()` in the future.

# Release 2.9

- Previously, the `TNNMGStep` class tacitly created the `Linearization` object.
  Now, a `Linearization` object can be alternatively given to `TNNMGStep`
  by means of a new `setLinearization` method.  That way, the options of a
  linearization class can be set and controlled before using it in `TNNMGStep`.


## Deprecations and removals

- ...
