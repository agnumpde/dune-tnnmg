#include <config.h>

#define DIMENSION 2
#define ALUGRID 1
//#define UGGRID 1
#define EMBEDDED_PYTHON 1
#undef EMBEDDED_PYTHON

#define FE_VERBOSE

// disable embedded python if python was not found
#ifndef HAVE_PYTHON
    #undef EMBEDDED_PYTHON
#endif

#if EMBEDDED_PYTHON
    #include <Python.h>
#endif

#include <fstream>
#include <iostream>
#include <iostream>
#include <vector>

#include <tr1/memory>

// dune includes ******************************************************
#ifdef UGGRID
    #include <dune/grid/uggrid.hh>
    #include <dune/grid/io/file/dgfparser/dgfug.hh>
#endif

#ifdef ALUGRID
    #include <dune/grid/alugrid.hh>
    #include <dune/grid/io/file/dgfparser/dgfalu.hh>
#endif

#include <dune/common/configparser.hh>
#include <dune/common/shared_ptr.hh>

#include <dune/istl/bcrsmatrix.hh>

#include <dune/grid/common/genericreferenceelements.hh>

// dune-fufem includes ******************************************************
#include <dune/fufem/boundarypatch.hh>
#include <dune/fufem/prolongboundarypatch.hh>

#if EMBEDDED_PYTHON
    #include <dune/fufem/functions/pythonfunction.hh>
#endif
#include <dune/fufem/functions/functions.hh>
#include <dune/fufem/functions/basisgridfunction.hh>

#include <dune/fufem/functionspacebases/p1nodalbasis.hh>

#include <dune/fufem/functiontools/namedfunctionmap.hh>
#include <dune/fufem/functiontools/boundarydofs.hh>

#include <dune/fufem/assemblers/operatorassembler.hh>
#include <dune/fufem/assemblers/functionalassembler.hh>
#include <dune/fufem/assemblers/transferoperatorassembler.hh>

#include <dune/fufem/assemblers/localassemblers/laplaceassembler.hh>
#include <dune/fufem/assemblers/localassemblers/massassembler.hh>
#include <dune/fufem/assemblers/localassemblers/lumpedmassassembler.hh>
#include <dune/fufem/assemblers/localassemblers/l2functionalassembler.hh>

#if HAVE_AMIRAMESH
    #include <dune/fufem/functiontools/amirameshbasiswriter.hh>
#endif
#include <dune/fufem/functiontools/vtkbasiswriter.hh>

// dune-solvers includes ***************************************************
#include <dune/solvers/norms/norm.hh>
#include <dune/solvers/norms/sumnorm.hh>
#include <dune/solvers/norms/energynorm.hh>
#include <dune/solvers/norms/diagnorm.hh>
#include <dune/solvers/norms/fullnorm.hh>
#include <dune/solvers/norms/blocknorm.hh>

// dune-tnnmg includes *******************************************************
#include <dune/tnnmg/problem-classes/blocknonlineargsproblem.hh>
#include <dune/tnnmg/problem-classes/nonlinearity.hh>
#include <dune/tnnmg/nonlinearities/zerononlinearity.hh>
#include <dune/tnnmg/nonlinearities/shiftednonlinearity.hh>
#include <dune/tnnmg/iterationsteps/genericnonlinearjacobi.hh>
#include <dune/tnnmg/solvers/scalartnnmg.hh>

#include "laplace.hh"

template <int dim, int N>
class ConstantFunction :
    public Dune::FunctionBase<double,double,dim,N>
{
    public:
        ConstantFunction(double c):
            c_(c)
        {}

        double eval(int comp, const Dune::FieldVector<double,dim>& x) const
        {
            return c_;
        }

        void evalall(const Dune::FieldVector<double,dim>& x, Dune::FieldVector<double,N>& y) const
        {
            y = eval(0,x);
        }

        double c_;
};

template <int dim, int N>
class AbsFunction :
    public Dune::FunctionBase<double,double,dim,N>
{
    public:
        AbsFunction()
        {}

        double eval(int comp, const Dune::FieldVector<double,dim>& x) const
        {
            return fabs(x[0]);
        }

        void evalall(const Dune::FieldVector<double,dim>& x, Dune::FieldVector<double,N>& y) const
        {
            y = eval(0,x);
        }
};

int main (int argc, char *argv[]) try
{
#ifdef EMBEDDED_PYTHON
    EmbeddedPython::start();
#endif

    Dune::ConfigParser parset;

    // parse command line once to obtain a possible parset argument
    // parse parameter file
    // parse command line a second time in order to allow overwriting parameters
    parset.parseCmd(argc, argv);
    parset.parseFile(parset.get("file", "adaptive_laplace.parset"));
    parset.parseCmd(argc, argv);

    // The grid dimension
    const int dim = DIMENSION;

    // define grid type
#ifdef UGGRID
    typedef Dune::UGGrid<dim> GridType;
#endif
#ifdef ALUGRID
    typedef Dune::ALUSimplexGrid<dim,dim> GridType;
#endif

    typedef NamedFunctionMap<GridType::Codim<0>::Geometry::GlobalCoordinate,Dune::FieldVector<double,1> > Functions;
    typedef Functions::Function Function;

    Functions functions;
    functions["one"] = new ConstantFunction<dim,1>(1.0);
    functions["zero"] = new ConstantFunction<dim,1>(0.0);
    functions["abs"] = new AbsFunction<dim,1>;

    // Generate the grid
    Dune::GridPtr<GridType> gridptr;
    if (dim==2)
        gridptr=Dune::GridPtr<GridType>(parset.get("grid.dgffile2d", "coarse.dgf").c_str());
    else if (dim==3)
        gridptr=Dune::GridPtr<GridType>(parset.get("grid.dgffile3d", "coarse.dgf").c_str());

    GridType& grid = *gridptr;

// grid specific settings
#ifdef UGGRID
    grid.setRefinementType(GridType::LOCAL);
    grid.setClosureType(GridType::NONE);
#endif
#ifdef ALUGRID
    if (parset.hasKey("grid.restore"))
    {
        double time;
        grid.readGrid<Dune::xdr>(parset.get<std::string>("grid.backupfile"), time);
        std::cout << "Grid restored. Time was " << time << std::endl;
    }
    else
#endif
    {
        //initial refinement of grid
        for (int i=0; i<parset.get("grid.refine", 0) or i<parset.get("laplace.refinement.minlevel",0); ++i)
            grid.globalRefine(1);
        std::cout << "Grid refined." << std::endl;
    }

#ifdef EMBEDDED_PYTHON
    std::string importDictName = formatString(std::string("importAsDuneFunction%1dD"), dim);
    if (parset.hasKey("python.inline"))
    {
        EmbeddedPython::run(parset.get("python.inline", ""));
        EmbeddedPython::importFunctionsFromModule(functions, "__main__", importDictName);
    }
    if (parset.hasKey("python.module"))
        EmbeddedPython::importFunctionsFromModule(functions, parset.get("python.module", "laplace"), importDictName);
#endif

    LaplaceProblem<GridType> laplaceProblem(grid, parset.sub("laplace"), functions);

    bool refined;

    do
    {
        laplaceProblem.assemble();
        laplaceProblem.solve();
        refined = laplaceProblem.adapt();
    }
    while(refined);


#ifdef EMBEDDED_PYTHON
    EmbeddedPython::stop();
#endif


    // delete created functions
    Functions::iterator it = functions.begin();
    Functions::iterator end = functions.end();
    for(; it!=end; ++it)
        delete it->second;

    return 0;
}

catch (Dune::Exception e)
{
    std::cout << e << std::endl;
}

