#include <config.h>

// Forcefully disable ParMetis
// dune-pdelab includes ParMetis headers which do weird things with MPI.
// As we are not using ParMetis anyway let's just disable it.
// The problem will disappear once we have the code ported away from dune-pdelab.
#define HAVE_PARMETIS 0

#include <array>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/parametertree.hh>
#include <dune/common/hybridutilities.hh>

#include <dune/grid/yaspgrid.hh>

#include <dune/grid/io/file/vtk.hh>

#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/bvector.hh>

#include<dune/pdelab/finiteelementmap/qkfem.hh>
#include <dune/pdelab/function/callableadapter.hh>
#include<dune/pdelab/localoperator/linearelasticity.hh>
#include<dune/pdelab/localoperator/l2volumefunctional.hh>
#include<dune/pdelab/gridfunctionspace/vectorgridfunctionspace.hh>
#include<dune/pdelab/gridfunctionspace/vtk.hh>
#include<dune/pdelab/constraints/conforming.hh>
#include<dune/pdelab/backend/istl.hh>
#include<dune/pdelab/backend/simple.hh>
#include<dune/pdelab/gridoperator/gridoperator.hh>

#include <dune/solvers/solvers/loopsolver.hh>
#include <dune/solvers/norms/energynorm.hh>
#include <dune/solvers/iterationsteps/projectedblockgsstep.hh>
#include <dune/solvers/iterationsteps/truncatedblockgsstep.hh>
#include <dune/solvers/iterationsteps/multigridstep.hh>
#include <dune/solvers/transferoperators/truncateddensemgtransfer.hh>

#include <dune/tnnmg/functionals/boxconstrainedquadraticfunctional.hh>
#include <dune/tnnmg/functionals/sumfunctional.hh>
#include <dune/tnnmg/functionals/trescafrictionfunctional.hh>
#include <dune/tnnmg/iterationsteps/nonlineargsstep.hh>
#include <dune/tnnmg/iterationsteps/tnnmgstep.hh>
#include <dune/tnnmg/problem-classes/bisection.hh>
#include <dune/tnnmg/projections/obstacledefectprojection.hh>
#include <dune/tnnmg/localsolvers/scalarobstaclesolver.hh>


using namespace Dune;

// Class that stores the material parameters of a St.Venant-Kirchhoff material
template <class GridView>
class StVenantKirchhoffParameters
  : public PDELab::LinearElasticityParameterInterface<
                     PDELab::LinearElasticityParameterTraits<GridView, double>,
                     StVenantKirchhoffParameters<GridView> >
{
public:
  typedef PDELab::LinearElasticityParameterTraits<GridView, double> Traits;

  StVenantKirchhoffParameters(typename Traits::RangeFieldType l,
                              typename Traits::RangeFieldType m) :
    lambda_(l), mu_(m)
  {}

  typename Traits::RangeFieldType
  lambda (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
  {
    return lambda_;
  }

  typename Traits::RangeFieldType
  mu (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
  {
    return mu_;
  }

private:
  typename Traits::RangeFieldType lambda_;
  typename Traits::RangeFieldType mu_;
};

struct TrescaFrictionDefectProjection
{
  template<class Functional, class Vector>
  constexpr void operator()(const Functional& f, const Vector& x, Vector& v)
  {
    // f is a SumFunctional, but only the first addend has lowerObstacle/upperObstacle methods
    const auto& obstacleFunctional = std::get<0>(f.functions());

    TNNMG::ObstacleDefectProjection()(obstacleFunctional,x,v);
  }
};

/** \brief The constrained linearization of the TrescaFrictionFunctional class
 */
template<class F, class BV>
class TrescaFrictionFunctionalConstrainedLinearization
{
  const static int dim = F::Vector::value_type::dimension;

  static Dune::FieldVector<double,dim-1> tangentialPart(const Dune::FieldVector<double,dim>& v)
  {
    Dune::FieldVector<double,dim-1> result;
    std::copy(v.begin()+1,v.end(),result.begin());
    return result;
  }

public:
  using Matrix = typename std::tuple_element_t<0,typename F::Functions>::Matrix;
    using Vector = typename F::Vector;
    using BitVector = BV;
    using ConstrainedMatrix = Matrix;
    using ConstrainedVector = Vector;
    using ConstrainedBitVector = BitVector;


    TrescaFrictionFunctionalConstrainedLinearization(const F& f, const BitVector& ignore) :
        f_(f),
        ignore_(&ignore),
        truncationTolerance_(1e-10)
    {}

    /** \brief Specify what degrees of freedom to ignore
     */
    void setIgnore(const BitVector& ignore)
    {
      ignore_ = &ignore;
    }

    void bind(const Vector& x)
    {
    constexpr double tolerance = 1e-15;

    ////////////////////////////////////////////////////
    // Compute first derivate of the functional
    ////////////////////////////////////////////////////

    // Quadratic part
    negativeGradient_ = derivative(std::get<0>(f_.functions()))(x);

    // The actual friction functional functional
    auto frictionFunctional = std::get<1>(f_.functions());
    const auto& coefficients = frictionFunctional.coefficients();

    for (size_t row=0; row<x.size(); ++row)
    {
      double tangentialNorm = tangentialPart(x[row]).two_norm();

      // Do nothing if we are at a nondifferentiable point
      if (tangentialNorm < tolerance)
        continue;

      for (int j=1; j<dim; j++)
        negativeGradient_[row][j] += coefficients[row][0] * x[row][j] / tangentialNorm;
    }

    negativeGradient_ *= -1;

    ////////////////////////////////////////////////////
    // Compute second derivative of the functional
    ////////////////////////////////////////////////////

    // Quadratic part
    hessian_ = derivative(derivative(std::get<0>(f_.functions())))(x);

    // Friction functional
    for (size_t row=0; row<x.size(); ++row)
    {
      double tangentialNorm = tangentialPart(x[row]).two_norm();

      // Do nothing if we are at the nondifferentiable point
        if (tangentialNorm < tolerance)
          continue;

      for (int i=1; i<dim; i++)
        for (int j=1; j<dim; j++)
          hessian_[row][row][i][j] += coefficients[row][0] * (- x[row][i] * x[row][j] / (tangentialNorm*tangentialNorm*tangentialNorm) + (i==j) / tangentialNorm);
    }

    ////////////////////////////////////////////////////
    // Determine which dofs to truncate
    ////////////////////////////////////////////////////

    // f is a SumFunctional, but only the first addend has lowerObstacle/upperObstacle methods
    const auto& obstacleFunctional = std::get<0>(f_.functions());

    // The ignore_ field contains the Dirichlet dofs
    truncationFlags_ = *ignore_;

    // determine which components to truncate
    for (std::size_t i=0; i<x.size(); ++i)
    {
      // The Tresca friction part
      double tangentialNorm = tangentialPart(x[i]).two_norm();

      if (coefficients[i][0] > tolerance and tangentialNorm < tolerance)
        truncationFlags_[i].set();

      // The obstacle part
      for (int j=0; j<dim; j++)
        if (x[i][j] >= obstacleFunctional.upperObstacle()[i][j]-tolerance
          or x[i][j] <= obstacleFunctional.lowerObstacle()[i][j]+tolerance)
          truncationFlags_[i][j] = true;
    }

    /////////////////////////////////////////////////////
    // Do the actual truncation
    /////////////////////////////////////////////////////

    for (std::size_t i=0; i<x.size(); ++i)
    {
      auto it = hessian_[i].begin();
      auto end = hessian_[i].end();
      for (; it!=end; ++it)
        for (std::size_t j=0; j<x[i].size(); ++j)
          for (std::size_t k=0; k<x[i].size(); ++k)
            if (truncationFlags_[i][j] || truncationFlags_[it.index()][k])
              (*it)[j][k] = 0;

      for (std::size_t j=0; j<x[i].size(); ++j)
        if (truncationFlags_[i][j])
        {
          negativeGradient_[i][j] = 0;
          //hessian_[i][i][j][j] = 1;
        }
    }

    }

    void extendCorrection(ConstrainedVector& cv, Vector& v) const
    {
        for(std::size_t i=0; i<v.size(); ++i)
          for (std::size_t j=0; j<v[i].size(); ++j)
          {
            if (truncationFlags_[i][j])
                v[i][j] = 0;
            else
                v[i][j] = cv[i][j];
          }
    }

    const BitVector& truncated() const
    {
        return truncationFlags_;
    }

    const auto& negativeGradient() const
    {
        return negativeGradient_;
    }

    const auto& hessian() const
    {
        return hessian_;
    }

private:
    const F& f_;
    const BitVector* ignore_;

    double truncationTolerance_;

    Vector negativeGradient_;
    Matrix hessian_;
    BitVector truncationFlags_;
};

template <class ElasticityFunctional, class TrescaFunctional>
class ScalarSumFunctional
{
public:
  ScalarSumFunctional(const ElasticityFunctional& elasticityFunctional,
                      const TrescaFunctional& trescaFunctional)
  : elasticityFunctional_(elasticityFunctional),
    trescaFunctional_(trescaFunctional)
    {}

  Solvers::Interval<double> domain() const
  {
    Solvers::Interval<double> d = {elasticityFunctional_.lowerObstacle(),
                                   elasticityFunctional_.upperObstacle()};

    return d;
  }

  Dune::Solvers::Interval<double> subDifferential(double z) const
  {
    double tolerance = 1e-14;

    // quadratic part
    Dune::Solvers::Interval<double> dJ = z*elasticityFunctional_.quadraticPart() - elasticityFunctional_.linearPart();

    // obstacle part
    if (z < elasticityFunctional_.lowerObstacle())
      dJ = -std::numeric_limits<double>::max();

    if (z <= tolerance + elasticityFunctional_.lowerObstacle())
      dJ[0] = -std::numeric_limits<double>::max();

    if (z >= -tolerance + elasticityFunctional_.upperObstacle())
      dJ[1] = std::numeric_limits<double>::max();

    if (z > elasticityFunctional_.upperObstacle())
      dJ = std::numeric_limits<double>::max();

    dJ += trescaFunctional_.subDifferential(z);

    return dJ;
  }

  const ElasticityFunctional& elasticityFunctional_;
  const TrescaFunctional& trescaFunctional_;

};

int main (int argc, char *argv[]) try
{
    MPIHelper::instance(argc, argv);

    // Solver settings
    int numLevels        = 3;
    int numIt            = 100;
    int nu1              = 3;    // number of pre-smoothing steps
    int nu2              = 3;    // number of post-smoothing steps
    double tolerance     = 1e-7;
    double baseTolerance = 1e-8;

    // read problem settings
    double lambda = 1e7;
    double mu     = 6.5e6;


    // /////////////////////////////
    //   Generate the grid
    // /////////////////////////////

    // The grid dimension
    const int dim = 2;

    typedef YaspGrid<dim> GridType;
    typedef GridType::LeafGridView GridView;

    FieldVector<double,dim> h(1);  // bounding box
    std::array<int,dim> n;
    std::fill(n.begin(), n.end(), 10);
    GridType grid(h,n);

    // refine uniformly until minlevel
    for (int i=0; i<numLevels-1; i++)
        grid.globalRefine(1);

    // Mark Dirichlet and contact nodes on the leaf grid view
    using BitVector = BitSetVector<dim>;
    BitVector dirichletNodes(grid.size(dim));
    BitSetVector<1> contactNodes(grid.size(dim));

    auto gridView = grid.leafGridView();
    for (const auto& vertex : vertices(gridView))
    {
        size_t vIdx = gridView.indexSet().index(vertex);
        dirichletNodes[vIdx] = (vertex.geometry().corner(0)[0] > 0.999);
        contactNodes[vIdx]   = (vertex.geometry().corner(0)[0] < 0.001);
    }

    ////////////////////////////////////////////////////////////////
    //  Assemble the stiffness matrix
    ////////////////////////////////////////////////////////////////

    // set up vector-valued finite element space
    typedef PDELab::QkLocalFiniteElementMap<GridView,double,double,1> FEM;
    FEM fem(grid.leafGridView());

    typedef PDELab::VectorGridFunctionSpace<GridView,
                                            FEM,
                                            dim,
                                            PDELab::ISTL::VectorBackend<PDELab::ISTL::Blocking::fixed>,
                                            PDELab::ISTL::VectorBackend<>,
                                            PDELab::ConformingDirichletConstraints,
                                            PDELab::EntityBlockedOrderingTag,
                                            PDELab::DefaultLeafOrderingTag
                                           > GFS;
    GFS gfs(grid.leafGridView(), fem);
    gfs.name("displacement");

    // create the model describing our problem
    StVenantKirchhoffParameters<GridView> model(lambda, mu);

    // make grid operator
    PDELab::LinearElasticity<StVenantKirchhoffParameters<GridView> > lop(model);

    typedef Dune::PDELab::ISTL::BCRSMatrixBackend<> MatrixBackend;
    MatrixBackend matrixBackend(9); // Estimate of the number of entries per matrix row

    typedef PDELab::GridOperator<GFS,
                                 GFS,
                                 PDELab::LinearElasticity<StVenantKirchhoffParameters<GridView> >,
                                 MatrixBackend,
                                 double,double,double> GridOperator;

    GridOperator gridOperator(gfs, gfs, lop, matrixBackend);

    typedef GridOperator::Traits::Domain V;
    typedef GridOperator::Jacobian M;
    using MatrixType = M::Container;   //  BCRSMatrix<FieldMatrix<double, dim, dim> >
    using VectorType = V::Container;   //  BlockVector<FieldVector<double,dim> >

    // Dummy coefficient vector
    V x0(gfs);
    x0 = 0.0;

    // represent operator as a matrix
    MatrixType stiffnessMatrix;
    M m(gridOperator, stiffnessMatrix);
    m = 0.0;

    gridOperator.jacobian(x0, m);

    // load vector
    VectorType rhs(grid.size(dim));
    rhs = 0;

    //////////////////////////////////////////////////////////////////////////////////
    //   Compute a vector that contains a the integrals over each shape function
    //////////////////////////////////////////////////////////////////////////////////

    // BUG \bug What we really need here is the integral over the contact boundary,
    // NOT the integral over the entire domain!!

    // { trescafriction_integrate_over_shapefunctions_begin }
    // set up scalar-valued finite element space
    typedef PDELab::GridFunctionSpace<typename GridType::LeafGridView,
                                      FEM,
                                      PDELab::ConformingDirichletConstraints,
                                      PDELab::Simple::VectorBackend<> > ScalarGFS;
    ScalarGFS scalarGFS(gridView,fem);

    auto constantOne = PDELab::makeGridFunctionFromCallable(gridView, [](auto x){return 1.0;});

    typedef PDELab::L2VolumeFunctional<decltype(constantOne)> L2VolumeFunctionalOperator;
    int qorder = 2;
    L2VolumeFunctionalOperator l2VolumeFunctionalOperator(constantOne,qorder);

    typedef PDELab::GridOperator<ScalarGFS,ScalarGFS,
                                 L2VolumeFunctionalOperator,PDELab::Simple::MatrixBackend<>,
                                 double,double,double> GlobalL2VolumeFunctionalOperator;
    GlobalL2VolumeFunctionalOperator globalL2VolumeFunctionalOperator(scalarGFS,scalarGFS,l2VolumeFunctionalOperator);

    // I want these weights to be in a std::vector, but currently the solver only accepts BlockVector
    std::vector<double> nodalWeightsTmp;
    typename GlobalL2VolumeFunctionalOperator::Traits::Domain sfWeights(scalarGFS,nodalWeightsTmp);
    sfWeights = 0.0;

    typename GlobalL2VolumeFunctionalOperator::Traits::Domain zero(scalarGFS);
    zero = 0;
    globalL2VolumeFunctionalOperator.residual(zero,sfWeights);

    // I want these weights to be in a std::vector, but currently the solver only accepts BlockVector
    BlockVector<FieldVector<double,dim> > nodalWeights(nodalWeightsTmp.size());
    std::copy(nodalWeightsTmp.begin(), nodalWeightsTmp.end(), nodalWeights.begin());
    // { trescafriction_integrate_over_shapefunctions_end }

    // ////////////////////////////////////////////////////////////
    //    Create initial iterate
    // ////////////////////////////////////////////////////////////

    // Initial iterate
    VectorType x(grid.size(dim));
    x = 0;

    FieldVector<double,dim> dirichletValue(0);
    dirichletValue[0] = -0.1;

    for (size_t i=0; i<rhs.size(); i++)
        for (int j=0; j<dim; j++)
            if (dirichletNodes[i][j])
                x[i][j] = dirichletValue[j];



  //////////////////////////////
  //   Create a solver
  //////////////////////////////

  TruncatedBlockGSStep<MatrixType,VectorType> linearBaseSolverStep;

  EnergyNorm<MatrixType,VectorType> baseEnergyNorm(linearBaseSolverStep);
  auto linearBaseSolver = std::make_shared<::LoopSolver<VectorType> >(&linearBaseSolverStep,
                                                                      100,
                                                                      baseTolerance,
                                                                      &baseEnergyNorm,
                                                                      Solver::QUIET);

  auto smoother = std::make_shared<TruncatedBlockGSStep<MatrixType, VectorType, BitVector> >();

  auto linearMultigridStep = std::make_shared<Solvers::MultigridStep<MatrixType, VectorType> >();

  linearMultigridStep->setMGType(1, nu1, nu2);
  linearMultigridStep->setBaseSolver(linearBaseSolver);
  linearMultigridStep->setSmoother(smoother);

  linearMultigridStep->mgTransfer_.resize(grid.maxLevel());
  for (size_t i=0; i<linearMultigridStep->mgTransfer_.size(); i++)
  {
    // create transfer operator from level i to i+1
    auto transferOperator = std::make_shared<TruncatedDenseMGTransfer<VectorType> >();
    transferOperator->setup(grid, i, i+1);

    linearMultigridStep->mgTransfer_[i] = transferOperator;
  }

  // { contact_create_tnnmg_begin }
  VectorType lowerObstacle(x.size());
  VectorType upperObstacle(x.size());
  std::fill(lowerObstacle.begin(), lowerObstacle.end(), -std::numeric_limits<double>::max());
  std::fill(upperObstacle.begin(), upperObstacle.end(),  std::numeric_limits<double>::max());
  for (size_t i=0; i<lowerObstacle.size(); i++)
    if (contactNodes[i][0])
      lowerObstacle[i][0] = 0;
    else
      nodalWeights[i] = 0;  // No friction on non-contact nodes

  using ElasticityFunctional = TNNMG::BoxConstrainedQuadraticFunctional<MatrixType,VectorType,VectorType,VectorType,double>;
  auto elasticityFunctional = ElasticityFunctional(stiffnessMatrix, rhs, lowerObstacle, upperObstacle);

  using TrescaFrictionFunctional = TrescaFrictionFunctional<VectorType, double>;
  auto trescaFrictionFunctional = TrescaFrictionFunctional(nodalWeights);

  using Functional = TNNMG::SumFunctional<ElasticityFunctional,TrescaFrictionFunctional>;
  auto J = Functional(elasticityFunctional,trescaFrictionFunctional);

  auto bisectionSolver = [](auto& xi, const auto& restriction, const auto& ignoreI) {
      if (ignoreI)
        return;

      Bisection bisection;
      ScalarSumFunctional<decltype(std::get<0>(restriction.functions())),
                          decltype(std::get<1>(restriction.functions()))> sumFunctional(std::get<0>(restriction.functions()), std::get<1>(restriction.functions()));
      xi = 0;
      int bisectionSteps;
      xi = bisection.minimize(sumFunctional, xi, xi, bisectionSteps);
    };

  auto localSolver = TNNMG::gaussSeidelLocalSolver(bisectionSolver);

  using Linearization = TrescaFrictionFunctionalConstrainedLinearization<Functional, BitVector>;
  using DefectProjection = TrescaFrictionDefectProjection;
  using LineSearchSolver = decltype(bisectionSolver);

  using NonlinearSmoother = TNNMG::NonlinearGSStep<Functional, decltype(localSolver), BitVector>;
  auto nonlinearSmoother = std::make_shared<NonlinearSmoother>(J, x, localSolver);

  using Step = TNNMG::TNNMGStep<Functional,
                                BitVector,
                                Linearization,
                                DefectProjection,
                                LineSearchSolver>;

  auto step = std::make_shared<Step>(J, x, nonlinearSmoother, linearMultigridStep, 1, DefectProjection(), bisectionSolver);

  auto norm = std::make_shared<EnergyNorm<MatrixType, VectorType> >(stiffnessMatrix);
  ::LoopSolver<VectorType> solver(step, numIt, tolerance, norm, Solver::FULL);

  step->setIgnore(dirichletNodes);

  step->setPreSmoothingSteps(nu1);

  solver.addCriterion(
            [&](){
                return Dune::formatString("   % 12d", step->linearization().truncated().count());
            },
            "   truncated   ");

  double initialEnergy = J(x);
  solver.addCriterion(
            [&](){
                static double oldEnergy=initialEnergy;
                double currentEnergy = J(x);
                double decrease = currentEnergy - oldEnergy;
                oldEnergy = currentEnergy;
                return Dune::formatString("   % 12.5e", decrease);
            },
            "   decrease    ");

  solver.addCriterion(
            [&](){
                return Dune::formatString("   % 12.5e", step->lastDampingFactor());
            },
            "   step size     ");

  solver.addCriterion(
            [&](){
                return Dune::formatString("   % 12.5e", J(x));
            },
            "   energy      ");

    // ///////////////////////////
    //   Compute solution
    // ///////////////////////////

    solver.preprocess();

    solver.solve();

    // ///////////////////////////
    // Output result
    // ///////////////////////////

    VTKWriter<GridType::LeafGridView> vtkWriter(grid.leafGridView());
    V xV(gfs);
    PDELab::Backend::native(xV) = x;
    Dune::PDELab::addSolutionToVTKWriter(vtkWriter,gfs,xV);
    vtkWriter.write("trescafriction_result");

} catch (Exception& e)
{
    std::cout << e.what() << std::endl;
}
