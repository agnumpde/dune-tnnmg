#include <config.h>

// Forcefully disable ParMetis
// dune-pdelab includes ParMetis headers which do weird things with MPI.
// As we are not using ParMetis anyway let's just disable it.
// The problem will disappear once we have the code ported away from dune-pdelab.
#define HAVE_PARMETIS 0

#include <dune/common/parallel/mpihelper.hh>

#include <dune/grid/yaspgrid.hh>
#include <dune/grid/io/file/vtk.hh>

#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/bvector.hh>

#include <dune/functions/functionspacebases/lagrangebasis.hh>
#include <dune/functions/gridfunctions/discreteglobalbasisfunction.hh>

#include <dune/pdelab/finiteelementmap/qkfem.hh>
#include <dune/pdelab/localoperator/convectiondiffusionfem.hh>
#include <dune/pdelab/gridfunctionspace/dunefunctionsgridfunctionspace.hh>
#include <dune/pdelab/gridfunctionspace/vtk.hh>
#include <dune/pdelab/constraints/conforming.hh>
#include <dune/pdelab/backend/istl.hh>
#include <dune/pdelab/gridoperator/gridoperator.hh>

#include <dune/solvers/solvers/loopsolver.hh>
#include <dune/solvers/norms/energynorm.hh>
#include <dune/solvers/iterationsteps/multigridstep.hh>
#include <dune/solvers/iterationsteps/blockgssteps.hh>
#include <dune/solvers/transferoperators/truncateddensemgtransfer.hh>

#include <dune/tnnmg/functionals/quadraticfunctional.hh>
#include <dune/tnnmg/functionals/quadraticfunctionalconstrainedlinearization.hh>
#include <dune/tnnmg/iterationsteps/nonlineargsstep.hh>
#include <dune/tnnmg/iterationsteps/tnnmgstep.hh>
#include <dune/tnnmg/localsolvers/scalarquadraticsolver.hh>
#include <dune/tnnmg/projections/trivialdefectprojection.hh>

using namespace Dune;


// { problem_description_begin }
template <class GridView, class RangeType>
class PoissonProblem
: public PDELab::ConvectionDiffusionModelProblem<GridView,RangeType>
{
public:
  template<typename Element, typename Coord>
  auto f(const Element& element, const Coord& x) const
  {
    return 10.0;
  }
};
// { problem_description_end }

int main (int argc, char *argv[]) try
{
    Dune::MPIHelper::instance(argc, argv);

    // read solver settings
    int numIt            = 100;
    int nu1              = 3;
    int nu2              = 3;
    double tolerance     = 1e-7;

    ////////////////////////////////
    //   Generate the grid
    ////////////////////////////////

    // The grid dimension
    // { poisson_grid_setup_begin }
    const int dim = 2;                         /*@\label{li:poisson_grid_creation_begin}@*/

    typedef YaspGrid<dim> GridType;
    typedef GridType::LeafGridView GridView;

    FieldVector<double,dim> h = {1.0, 1.0};
    std::array<int,dim> n = {2, 2};          // Elements per direction
    GridType grid(h,n);
  
    // refine uniformly to create multigrid hierarchy
    for (int i=0; i<3; i++)
        grid.globalRefine(1);                  /*@\label{li:poisson_grid_creation_end}@*/   /*@\label{li:poisson_global_refinement}@*/

    auto gridView = grid.leafGridView();

    // Mark Dirichlet nodes on the leaf grid view
    using BitVector = BitSetVector<1>;
    BitVector dirichletNodes(gridView.size(dim));   /*@\label{li:poisson_dirichlet_detection_begin}@*/

    for (const auto& vertex : vertices(gridView))
    {
        auto vIdx = gridView.indexSet().index(vertex);
        FieldVector<double,dim> v = vertex.geometry().corner(0);
        dirichletNodes[vIdx] = (v[0] < 0.001 or v[0] > 0.999 or v[1] < 0.001 or v[1] > 0.999);
    }                                                 /*@\label{li:poisson_dirichlet_detection_end}@*/
    // { poisson_grid_setup_end }

    ////////////////////////////////////////////////////////////////
    //  Assemble the stiffness matrix and load vector
    ////////////////////////////////////////////////////////////////

    // { poisson_assembler_begin }
    // Construct first-order Lagrangian finite element space for a cube grid
    using Basis = Functions::LagrangeBasis<GridView,1>;
    auto basis = std::make_shared<Basis>(gridView);

    using FiniteElementMap = PDELab::QkLocalFiniteElementMap<GridView,double,double,1>;
    FiniteElementMap fem(gridView);

    typedef PDELab::GridFunctionSpace<GridView,
                                      FiniteElementMap,
                                      PDELab::ConformingDirichletConstraints,
                                      PDELab::ISTL::VectorBackend<>
                                     > GridFunctionSpace;
    GridFunctionSpace gfs(grid.leafGridView(), fem);
    gfs.name("value");                                                        /*@\label{li:poisson_fe_space_setup_end}@*/

    // make grid operator
    PoissonProblem<GridView,double> poissonProblem;                           /*@\label{li:poisson_assembler_setup_begin}@*/

    typedef PDELab::ConvectionDiffusionFEM<decltype(poissonProblem),
                                           FiniteElementMap> LocalOperator;
    LocalOperator localOperator(poissonProblem);

    typedef PDELab::GridOperator<GridFunctionSpace,
                                 GridFunctionSpace,
                                 LocalOperator,
                                 PDELab::ISTL::BCRSMatrixBackend<>,
                                 double,double,double> GlobalLaplaceOperator;
    GlobalLaplaceOperator globalLaplaceOperator(gfs,gfs,localOperator, {9});            /*@\label{li:poisson_assembler_setup_end}@*/

    // make coefficient vector and initialize it from a function
//    typedef GlobalLaplaceOperator::Traits::Domain VectorContainer;
    using VectorContainer = PDELab::Backend::Vector<GridFunctionSpace,double>;          /*@\label{li:poisson_assembly_begin}@*/

    VectorContainer x0(gfs);
    x0 = 0.0;

    // represent operator as a matrix
    typedef GlobalLaplaceOperator::Jacobian M;
    typedef M::Container MatrixType;
    MatrixType stiffnessMatrix;
    M m(globalLaplaceOperator,stiffnessMatrix);
    m = 0.0;

    globalLaplaceOperator.jacobian(x0,m);

    // evaluate algebraic load vector
    typedef VectorContainer::Container VectorType;
    VectorType rhs;
    VectorContainer r(gfs,rhs);
    r = 0.0;

    globalLaplaceOperator.residual(x0,r);                                     /*@\label{li:poisson_assembly_end}@*/
    // { poisson_assembler_end }

    //////////////////////////////////////////////////////////////
    //    Create initial iterate
    //////////////////////////////////////////////////////////////

    // { initial_iterate_begin }
    VectorType x(basis->size());
    x = 0;
    // { initial_iterate_end }

    // ////////////////////////////
    //   Create a solver
    // ////////////////////////////

  // { poisson_create_functional_begin }
  using Functional = TNNMG::QuadraticFunctional<MatrixType, VectorType>;
  auto J = Functional(stiffnessMatrix, rhs);
    // { poisson_create_functional_end }

    // { poisson_setup_mg_begin }
  auto localLinearSolver = Solvers::BlockGS::LocalSolvers::direct();
  using BlockGSStep = Dune::Solvers::BlockGSStep<std::decay_t<decltype(localLinearSolver)>,
                                                    MatrixType,
                                                    VectorType,
                                                    BitVector>;
  BlockGSStep linearBaseSolverStep;

  EnergyNorm<MatrixType,VectorType> baseEnergyNorm(linearBaseSolverStep);
  auto linearBaseSolver = std::make_shared<Solvers::LoopSolver<VectorType> >(&linearBaseSolverStep,
                                                                             100,
                                                                             1e-8,
                                                                             &baseEnergyNorm,
                                                                             Solver::QUIET);

  auto smoother = std::make_shared<BlockGSStep>();

  auto linearMultigridStep = std::make_shared<Solvers::MultigridStep<MatrixType, VectorType> >();

  linearMultigridStep->setMGType(1, nu1, nu2);
  linearMultigridStep->setBaseSolver(linearBaseSolver);
  linearMultigridStep->setSmoother(smoother);

  linearMultigridStep->mgTransfer_.resize(grid.maxLevel());
  for (size_t i=0; i<linearMultigridStep->mgTransfer_.size(); i++)
  {
    // create transfer operator from level i to i+1
    auto transferOperator = std::make_shared<TruncatedDenseMGTransfer<VectorType> >();
    transferOperator->setup(grid, i, i+1);

    linearMultigridStep->mgTransfer_[i] = transferOperator;
  }
  // { poisson_setup_mg_end }

  // { poisson_create_tnnmgstep_begin }
  using DefectProjection = TNNMG::TrivialDefectProjection;
  using Linearization = TNNMG::QuadraticFunctionalConstrainedLinearization<Functional, BitVector>;
  using LineSearchSolver = TNNMG::ScalarQuadraticSolver;

  auto localSolver = TNNMG::gaussSeidelLocalSolver(TNNMG::ScalarQuadraticSolver());
  using NonlinearSmoother = TNNMG::NonlinearGSStep<Functional, decltype(localSolver), BitVector>;
  auto nonlinearSmoother = std::make_shared<NonlinearSmoother>(J, x, localSolver);

  using Step = TNNMG::TNNMGStep<Functional,
                                BitVector,
                                Linearization,
                                DefectProjection,
                                LineSearchSolver>;

  auto step = std::make_shared<Step>(J, x, nonlinearSmoother, linearMultigridStep, 1, DefectProjection(), LineSearchSolver());
  // { poisson_create_tnnmgstep_end }

  // { poisson_create_loopsolver_begin }
  auto norm = std::make_shared<EnergyNorm<MatrixType, VectorType> >(stiffnessMatrix);

  ::LoopSolver<VectorType> solver(step, numIt, tolerance, norm, Solver::FULL);

  step->setIgnore(dirichletNodes);
  step->setPreSmoothingSteps(3);

  solver.addCriterion(
            [&](){
                return Dune::formatString("   % 12d", step->linearization().truncated().count());
            },
            "   truncated   ");

  double initialEnergy = J(x);
  solver.addCriterion(
            [&](){
                static double oldEnergy=initialEnergy;
                double currentEnergy = J(x);
                double decrease = currentEnergy - oldEnergy;
                oldEnergy = currentEnergy;
                return Dune::formatString("   % 12.5e", decrease);
            },
            "   decrease    ");

  solver.addCriterion(
            [&](){
                return Dune::formatString("   % 12.5e", step->lastDampingFactor());
            },
            "   step size     ");


  solver.addCriterion(
            [&](){
                return Dune::formatString("   % 12.5e", J(x));
            },
            "   energy      ");
  // { poisson_create_loopsolver_end }

  /////////////////////////////
  //   Compute solution
  /////////////////////////////

  // { poisson_solve_begin }
  solver.preprocess();
  solver.solve();
  // { poisson_solve_end }

    // ///////////////////////////
    // Output result
    // ///////////////////////////

    // { poisson_write_begin }
    //  Make a discrete function from the FE basis and the coefficient vector
    auto xFunction = Functions::makeDiscreteGlobalBasisFunction<double>(*basis, x);

    VTKWriter<GridView> vtkWriter(gridView);
    vtkWriter.addVertexData(xFunction, VTK::FieldInfo("x", VTK::FieldInfo::Type::scalar, 1));
    vtkWriter.write("poisson_result");
    // { poisson_write_end }

} catch (Exception& e) {
    std::cout << e.what() << std::endl;
}
