#include <config.h>

// Forcefully disable ParMetis
// dune-pdelab includes ParMetis headers which do weird things with MPI.
// As we are not using ParMetis anyway let's just disable it.
// The problem will disappear once we have the code ported away from dune-pdelab.
#define HAVE_PARMETIS 0

#include <dune/common/parallel/mpihelper.hh>

#include <dune/grid/yaspgrid.hh>
#include <dune/grid/io/file/vtk.hh>

#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/bvector.hh>

#include <dune/functions/functionspacebases/lagrangebasis.hh>
#include <dune/functions/gridfunctions/discreteglobalbasisfunction.hh>

#include <dune/pdelab/finiteelementmap/qkfem.hh>
#include <dune/pdelab/localoperator/convectiondiffusionfem.hh>
#include <dune/pdelab/gridfunctionspace/dunefunctionsgridfunctionspace.hh>
#include <dune/pdelab/gridfunctionspace/vtk.hh>
#include <dune/pdelab/constraints/conforming.hh>
#include <dune/pdelab/backend/istl.hh>
#include <dune/pdelab/gridoperator/gridoperator.hh>

#include <dune/solvers/solvers/loopsolver.hh>
#include <dune/solvers/norms/energynorm.hh>
#include <dune/solvers/iterationsteps/multigridstep.hh>
#include <dune/solvers/iterationsteps/truncatedblockgsstep.hh>
#include <dune/solvers/transferoperators/truncateddensemgtransfer.hh>

#include <dune/tnnmg/functionals/bcqfconstrainedlinearization.hh>
#include <dune/tnnmg/functionals/boxconstrainedquadraticfunctional.hh>
#include <dune/tnnmg/iterationsteps/nonlineargsstep.hh>
#include <dune/tnnmg/iterationsteps/tnnmgstep.hh>
#include <dune/tnnmg/localsolvers/scalarobstaclesolver.hh>
#include <dune/tnnmg/projections/obstacledefectprojection.hh>

using namespace Dune;


// { problem_description_begin }
template <class GridView, class RangeType>
class Problem
: public PDELab::ConvectionDiffusionModelProblem<GridView,RangeType>
{
public:

  template<typename Element, typename Coord>
  auto f(const Element& element, const Coord& x) const
  {
    return -10.0;
  }

  template<typename Element, typename Coord>
  auto bctype(const Element& element, const Coord& x) const
  {
    return PDELab::ConvectionDiffusionBoundaryConditions::Dirichlet;
  }
};
// { problem_description_end }

int main (int argc, char *argv[]) try
{
    Dune::MPIHelper::instance(argc, argv);

    // Some types that I need
    typedef BCRSMatrix<FieldMatrix<double, 1, 1> > MatrixType;
    typedef BlockVector<FieldVector<double,1> >      VectorType;

    // read solver settings
    int numLevels        = 4;
    int numIt            = 100;
    int nu1              = 3;
    int nu2              = 3;
    int mu               = 1;
    double tolerance     = 1e-7;
    
    // /////////////////////////////
    //   Generate the grid
    // /////////////////////////////

    // The grid dimension
    const int dim = 2;

    typedef YaspGrid<dim> GridType;
    typedef GridType::LeafGridView GridView;

    FieldVector<double,dim> h(1);  // bounding box
    std::array<int,dim> n;
    std::fill(n.begin(), n.end(), 2);
    GridType grid(h,n);
  
    // refine uniformly until minlevel
    for (int i=0; i<numLevels-1; i++)
        grid.globalRefine(1);

    // Mark Dirichlet nodes on the leaf grid view
    using BitVector = BitSetVector<1>;
    BitVector dirichletNodes(grid.size(dim));

    auto gridView = grid.leafGridView();
    for (const auto& vertex : vertices(gridView))
    {
        size_t vIdx = gridView.indexSet().index(vertex);
        FieldVector<double,dim> v = vertex.geometry().corner(0);
        dirichletNodes[vIdx] = (v[0] < 0.001 or v[0] > 0.999 or v[1] < 0.001 or v[1] > 0.999);
    }

    // ////////////////////////////////////////////////////////////
    //    Create initial iterate
    // ////////////////////////////////////////////////////////////

    VectorType x(grid.size(dim));

    // Initial iterate
    x = 0;

    for (size_t i=0; i<x.size(); i++)
        if (dirichletNodes[i][0])
            x[i] = 0.0;

        
    ////////////////////////////////////////////////////////////////
    //  Assemble the stiffness matrix and load vector
    ////////////////////////////////////////////////////////////////
    using Basis = Functions::LagrangeBasis<GridView,1>;
    auto basis = std::make_shared<Basis>(gridView);

    using VectorType = BlockVector<FieldVector<double,1> >;
    using FiniteElementMap = PDELab::QkLocalFiniteElementMap<GridView,double,double,1>;
    FiniteElementMap fem(gridView);

    typedef PDELab::GridFunctionSpace<GridView,
                                      FiniteElementMap,
                                      PDELab::ConformingDirichletConstraints,
                                      PDELab::ISTL::VectorBackend<>
                                     > GridFunctionSpace;
    GridFunctionSpace gfs(grid.leafGridView(), fem);
    gfs.name("value");

    // Container for the Dirichlet boundary conditions
    typedef typename GridFunctionSpace::template ConstraintsContainer<double>::Type C;
    C cg;

    Problem<GridView,double> problem;
    PDELab::ConvectionDiffusionBoundaryConditionAdapter<decltype(problem)> bctype(problem);
    PDELab::constraints(bctype,gfs,cg);

    // make grid operator
    typedef PDELab::ConvectionDiffusionFEM<decltype(problem),
                                           typename GridFunctionSpace::Traits::FiniteElementMap> LOP;
    LOP lop(problem);

    typedef PDELab::GridOperator<GridFunctionSpace,
                                 GridFunctionSpace,
                                 LOP,
                                 PDELab::ISTL::BCRSMatrixBackend<>,
                                 double,double,double,C,C> GO;
    GO go(gfs,cg,gfs,cg,lop, {9});

    // make coefficient vector and initialize it from a function
    typedef typename GO::Traits::Domain VectorContainer;
    VectorContainer x0(gfs);
    x0 = 0.0;

    // represent operator as a matrix
    typedef typename GO::Jacobian MatrixContainer;
    typename MatrixContainer::Container stiffnessMatrix;
    MatrixContainer m(go,stiffnessMatrix);
    m = 0.0;

    go.jacobian(x0,m);

    // evaluate residual w.r.t initial guess
    VectorType rhs(grid.size(dim));
    VectorContainer r(gfs,rhs);
    r = 0.0;

    go.residual(x0,r);

    // ////////////////////////////
    //   Create a solver
    // ////////////////////////////

  // { obstacle_create_functional_begin }
  TruncatedBlockGSStep<MatrixType,VectorType> linearBaseSolverStep;

  EnergyNorm<MatrixType,VectorType> baseEnergyNorm(linearBaseSolverStep);
  auto linearBaseSolver = std::make_shared<Solvers::LoopSolver<VectorType> >(&linearBaseSolverStep,
                                                                             100,
                                                                             1e-8,
                                                                             &baseEnergyNorm,
                                                                             Solver::QUIET);

  auto smoother = std::make_shared<TruncatedBlockGSStep<MatrixType, VectorType, BitVector> >();

  auto linearMultigridStep = std::make_shared<Solvers::MultigridStep<MatrixType, VectorType> >();

  linearMultigridStep->setMGType(1, nu1, nu2);
  linearMultigridStep->setBaseSolver(linearBaseSolver);
  linearMultigridStep->setSmoother(smoother);

  linearMultigridStep->mgTransfer_.resize(grid.maxLevel());
  for (size_t i=0; i<linearMultigridStep->mgTransfer_.size(); i++)
  {
    // create transfer operator from level i to i+1
    auto transferOperator = std::make_shared<TruncatedDenseMGTransfer<VectorType> >();
    transferOperator->setup(grid, i, i+1);

    linearMultigridStep->mgTransfer_[i] = transferOperator;
  }

  VectorType lower(rhs.size());
  VectorType upper(rhs.size());

  lower = -std::numeric_limits<double>::max();
  upper = 0.3;

  using Functional = TNNMG::BoxConstrainedQuadraticFunctional<MatrixType, VectorType, VectorType, VectorType, double>;
  auto J = Functional(stiffnessMatrix, rhs, lower, upper);

  auto localSolver = gaussSeidelLocalSolver(TNNMG::ScalarObstacleSolver());

  using Linearization = TNNMG::BoxConstrainedQuadraticFunctionalConstrainedLinearization<Functional, BitVector>;
  using DefectProjection = TNNMG::ObstacleDefectProjection;
  using LineSearchSolver = TNNMG::ScalarObstacleSolver;

  using NonlinearSmoother = TNNMG::NonlinearGSStep<Functional, decltype(localSolver), BitVector>;
  auto nonlinearSmoother = std::make_shared<NonlinearSmoother>(J, x, localSolver);

  using Step = TNNMG::TNNMGStep<Functional,
                                BitVector,
                                Linearization,
                                DefectProjection,
                                LineSearchSolver>;

  auto step = std::make_shared<Step>(J, x, nonlinearSmoother, linearMultigridStep, mu, DefectProjection(), LineSearchSolver());

  auto norm = std::make_shared<EnergyNorm<MatrixType, VectorType> >(stiffnessMatrix);
  ::LoopSolver<VectorType> solver(step, numIt, tolerance, norm, Solver::FULL);

  step->setIgnore(dirichletNodes);
  step->setPreSmoothingSteps(3);

  solver.addCriterion(
            [&](){
                return Dune::formatString("   % 12d", step->linearization().truncated().count());
            },
            "   truncated   ");

  double initialEnergy = J(x);
  solver.addCriterion(
            [&](){
                static double oldEnergy=initialEnergy;
                double currentEnergy = J(x);
                double decrease = currentEnergy - oldEnergy;
                oldEnergy = currentEnergy;
                return Dune::formatString("   % 12.5e", decrease);
            },
            "   decrease    ");

  solver.addCriterion(
            [&](){
                return Dune::formatString("   % 12.5e", step->lastDampingFactor());
            },
            "   step size     ");


  solver.addCriterion(
            [&](){
                return Dune::formatString("   % 12.5e", J(x));
            },
            "   energy      ");


  std::vector<double> correctionNorms;
  solver.addCriterion(Dune::Solvers::correctionNormCriterion(*step, *norm, 1e-10, correctionNorms));
  // { obstacle_create_functional_end }

  /////////////////////////////
  //   Compute solution
  /////////////////////////////

  solver.preprocess();
  solver.solve();

    // ///////////////////////////
    // Output result
    // ///////////////////////////

  //  Make a discrete function from the FE basis and the coefficient vector
  auto xFunction = Functions::makeDiscreteGlobalBasisFunction<double>(*basis, x);

  VTKWriter<GridView> vtkWriter(gridView);
  vtkWriter.addVertexData(xFunction, VTK::FieldInfo("x", VTK::FieldInfo::Type::scalar, 1));
    vtkWriter.write("obstacle_result");

} catch (Exception& e) {
  std::cout << e.what() << std::endl;
}
