#include <config.h>

// Forcefully disable ParMetis
// dune-pdelab includes ParMetis headers which do weird things with MPI.
// As we are not using ParMetis anyway let's just disable it.
// The problem will disappear once we have the code ported away from dune-pdelab.
#define HAVE_PARMETIS 0
#include <array>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/parametertree.hh>

#include <dune/grid/yaspgrid.hh>

#include <dune/grid/io/file/vtk.hh>

#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/bvector.hh>

#include<dune/pdelab/finiteelementmap/qkfem.hh>
#include<dune/pdelab/localoperator/linearelasticity.hh>
#include<dune/pdelab/gridfunctionspace/vectorgridfunctionspace.hh>
#include<dune/pdelab/gridfunctionspace/vtk.hh>
#include<dune/pdelab/constraints/conforming.hh>
#include<dune/pdelab/backend/istl.hh>
#include<dune/pdelab/gridoperator/gridoperator.hh>

#include <dune/solvers/solvers/loopsolver.hh>
#include <dune/solvers/norms/energynorm.hh>
#include <dune/solvers/iterationsteps/projectedblockgsstep.hh>
#include <dune/solvers/iterationsteps/truncatedblockgsstep.hh>
#include <dune/solvers/iterationsteps/multigridstep.hh>
#include <dune/solvers/transferoperators/truncateddensemgtransfer.hh>

#include <dune/tnnmg/functionals/bcqfconstrainedlinearization.hh>
#include <dune/tnnmg/functionals/boxconstrainedquadraticfunctional.hh>
#include <dune/tnnmg/iterationsteps/nonlineargsstep.hh>
#include <dune/tnnmg/iterationsteps/tnnmgstep.hh>
#include <dune/tnnmg/localsolvers/scalarobstaclesolver.hh>
#include <dune/tnnmg/projections/obstacledefectprojection.hh>

using namespace Dune;

// Class that stores the material parameters of a St.Venant-Kirchhoff material
// { contact_material_model_begin }
template <class GridView>
class StVenantKirchhoffParameters
  : public Dune::PDELab::LinearElasticityParameterInterface<
  Dune::PDELab::LinearElasticityParameterTraits<GridView, double>,
  StVenantKirchhoffParameters<GridView> >
{
public:
  typedef Dune::PDELab::LinearElasticityParameterTraits<GridView, double> Traits;

  StVenantKirchhoffParameters(typename Traits::RangeFieldType l,
                              typename Traits::RangeFieldType m) :
    lambda_(l), mu_(m)
  {}

  typename Traits::RangeFieldType
  lambda (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
  {
    return lambda_;
  }

  typename Traits::RangeFieldType
  mu (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
  {
    return mu_;
  }

private:
  typename Traits::RangeFieldType lambda_;
  typename Traits::RangeFieldType mu_;
};
// { contact_material_model_end }



int main (int argc, char *argv[]) try
{
    Dune::MPIHelper::instance(argc, argv);

    // Solver settings
    int numLevels        = 3;
    int numIt            = 100;
    int nu1              = 3;    // number of pre-smoothing steps
    int nu2              = 3;    // number of post-smoothing steps
    double tolerance     = 1e-7;
    double baseTolerance = 1e-8;

    // read problem settings
    double lambda = 1e7;
    double mu     = 6.5e6;


    // /////////////////////////////
    //   Generate the grid
    // /////////////////////////////

    // The grid dimension
    const int dim = 2;

    typedef YaspGrid<dim> GridType;
    typedef GridType::LeafGridView GridView;

    FieldVector<double,dim> h(1);  // bounding box
    std::array<int,dim> n;
    std::fill(n.begin(), n.end(), 10);
    GridType grid(h,n);
  
    // refine uniformly until minlevel
    for (int i=0; i<numLevels-1; i++)
        grid.globalRefine(1);

    // Mark Dirichlet and contact nodes on the leaf grid view
    // { contact_contactnodes_begin }
    using BitVector = BitSetVector<dim>;
    BitVector dirichletNodes(grid.size(dim));
    BitSetVector<1> contactNodes(grid.size(dim));

    auto gridView = grid.leafGridView();
    for (const auto& vertex : vertices(gridView))
    {
        size_t vIdx = gridView.indexSet().index(vertex);
        dirichletNodes[vIdx] = (vertex.geometry().corner(0)[0] > 0.999);
        contactNodes[vIdx]   = (vertex.geometry().corner(0)[0] < 0.001);
    }
    // { contact_contactnodes_end }

    ////////////////////////////////////////////////////////////////
    //  Assemble the stiffness matrix
    ////////////////////////////////////////////////////////////////

    // set up vector-valued finite element space
    // { contact_fe_space_setup_begin }
    typedef PDELab::QkLocalFiniteElementMap<GridView,double,double,1> FEM;
    FEM fem(grid.leafGridView());    /*@\label{li:contact_create_fe_map}@*/

    typedef PDELab::VectorGridFunctionSpace<GridView,   /*@\label{li:contact_create_gfs_begin}@*/
                                            FEM,
                                            dim,        /*@\label{li:contact_value_space_dimension}@*/
                                            PDELab::ISTL::VectorBackend<PDELab::ISTL::Blocking::fixed>,
                                            PDELab::ISTL::VectorBackend<>,
                                            PDELab::ConformingDirichletConstraints,
                                            PDELab::EntityBlockedOrderingTag,
                                            PDELab::DefaultLeafOrderingTag
                                           > GFS;
    GFS gfs(grid.leafGridView(), fem);
    gfs.name("displacement");               /*@\label{li:contact_create_gfs_end}@*/
    // { contact_fe_space_setup_end }

    // { contact_operator_setup_begin }
    // create the model describing our problem
    StVenantKirchhoffParameters<GridView> model(lambda, mu);  /*@\label{li:contact_create_linear_model}@*/

    // make grid operator
    PDELab::LinearElasticity<StVenantKirchhoffParameters<GridView> > lop(model);

    // set up linear operator acting on the FEM space
    typedef PDELab::GridOperator<GFS,
                                 GFS,
                                 PDELab::LinearElasticity<StVenantKirchhoffParameters<GridView> >,
                                 PDELab::ISTL::BCRSMatrixBackend<>,
                                 double,double,double> GridOperator;

    GridOperator gridOperator(gfs, gfs, lop, {9});
    // { contact_operator_setup_end }

    // { contact_assembly_begin }
    typedef GridOperator::Traits::Domain V;
    typedef GridOperator::Jacobian M;
    using MatrixType = M::Container;   //  BCRSMatrix<FieldMatrix<double, dim, dim> >
    using VectorType = V::Container;   //  BlockVector<FieldVector<double,dim> >

    // Dummy coefficient vector
    V x0(gfs);
    x0 = 0.0;

    // represent operator as a matrix
    MatrixType stiffnessMatrix;
    M m(gridOperator,stiffnessMatrix);
    m = 0.0;

    // Compute stiffness matrix
    gridOperator.jacobian(x0, m);

    // load vector
    VectorType rhs(grid.size(dim));
    rhs = 0;
    // { contact_assembly_end }

    // ////////////////////////////////////////////////////////////
    //    Create initial iterate
    // ////////////////////////////////////////////////////////////

    // { contact_initial_iterate_begin }
    // Initial iterate
    VectorType x(grid.size(dim));
    x = 0;

    FieldVector<double,dim> dirichletValue(0);
    dirichletValue[0] = -0.1;

    for (size_t i=0; i<rhs.size(); i++)
        for (int j=0; j<dim; j++)
            if (dirichletNodes[i][j])
                x[i][j] = dirichletValue[j];
    // { contact_initial_iterate_end }



    // ////////////////////////////
    //   Create a solver
    // ////////////////////////////

    // { contact_setup_mg_begin }
    TruncatedBlockGSStep<MatrixType,VectorType> linearBaseSolverStep;

    EnergyNorm<MatrixType,VectorType> baseEnergyNorm(linearBaseSolverStep);
    auto linearBaseSolver = std::make_shared<::LoopSolver<VectorType> >(&linearBaseSolverStep,
                                            100,
                                            baseTolerance,
                                            &baseEnergyNorm,
                                            Solver::QUIET);

  // Make pre and postsmoothers
  auto linearSmoother  = std::make_shared<TruncatedBlockGSStep<MatrixType, VectorType, BitVector> >();

  auto linearMultigridStep = std::make_shared<MultigridStep<MatrixType, VectorType> >();

  linearMultigridStep->setMGType(1, nu1, nu2);
  linearMultigridStep->setBaseSolver(linearBaseSolver);
  linearMultigridStep->setSmoother(linearSmoother);

  using TransferOperator = TruncatedDenseMGTransfer<VectorType>;
  using TransferOperators = std::vector<std::shared_ptr<TransferOperator>>;
  TransferOperators transferOperators(numLevels-1);

  linearMultigridStep->mgTransfer_.resize(numLevels-1);
  for (size_t i=0; i<linearMultigridStep->mgTransfer_.size(); i++)
  {
    // create transfer operator from level i to i+1
    transferOperators[i] = std::make_shared<TransferOperator>();
    transferOperators[i]->setup(grid, i, i+1);
    linearMultigridStep->mgTransfer_[i] = transferOperators[i];
  }
  // { contact_setup_mg_end }

  // { contact_create_tnnmg_begin }
  VectorType lowerObstacle(x.size());
  VectorType upperObstacle(x.size());
  std::fill(lowerObstacle.begin(), lowerObstacle.end(), -std::numeric_limits<double>::max());
  std::fill(upperObstacle.begin(), upperObstacle.end(),  std::numeric_limits<double>::max());
  for (size_t i=0; i<lowerObstacle.size(); i++)
    if (contactNodes[i][0])
      lowerObstacle[i][0] = 0;

  using Functional = TNNMG::BoxConstrainedQuadraticFunctional<MatrixType, VectorType, VectorType, VectorType, double>;
  auto J = Functional(stiffnessMatrix, rhs, lowerObstacle, upperObstacle);

  auto localSolver = gaussSeidelLocalSolver(TNNMG::ScalarObstacleSolver());

  using Linearization = TNNMG::BoxConstrainedQuadraticFunctionalConstrainedLinearization<Functional, BitVector>;
  using DefectProjection = TNNMG::ObstacleDefectProjection;
  using LineSearchSolver = TNNMG::ScalarObstacleSolver;

  using NonlinearSmoother = TNNMG::NonlinearGSStep<Functional, decltype(localSolver), BitVector>;
  auto nonlinearSmoother = std::make_shared<NonlinearSmoother>(J, x, localSolver);

  using Step = TNNMG::TNNMGStep<Functional,
                                BitVector,
                                Linearization,
                                DefectProjection,
                                LineSearchSolver>;

  auto step = std::make_shared<Step>(J, x, nonlinearSmoother, linearMultigridStep, 1, DefectProjection(), LineSearchSolver());

  auto norm = std::make_shared<EnergyNorm<MatrixType, VectorType> >(stiffnessMatrix);
  ::LoopSolver<VectorType> solver(step, numIt, tolerance, norm, Solver::FULL);

  step->setIgnore(dirichletNodes);
  step->setPreSmoothingSteps(3);

  solver.addCriterion(
            [&](){
                return Dune::formatString("   % 12d", step->linearization().truncated().count());
            },
            "   truncated   ");

  double initialEnergy = J(x);
  solver.addCriterion(
            [&](){
                static double oldEnergy=initialEnergy;
                double currentEnergy = J(x);
                double decrease = currentEnergy - oldEnergy;
                oldEnergy = currentEnergy;
                return Dune::formatString("   % 12.5e", decrease);
            },
            "   decrease    ");

  solver.addCriterion(
            [&](){
                return Dune::formatString("   % 12.5e", step->lastDampingFactor());
            },
            "   step size     ");

  solver.addCriterion(
            [&](){
                return Dune::formatString("   % 12.5e", J(x));
            },
            "   energy      ");
    // { contact_create_tnnmg_end }

    // ///////////////////////////
    //   Compute solution
    // ///////////////////////////

    solver.preprocess();

    solver.solve();

    // ///////////////////////////
    // Output result
    // ///////////////////////////

    VTKWriter<GridType::LeafGridView> vtkWriter(grid.leafGridView());
    V xV(gfs);
    PDELab::Backend::native(xV) = x;
    Dune::PDELab::addSolutionToVTKWriter(vtkWriter,gfs,xV);
    vtkWriter.write("contact_result");

} catch (Exception& e) {
    std::cout << e.what() << std::endl;
}
