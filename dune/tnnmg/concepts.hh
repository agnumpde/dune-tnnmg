// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set ts=8 sw=2 et sts=2:
#ifndef DUNE_TNNMG_CONCEPTS_HH
#define DUNE_TNNMG_CONCEPTS_HH



namespace Dune {
namespace TNNMG {
namespace Concept {



struct IsLessThanComparable
{

  struct RawLessThanComparable
  {
    template<class T1, class T2>
    auto require(const T1& t1, const T2& t2) -> decltype(
      t1<t2
    );
  };

  struct LessThanComparableHelper
  {

    // if one of both types is not a tuple we can use the raw comparable check
    template<class T1, class T2,
      std::enable_if_t<(not IsTupleOrDerived<T1>::value) or (not IsTupleOrDerived<T2>::value), int> = 0>
    static constexpr auto check(const T1&, const T2&)
    {
      return std::integral_constant<bool, models<RawLessThanComparable, T1, T2>()>();
    }

    // tuples of different length are not comparable
    template<class... T, class... U,
      std::enable_if_t<not (std::tuple_size<std::tuple<T...>>::value == std::tuple_size<std::tuple<U...>>::value), int> = 0>
    static constexpr auto check(const std::tuple<T...>&, const std::tuple<U...>&)
    {
      return std::false_type();
    }

    // empty tuples are always comparable
    static constexpr auto check(const std::tuple<>&, const std::tuple<>&)
    {
      return std::true_type();
    }

    // check if tuples are comparable
    template<class T0, class... T, class U0, class... U,
      std::enable_if_t<(std::tuple_size<std::tuple<T...>>::value == std::tuple_size<std::tuple<U...>>::value), int> = 0>
    static constexpr auto check(const std::tuple<T0, T...>&, const std::tuple<U0, U...>&)
    {
      using HeadComparable = decltype(check(std::declval<T0>(), std::declval<U0>()));
      using TailComparable = decltype(check(std::declval<std::tuple<T...>>(), std::declval<std::tuple<U...>>()));
      return std::integral_constant<bool, HeadComparable::value and TailComparable::value>();
    }
  };

  template<class T1, class T2>
  auto require(const T1& t1, const T2& t2) -> decltype(
    Dune::Concept::requireTrue<decltype(LessThanComparableHelper::check(t1, t2))::value>()
  );
};


} // namespace Concept
} // namespace TNNMG
} // namespace Dune



#endif // DUNE_TNNMG_CONCEPTS_HH
