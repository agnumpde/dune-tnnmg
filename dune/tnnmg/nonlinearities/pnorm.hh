#ifndef PNORM_HH
#define PNORM_HH

#include <dune/common/fmatrix.hh>
#include <dune/common/fvector.hh>

#include "smallfunctional.hh"

//! The map \f$ x \mapsto 1/p * (|x|^2 + c)^{p/2}\f$
template<int dimension>
class PNorm : public SmallFunctional< dimension >
{
    public:
        typedef typename PNorm< dimension >::SmallVector SmallVector;
        typedef typename PNorm< dimension >::SmallMatrix SmallMatrix;

        PNorm(double _p, double _c=0.0) :
            p(_p),
            c(_c)
        {}

        double operator()(const SmallVector v) const
        {
            return pow(c+v.two_norm2(), p/2.0) / p;
        }

        SmallVector d(const SmallVector v) const
        {
            SmallVector r = v;
            double norm2 = c + v.two_norm2();
            if (norm2 > 0.0)
                r *= pow(norm2, (p-2.0)/2.0);
            else
                r *= 0.0;
            return r;
        }

        SmallMatrix d2(const SmallVector v) const
        {
            SmallMatrix H(0.0);

            double norm2 = c + v.two_norm2();

            double norm2_p2;
            double norm2_p4;

            if (norm2 > 0.0)
            {
                norm2_p2 = pow(norm2, (p-2.0)/2.0);
                norm2_p4 = (p-2.0) * pow(norm2, (p-4.0)/2.0);
            }
            else
            {
                norm2_p2 = 1.0;
                norm2_p4 = 0.0;
                /*		norm_p2 = pow(1e-5, p-2.0);
                        norm_p4 = (p-2.0) * pow(1e-5, p-4.0);*/
            }

            for (int row=0; row < SmallMatrix::rows; ++row)
            {
                for (int col=0; col < SmallMatrix::cols; ++col)
                    H[row][col] = norm2_p4 * v[row]*v[col];

                H[row][row] += norm2_p2;
            }
            return H;
        }

    private:
        double p;
        double c;
};

#endif
