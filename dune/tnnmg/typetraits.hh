// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set ts=8 sw=2 et sts=2:
#ifndef DUNE_TNNMG_TYPETRAITS_HH
#define DUNE_TNNMG_TYPETRAITS_HH



namespace Dune {
namespace TNNMG {

// Empty for now. But we may need more type traits in dune-tnnmg later on.
// To avoid removing and readding the header it's kept empty.

} // namespace TNNMG
} // namespace Dune



#endif // DUNE_TNNMG_TYPETRAITS_HH
