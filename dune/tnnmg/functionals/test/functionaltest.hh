// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_TNNMG_FUNCTIONALS_FUNCTIONALTEST_HH
#define DUNE_TNNMG_FUNCTIONALS_FUNCTIONALTEST_HH


/** \file
 *  \brief Contains various unit tests for nonlinear convex functionals
 */

#include <vector>
#include <cmath>
#include <limits>
#include <array>
#include <iostream>

#include <dune/common/exceptions.hh>
#include <dune/common/classname.hh>

#include <dune/istl/matrixindexset.hh>

#include <dune/solvers/common/interval.hh>

namespace Dune {

namespace TNNMG {

/** \brief Test whether functional is convex
 * \tparam Functional Type of the functional to be tested
 * \param functional The functional to be tested
 * \param testPoints Test convexity along segment between pairs of these points
 */
template <class Functional>
void testConvexity(const Functional& functional,
                   const std::vector<typename Functional::Vector>& testPoints)
{
  for (size_t i=0; i<testPoints.size(); i++)
    for (size_t j=0; j<testPoints.size(); j++)
    {
      // short-hand for the two test points
      const auto& p0 = testPoints[i];
      const auto& p1 = testPoints[j];

      // pre-compute functional values at the segment ends
      double v0 = functional(p0);
      double v1 = functional(p1);

      // Test convexity at a few selected points between the two test points
      for (double t : {0.0, 0.2, 0.4, 0.6, 0.8, 1.0})
      {
        // convex combination between the two test points
        auto p = p0;
        p *= (1-t);
        p.axpy(t,p1);

        // Test for convexity
        if (functional(p) > ((1-t)*v0 + t*v1) + 1e-10)
          DUNE_THROW(Exception, "Functional is not convex!");
      }
    }
}

/** \brief Test whether a functional is positive 1-homogeneous
 *
 * Being positive 1-homogeneous is not a requirement for functionals to work
 * in a TNNMG algorithm.  However, various important functionals are homogeneous
 * in this sense, therefore it is convenient to have a test for it.
 *
 * \tparam Functional Type of the functional to be tested
 * \param functional The functional to be tested
 * \param testDirections Test homogeneity at these points
 */
template <class Functional>
void testHomogeneity(const Functional& functional,
                     const std::vector<typename Functional::Vector>& testDirections)
{
  for (auto&& testDirection : testDirections)
  {
    // Test convexity at a few selected points between the two test points
    for (double t : {0.0, 0.25, 0.5, 0.75, 1.0, 1.5, 2.0, 5.0})
    {
      auto scaledDirection = testDirection;
      scaledDirection *= t;
      if (std::abs(functional(scaledDirection) - t*functional(testDirection)) > 1e-6)
        DUNE_THROW(MathError, "Functional is not positive 1-homogeneous!");
    }
  }
}


/** \brief Test the addGradient method
 *
 * This method tests the Functional::addGradient method by comparing its result with a
 * Finite-Difference-Approximation.  Test points from the testPoints input argument
 * are skipped if the method Functional::regularity returns a value larger than 1e6
 * for them.  In that case the functional is not differentiable at that point.
 *
 * \tparam Functional Type of the functional to be tested
 * \param functional The functional to be tested
 * \param testPoints Test addGradient at these points
 */
template <class Functional>
void testGradient(Functional& functional,
                  const std::vector<typename Functional::Vector>& testPoints)
{
  for (auto&& testPoint : testPoints)
  {
    // Get the gradient at the current test point as computed by 'functional'
    typename Functional::Vector gradient(testPoint.size());
    gradient = 0;
    functional.addGradient(testPoint, gradient);

    // Compute FD approximation to the gradient
    // Best value: square root of the machine precision
    const double eps = std::sqrt(std::numeric_limits<double>::epsilon());

    for (size_t i=0; i<testPoint.size(); i++)
      for (size_t j=0; j<testPoint[i].size(); j++)
      {
        // Do not compare anything if the functional claims to be non-differentiable
        // at the test point in the given direction
        functional.setVector(testPoint);
        if (functional.regularity(i, testPoint[i][j], j) > 1e10)
          continue;

        auto forwardPoint = testPoint;
        forwardPoint[i][j] += eps;
        auto backwardPoint = testPoint;
        backwardPoint[i][j] -= eps;

        auto fdGradient = (functional(forwardPoint) - functional(backwardPoint)) / (2*eps);

        if (std::abs(fdGradient - gradient[i][j]) > 100*eps)
        {
          std::cout << "Bug in addGradient for functional type " << Dune::className<Functional>() << ":" << std::endl;
          std::cerr << "Gradient doesn't match FD approximation at coefficient (" << i << ", " << j << ")" << std::endl;
          std::cerr << "Gradient: " << gradient[i][j] << ",   FD: " << fdGradient << std::endl;
          DUNE_THROW(MathError,"");
        }
      }
  }
}

/** \brief Test the addHessian method
 *
 * This method tests the Functional::addHessian method by comparing its result with a
 * Finite-Difference-Approximation.  Test points from the testPoints input argument
 * are skipped if the method Functional::regularity returns a value larger than 1e6
 * for them.  In that case the functional is not differentiable at that point.
 *
 * \tparam Functional Type of the functional to be tested
 * \param functional The functional to be tested
 * \param testPoints Test addHessian at these points
 */
template <class Functional>
void testHessian(Functional& functional,
                 const std::vector<typename Functional::Vector>& testPoints)
{
  for (auto&& testPoint : testPoints)
  {
    // Get the Hessian at the current test point as computed by 'functional'
    /** \todo This code is conservative, and adds the entire matrix to the pattern.
     * A smarter code would ask the functional for all entries it intends to write,
     * but I don't know if and how this is possible.
     */
    MatrixIndexSet diagonalPattern;
    diagonalPattern.resize(testPoint.size(), testPoint.size());
    for (size_t i=0; i<testPoint.size(); i++)
      for (size_t j=0; j<testPoint.size(); j++)
        diagonalPattern.add(i,j);

    typename Functional::MatrixType hessian;
    diagonalPattern.exportIdx(hessian);

    hessian = 0;
    functional.addHessian(testPoint, hessian);

    // FD step size. Best value: fourth root of the machine precision
    const double eps = std::pow(std::numeric_limits<double>::epsilon(),0.25);

    for (size_t i=0; i<testPoint.size(); i++)
    {
      for (size_t j=0; j<testPoint.size(); j++)
      {
        for (size_t k=0; k<testPoint[i].size(); k++)
        {
          for (size_t l=0; l<testPoint[j].size(); l++)
          {
            // Do not compare anything if the functional claims to be non-differentiable
            // at the test point in the given direction
            functional.setVector(testPoint);
            bool nonDifferentiableIK = functional.regularity(i, testPoint[i][k], k) > 1e10;
            bool nonDifferentiableJL = functional.regularity(j, testPoint[j][l], l) > 1e10;
            if (nonDifferentiableIK || nonDifferentiableJL)
              continue;

            // Compute an approximation to the derivative by finite differences
            std::array<typename Functional::VectorType, 4> neighborPoints;
            std::fill(neighborPoints.begin(), neighborPoints.end(), testPoint);

            neighborPoints[0][i][k] += eps;
            neighborPoints[0][j][l] += eps;
            neighborPoints[1][i][k] -= eps;
            neighborPoints[1][j][l] += eps;
            neighborPoints[2][i][k] += eps;
            neighborPoints[2][j][l] -= eps;
            neighborPoints[3][i][k] -= eps;
            neighborPoints[3][j][l] -= eps;

            std::array<double,4> neighborValues;
            for (int ii = 0; ii < 4; ii++)
              neighborValues[ii] = functional(neighborPoints[ii]);

            // The current partial derivative
            double derivative = hessian.exists(i,j) ? hessian[i][j][k][l] : 0.0;

            double finiteDiff = (neighborValues[0]
                  - neighborValues[1] - neighborValues[2]
                  + neighborValues[3]) / (4 * eps * eps);

            // Check whether second derivative and its FD approximation match
            if (std::abs(derivative - finiteDiff) > eps)
            {
              std::cout << "Bug in addHessian for functional type "
                        << Dune::className<Functional>() << ":" << std::endl;
              std::cout << "    Second derivative does not agree with FD approximation" << std::endl;
              std::cout << "    Expected: " << derivative << ",  FD: " << finiteDiff << std::endl;
              std::cout << std::endl;

              DUNE_THROW(MathError,"");
            }
          }
        }
      }
    }
  }
}

/** \brief Test the directionalSubDiff method
 *
 * This method tests the Functional::directionalSubDiff method by comparing its result with a
 * Finite-Difference-Approximation.
 *
 * \tparam Functional Type of the functional to be tested
 * \param functional The functional to be tested
 * \param testPoints Test directionalSubDiff at these points
 */
template <class Functional>
void testDirectionalSubdifferential(const Functional& functional,
                                    const std::vector<typename Functional::Vector>& testPoints)
{
  // Step size. Best value: square root of the machine precision
  const double eps = std::sqrt(std::numeric_limits<double>::epsilon());

  // Loop over all test points
  for (auto&& testPoint : testPoints)
  {
    // Abuse test points also as test directions
    for (auto&& testDirection : testPoints)
    {
      Solvers::Interval<double> subDifferential;
      functional.directionalSubDiff(testPoint, testDirection, subDifferential);

      auto forwardPoint = testPoint;
      forwardPoint.axpy(eps,testDirection);
      auto backwardPoint = testPoint;
      backwardPoint.axpy(-eps,testDirection);

      auto forwardFDGradient  = (functional(forwardPoint) - functional(testPoint)) / eps;
      auto backwardFDGradient = (functional(testPoint) - functional(backwardPoint)) / eps;

      if (std::abs(backwardFDGradient - subDifferential[0]) > 100*eps
          or std::abs(forwardFDGradient - subDifferential[1]) > 100*eps)
      {
        std::cout << "Bug in directionalSubDiff for functional type " << Dune::className<Functional>() << ":" << std::endl;
        std::cerr << "Subdifferential doesn't match FD approximation" << std::endl;
        std::cerr << "SubDifferential: " << subDifferential
                  << ",   FD: [" << backwardFDGradient << ", " << forwardFDGradient << "]" << std::endl;
        std::cerr << "Test point:" << std::endl << testPoint << std::endl;
        std::cerr << "Test direction:" << std::endl << testDirection << std::endl;
        DUNE_THROW(MathError,"");
      }
    }
  }
}


/** \brief Test the subDiff method
 *
 * This method tests the Functional::subDiff method by comparing its result with a
 * Finite Difference approximation.
 *
 * \tparam Functional Type of the functional to be tested
 * \param functional The functional to be tested
 * \param testPoints Test subDiff at these points
 */
template <class Functional>
void testSubDiff(Functional& functional,
                 const std::vector<typename Functional::Vector>& testPoints)
{
  for (auto&& testPoint : testPoints)
  {
    // Step size. Best value: square root of the machine precision
    const double eps = std::sqrt(std::numeric_limits<double>::epsilon());

    for (size_t i=0; i<testPoint.size(); i++)
      for (size_t j=0; j<testPoint[i].size(); j++)
      {
        Solvers::Interval<double> subDifferential;
        // subDiff needs to be given the current point internally
        functional.setVector(testPoint);
        functional.subDiff(i, testPoint[i][j], subDifferential, j);

        auto forwardPoint = testPoint;
        forwardPoint[i][j] += eps;
        auto backwardPoint = testPoint;
        backwardPoint[i][j] -= eps;

        auto forwardFDGradient  = (functional(forwardPoint) - functional(testPoint)) / eps;
        auto backwardFDGradient = (functional(testPoint) - functional(backwardPoint)) / eps;

        if (std::abs(backwardFDGradient - subDifferential[0]) > 100*eps
            or std::abs(forwardFDGradient - subDifferential[1]) > 100*eps)
        {
          std::cout << "Bug in subDiff for functional type " << Dune::className<Functional>() << ":" << std::endl;
          std::cerr << "Subdifferential doesn't match FD approximation at coefficient (" << i << ", " << j << ")" << std::endl;
          std::cerr << "SubDifferential: " << subDifferential
                    << ",   FD: [" << backwardFDGradient << ", " << forwardFDGradient << "]" << std::endl;
          DUNE_THROW(MathError,"");
        }
      }
  }
}

/** \brief Test whether the values of a directional restriction are correct, by comparing
 *  with the values of the unrestricted functional.
 * \param functional The functional to be restricted
 * \param testPoints Set of test points, will additionally be abused as a set of test directions, too
 * \param testParameters Test the one-dimensional restriction at these parameter values
 */
template <typename Functional, typename TestPoints>
void testDirectionalRestrictionValues(const Functional& functional,
                                      const TestPoints& testPoints,
                                      const std::vector<double> testParameters)
{
  // Loop over all test points
  for (auto&& origin : testPoints)
  {
    // Abuse test points also as test directions
    for (auto&& testDirection : testPoints)
    {
      auto restriction = directionalRestriction(functional, origin, testDirection);

      for (auto v : testParameters)
      {
        // Test whether restriction really is a restriction
        auto probe = origin;
        probe.axpy(v, testDirection);

        auto restrictionValue = restriction(v);
        auto realValue        = functional(probe);

        if (fabs(restrictionValue - realValue) > 1e-6)
        {
          std::cerr << "Value of the directional restriction does not match the actual "
                    << "functional value." << std::endl
                    << "Restriction value at parameter " << v << " : " << restrictionValue << std::endl
                    << "Actual value: " << realValue << std::endl;
          abort();
        }
      }
    }
  }
}

/** \brief Test whether the subdifferentials of a directional restriction are correct, by comparing
 *  with a finite-difference approximation
 * \param functional The functional to be restricted
 * \param testPoints Set of test points, will additionally be abused as a set of test directions, too
 * \param testParameters Test the one-dimensional restriction at these parameter values
 */
template <typename Functional, typename TestPoints>
void testDirectionalRestrictionSubdifferential(const Functional& functional,
                                               const TestPoints& testPoints,
                                               const std::vector<double> testParameters)
{
  // Loop over all test points
  for (auto&& origin : testPoints)
  {
    // Abuse test points also as test directions
    for (auto&& testDirection : testPoints)
    {
      auto restriction = directionalRestriction(functional, origin, testDirection);

      for (auto v : testParameters)
      {
        // Test the subdifferential of the restriction
        Solvers::Interval<double> subDifferential = restriction.subDifferential(v);

        // Step size. Best value: square root of the machine precision
        const double eps = std::sqrt(std::numeric_limits<double>::epsilon());

        auto forwardFDGradient  = (restriction(v+eps) - restriction(v)) / eps;
        auto backwardFDGradient = (restriction(v) - restriction(v-eps)) / eps;

        if (std::abs(backwardFDGradient - subDifferential[0]) > 1e4*eps
            or std::abs(forwardFDGradient - subDifferential[1]) > 1e4*eps)
        {
          std::cout << "Bug in subdifferential for directional restriction " << Dune::className(restriction) << ":" << std::endl;
          std::cerr << "Subdifferential doesn't match FD approximation" << std::endl;
          std::cerr << "SubDifferential: " << subDifferential
                    << ",   FD: [" << backwardFDGradient << ", " << forwardFDGradient << "]" << std::endl;
          std::cerr << "Origin:" << std::endl << origin << std::endl;
          std::cerr << "Test direction:" << std::endl << testDirection << std::endl;
          std::cerr << "Parameter value: " << v << std::endl;
          DUNE_THROW(MathError,"");
        }
      }
    }
  }
}


}   // namespace TNNMG

}   // namespace Dune

#endif   // DUNE_TNNMG_FUNCTIONALS_FUNCTIONALTEST_HH
