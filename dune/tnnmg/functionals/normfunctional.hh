#ifndef DUNE_TNNMG_FUNCTIONALS_NORMFUNCTIONAL_HH
#define DUNE_TNNMG_FUNCTIONALS_NORMFUNCTIONAL_HH

#include <limits>

#include <dune/common/exceptions.hh>

#include <dune/solvers/common/interval.hh>
#include <dune/solvers/common/resize.hh>

namespace Dune {

namespace TNNMG {

template <class V, class R=double>
class ShiftedNormFunctional;

/** \brief Coordinate restriction of the Euclidean norm functional
 *
 *  This general implementation doesn't really allow anything but shifting.
 *  The shifted version be then be restricted further.
 *
 *  \tparam V Type of the vector before restriction
 *  \tparam R Range type
 *  \tparam I Index type used to access individual vector entries
 */
template <class V, class R, class I = std::size_t>
class NormCoordinateRestriction
{
public:

  using GlobalVector = V;
  using GlobalIndex = I;

  using Vector = std::decay_t<decltype(std::declval<GlobalVector>()[std::declval<GlobalIndex>()])>;

  using Range = R;

  NormCoordinateRestriction(const GlobalVector& coefficients, const GlobalVector& origin, GlobalIndex index)
  : coefficients_(&coefficients),
    origin_(origin),
    index_(index)
  {}

  Range operator()(const Vector& v) const
  {
    DUNE_THROW(Dune::NotImplemented, "Evaluation of NormCoordinateRestriction not implemented");
  }

  GlobalIndex index() const
  {
    return index_;
  }

  const auto& coefficients() const
  {
    return (*coefficients_)[index_];
  }

  template<class Origin,
  std::enable_if_t<! Dune::IsNumber<Origin>::value, int> = 0>
  friend ShiftedNormFunctional<Vector, Range>
  shift(const NormCoordinateRestriction& f, const Origin& origin)
  {
    return ShiftedNormFunctional<Vector, Range>(f.coefficients(),
                                                f.origin_[f.index_],   // origin of the functional before this shift
                                                origin);   // new additional shift, with reference semantics
  }

protected:

  const V* coefficients_;

  const GlobalVector origin_;
  const GlobalIndex index_;
};

/** \brief Coordinate restriction of Euclidean norm functional to a scalar function
 *
 *  This is the specialization for the restriction to 1-dimensional subspaces.
 *  Unlike the general implementation above, this specialization can produce the subdifferential
 *  of the functional.
 *
 *  \tparam V Type of the vector before restriction
 *  \tparam R Range type
 *  \tparam I Index type used to access individual vector entries
 *
 */
template <int blocksize, class R, class I>
class NormCoordinateRestriction<FieldVector<double,blocksize>, R, I>
{
public:

  using GlobalVector = FieldVector<double,blocksize>;
  using GlobalIndex = I;

  using Vector = std::decay_t<decltype(std::declval<GlobalVector>()[std::declval<GlobalIndex>()])>;

  using Range = R;

  NormCoordinateRestriction(const GlobalVector& coefficients, const GlobalVector& origin, GlobalIndex index)
  : coefficients_(&coefficients),
    origin_(origin),
    index_(index)
  {}

  Range operator()(const Vector& v) const
  {
    auto tmp = origin_;
    tmp[index_] += v;

    return tmp.two_norm();
  }

  GlobalIndex index() const
  {
    return index_;
  }

  const auto& coefficients() const
  {
    return (*coefficients_)[index_];
  }

  void domain(Solvers::Interval<double>& d) const
  {
    // Our domain is the entire space
    d[0] = -std::numeric_limits<double>::infinity();
    d[1] =  std::numeric_limits<double>::infinity();
  }

  void subDiff(double z, Solvers::Interval<double>& dJ) const
  {
    constexpr double tolerance = 1e-15;

    dJ = 0.0;

    // Compute norm at coefficient i
    auto tmp = *origin_;
    tmp[index_] += z;

    double norm = tmp.two_norm();

    if (norm < tolerance)
    {
      // We are at 0 -- the result is a true subdifferential
      dJ[0] -= (*coefficients_)[0];
      dJ[1] += (*coefficients_)[0];
    }
    else
    {
      // Compute the j-th entry of the gradient
      dJ += (*coefficients_)[0] * tmp[index_] / norm;
    }
  }

protected:

  const GlobalVector* coefficients_;

  const GlobalVector origin_;
  const GlobalIndex index_;
};



/** \brief Shifted Euclidean norm functional
 *
 *  \tparam V global vector type
 *  \tparam R Range type
 */
template <class V, class R>
class ShiftedNormFunctional
{
public:

  using Vector = V;
  using Range = R;

  ShiftedNormFunctional(const Vector& coefficients, const Vector& origin, const Vector& offset)
  : coefficients_(&coefficients),
    origin_(origin),
    offset_(&offset)
  {}

  Range operator()(const Vector& v) const
  {
    DUNE_THROW(Dune::NotImplemented, "Evaluation of ShiftedNormFunctional not implemented");
  }

  /** \brief Tell the functional that the origin has changed */
  void updateOrigin()
  {
    // Does not do anything, because no information is precomputed
  }

  void updateOrigin(std::size_t i)
  {
    // Does not do anything, because no information is precomputed
  }

  template <class Index>
  friend NormCoordinateRestriction<Vector, Range, Index>
  coordinateRestriction(const ShiftedNormFunctional& f, const Index& i)
  {
    auto tmp = f.origin_;
    tmp += *f.offset_;
    return NormCoordinateRestriction<Vector, Range, Index>(*f.coefficients_, tmp, i);
  }

protected:
  const Vector* coefficients_;
  const Vector origin_;
  const Vector* offset_;
};


/** \brief Directional restriction of the Euclidean norm functional
 *
 *  \tparam V global vector type
 *  \tparam R Range type
 */
template<class V, class R=double>
class NormDirectionalRestriction
{
public:

  using GlobalVector = V;

  using Vector = typename GlobalVector::field_type;
  using Range = R;

  NormDirectionalRestriction(const GlobalVector& coefficients, const GlobalVector& origin, const GlobalVector& direction)
  : coefficients_(&coefficients),
    origin_(&origin),
    direction_(&direction)
  {}

  Range operator()(const Vector& v) const
  {
    Range result(0.0);

    for(size_t i=0; i<(*origin_).size(); ++i)
    {
      // Extract the plastic strain components into a separate data structure, for easier readability
      auto p = (*origin_)[i];
      p.axpy(v, (*direction_)[i]);
      result += (*coefficients_)[i][0] * p.two_norm();
    }

    return result;
  }

  GlobalVector& origin()
  {
    return *origin_;
  }

  const GlobalVector& origin() const
  {
    return *origin_;
  }

  void domain(Dune::Solvers::Interval<double>& d) const
  {
    d[0] = -std::numeric_limits<double>::max();
    d[1] =  std::numeric_limits<double>::max();
  }

  Solvers::Interval<double> subDifferential(double z) const
  {
    Solvers::Interval<double> result(0.0);

    double tolerance = 1e-15;

    for (size_t row=0; row<origin_->size(); ++row)
    {
      auto p = (*origin_)[row];
      p.axpy(z, (*direction_)[row]);

      auto norm = p.two_norm();

      if (norm >= tolerance)
      {
        // Functional is differentiable here -- compute the standard directional derivative
        // We compute the directional derivative as the product of the gradient and the direction
        FieldVector<double,p.size()> gradient(0);
        for (size_t i=0; i<p.size(); i++)
          gradient[i] += (*coefficients_)[row][0] * p[i] / norm;

        result += gradient * (*direction_)[row];
      }
      else
      {
        result[0] -= (*coefficients_)[row][0] * (*direction_)[row].two_norm();
        result[1] += (*coefficients_)[row][0] * (*direction_)[row].two_norm();
      }
    }

    return result;

  }

protected:
  const GlobalVector* coefficients_;
  const GlobalVector* origin_;
  const GlobalVector* direction_;
};



/** \brief The weighted sum of the Euclidean norm of the vector blocks
 *
 * This Functional class implements the functional
 * \f[
 *    j(v) = \sum_{i=1}^n a_i |v_i|,
 * \f]
 *where \f$ v \f$ is a vector in \f$ (\mathbb{R}^N)^n \f$, and \f$ a \in \mathbb{R}^n \f$
 * is a vector of coefficients.
 *
 *  \tparam V Vector type
 *  \tparam R Range type
 */
template<class V, class R=double>
class NormFunctional
{
public:

  using Vector = V;
  using Range = R;

  NormFunctional(const Vector& coefficients) :
  coefficients_(coefficients)
  {}

  Range operator()(const Vector& v) const
  {
    Range result(0.0);

    for (size_t i=0; i<v.size(); ++i)
      result += coefficients_[i][0] * v[i].two_norm();

    return result;
  }

  const Vector& coefficients() const
  {
    return coefficients_;
  }

  friend auto directionalRestriction(const NormFunctional& f, const Vector& origin, const Vector& direction)
  {
    return NormDirectionalRestriction<Vector, Range>(f.coefficients_, origin, direction);
  }

  friend ShiftedNormFunctional<Vector, Range>
    shift(const NormFunctional& f, const Vector& origin)
  {
    Vector zero;
    Solvers::resizeInitialize(zero, origin, 0.0);
    return ShiftedNormFunctional<Vector, Range>(f.coefficients_, std::move(zero), origin);
  }

protected:
  const Vector coefficients_;
};

}  // namespace TNNMG

}  // namespace Dune
#endif

