#ifndef DUNE_TNNMG_FUNCTIONALS_CONVEXFUNCTIONAL_HH
#define DUNE_TNNMG_FUNCTIONALS_CONVEXFUNCTIONAL_HH

namespace Dune {

  namespace TNNMG {

    /** \brief Convex functional from (R^N)^n to R
     *
     *
     *  \tparam VectorTypeTEMPLATE The type used for elements of (R^N)^n
     */
    template<class VectorTypeTEMPLATE>
    class ConvexFunctional
    {
    public:
      //! Export the employed block vector type
      typedef VectorTypeTEMPLATE VectorType;

      /** \brief Evaluates the functional at a given vector u
       *
       *  \param[in] u vector to evaluate the energy at
       *  \returns computed energy
       */
      virtual double operator()(const VectorType& u) const = 0;
    };

  }

}

#endif
