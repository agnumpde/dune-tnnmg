// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_TNNMG_FUNCTIONAL_QUADRATICFUNCTIONALCONSTRAINEDLINEARIZATION_HH
#define DUNE_TNNMG_FUNCTIONAL_QUADRATICFUNCTIONALCONSTRAINEDLINEARIZATION_HH


#include <cstddef>



namespace Dune {
namespace TNNMG {



/**
 * \brief A constrained linearization for QuadraticFunctional
 *
 * ATTENTION: This is currently only implemented for scalar valued vectors.
 */
template<class F, class BV>
class QuadraticFunctionalConstrainedLinearization
{
public:
    using Matrix = typename F::Matrix;
    using Vector = typename F::Vector;
    using BitVector = BV;
    using ConstrainedMatrix = Matrix;
    using ConstrainedVector = Vector;
    using ConstrainedBitVector = BitVector;


    QuadraticFunctionalConstrainedLinearization(const F& f, const BitVector& ignore) :
        f_(f),
        ignore_(&ignore),
        truncationTolerance_(1e-10)
    {}

    /** \brief Specify what degrees of freedom to ignore
     */
    void setIgnore(const BitVector& ignore)
    {
      ignore_ = &ignore;
    }

    void bind(const Vector& x)
    {
        negativeGradient_ = derivative(f_)(x);
        negativeGradient_ *= -1;
        hessian_ = derivative(derivative(f_))(x);
        truncationFlags_ = *ignore_;

        // truncate
        for(std::size_t i=0; i<x.size(); ++i)
        {
            auto it = hessian_[i].begin();
            auto end = hessian_[i].end();
            for(; it!=end; ++it)
                if (truncationFlags_[i].any() || truncationFlags_[it.index()].any())
                    *it = 0;
            if (truncationFlags_[i].any())
            {
                negativeGradient_[i] = 0;
                hessian_[i][i] = 1;
            }
        }
    }

    void extendCorrection(ConstrainedVector& cv, Vector& v) const
    {
        // ATTENTION this is restricted to scalar problems currently
        for(std::size_t i=0; i<v.size(); ++i)
        {
            if (truncationFlags_[i].any())
                v[i] = 0;
            else
                v[i] = cv[i];
        }
    }

    const BitVector& truncated() const
    {
        return truncationFlags_;
    }

    const auto& negativeGradient() const
    {
        return negativeGradient_;
    }

    const auto& hessian() const
    {
        return hessian_;
    }

private:
    const F& f_;
    const BitVector* ignore_;

    double truncationTolerance_;

    Vector negativeGradient_;
    Matrix hessian_;
    BitVector truncationFlags_;
};



} // end namespace TNNMG
} // end namespace Dune




#endif // DUNE_TNNMG_FUNCTIONAL_QUADRATICFUNCTIONALCONSTRAINEDLINEARIZATION
