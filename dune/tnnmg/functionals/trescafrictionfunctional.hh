// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set ts=8 sw=2 et sts=2:
#ifndef DUNE_TNNMG_FUNCTIONALS_TRESCAFRICTIONFUNCTIONAL_HH
#define DUNE_TNNMG_FUNCTIONALS_TRESCAFRICTIONFUNCTIONAL_HH

#include <dune/common/bitsetvector.hh>
#include <dune/common/typeutilities.hh>
#include <dune/common/typetraits.hh>

#include <dune/matrix-vector/algorithm.hh>

#include <dune/solvers/common/interval.hh>
#include <dune/solvers/common/resize.hh>

#include <dune/tnnmg/functionals/nonsmoothconvexfunctional.hh>


template <class V, class R=double>
class ShiftedTrescaFrictionFunctional;


/** \brief Coordinate restriction of a Tresca friction functional
 *
 *  \tparam V global vector type
 *  \tparam R Range type
 *  \tparam I Index type used to access individual vector entries
 */
template <class V, class R, class I = std::size_t>
class TrescaFrictionCoordinateRestriction
{
public:

  using GlobalVector = V;
  using GlobalIndex = I;

  using Vector = std::decay_t<decltype(std::declval<GlobalVector>()[std::declval<GlobalIndex>()])>;

  using Range = R;

  TrescaFrictionCoordinateRestriction(const GlobalVector& coefficients,
                                      const GlobalVector& origin,
                                      GlobalIndex index)
  : coefficients_(&coefficients),
    origin_(origin),
    index_(index)
  {}

  Range operator()(const Vector& v) const
  {
    DUNE_THROW(Dune::NotImplemented, "Evaluation of TrescaFrictionCoordinateRestriction not implemented");
  }

  GlobalIndex index() const
  {
    return index_;
  }

  const auto& coefficients() const
  {
    return (*coefficients_)[index_];
  }

  template<class Origin,
    std::enable_if_t<! Dune::IsNumber<Origin>::value, int> = 0>
  friend ShiftedTrescaFrictionFunctional<Vector, Range>
    shift(const TrescaFrictionCoordinateRestriction& f, const Origin& origin)
  {
    return ShiftedTrescaFrictionFunctional<Vector, Range>(f.coefficients(),
                                                          f.origin_[f.index_],   // origin of the functional before this shift
                                                          origin);   // new additional shift, with reference semantics
  }

protected:
  const GlobalVector* coefficients_;
  const GlobalVector origin_;
  const GlobalIndex index_;
};

/** \brief Coordinate restriction of a Tresca friction functional
 *
 *  \tparam blocksize The number of scalar entries of one vector block
 *  \tparam R Range type
 *  \tparam I Index type used to access individual vector entries
 */
template <int blocksize, class R, class I>
class TrescaFrictionCoordinateRestriction<Dune::FieldVector<double,blocksize>, R, I>
{
  /** \todo Use the implementation in TrescaFrictionFunctional instead */
  template <int dim>
  static Dune::FieldVector<double,dim-1> tangentialPart(const Dune::FieldVector<double,dim>& v)
  {
    Dune::FieldVector<double,dim-1> result;
    std::copy(v.begin()+1,v.end(),result.begin());
    return result;
  }

public:

  using GlobalVector = Dune::FieldVector<double,blocksize>  ;
  using GlobalIndex = I;

  using Vector = std::decay_t<decltype(std::declval<GlobalVector>()[std::declval<GlobalIndex>()])>;

  using Range = R;

  TrescaFrictionCoordinateRestriction(const GlobalVector& coefficients,
                                      const GlobalVector& origin,
                                      GlobalIndex index)
  : coefficients_(&coefficients),
    origin_(origin),
    index_(index)
  {}

  Range operator()(const Vector& v) const
  {
    // BUG: This code here omits all contributions from other than the local blocks
    // AFAICT that's okay, because this method is not used by the actual solver.
    auto tmp = origin_;
    tmp[index_] += v;

    assert(coefficients_->size()==1);
    return (*coefficients_)[0] * tangentialPart(tmp).two_norm();
  }

  GlobalIndex index() const
  {
    return index_;
  }

  const auto& coefficients() const
  {
    return (*coefficients_)[index_];
  }

  Dune::Solvers::Interval<double> domain() const
  {
    // Our domain is the entire space
    return Dune::Solvers::Interval<double>{-std::numeric_limits<double>::infinity(),
                                            std::numeric_limits<double>::infinity()};
  }

  Dune::Solvers::Interval<double> subDifferential(double z) const
  {
    constexpr double tolerance = 1e-15;

    Dune::Solvers::Interval<double> dJ = 0.0;

    if (index_!=0)
    {
      // Compute current tangential displacement
      auto displacement = origin_;
      displacement[index_] += z;
      double tangentialNorm = tangentialPart(displacement).two_norm();
      if (tangentialNorm < tolerance)
      {
        dJ[0] -= (*coefficients_)[0];
        dJ[1] += (*coefficients_)[0];
      }
      else
        dJ += (*coefficients_)[0] * displacement[index_] / tangentialNorm;
    }
    return dJ;
  }

protected:
  const GlobalVector* coefficients_;
  const GlobalVector origin_;
  const GlobalIndex index_;
};



/** \brief Shifted Tresca friction functional
 *
 *  \tparam V global vector type
 *  \tparam R Range type
 */
template <class V, class R>
class ShiftedTrescaFrictionFunctional
{
public:

  using Vector = V;
  using Range = R;

  ShiftedTrescaFrictionFunctional(const Vector& coefficients,
                                  const Vector& origin,
                                  const Vector& offset)
  : coefficients_(&coefficients),
    origin_(origin),
    offset_(&offset)
  {}

  Range operator()(const Vector& v) const
  {
    DUNE_THROW(Dune::NotImplemented, "Evaluation of ShiftedTrescaFrictionFunctional not implemented");
  }

  void updateOrigin()
  {
    // Does not do anything, because no information is precomputed
  }

  void updateOrigin(std::size_t i)
  {
    // Does not do anything, because no information is precomputed
  }

  template<class Index>
  friend TrescaFrictionCoordinateRestriction<Vector, Range, Index>
    coordinateRestriction(const ShiftedTrescaFrictionFunctional& f, const Index& i)
  {
    auto tmp = f.origin_;
    tmp += *f.offset_;
    return TrescaFrictionCoordinateRestriction<Vector, Range, Index>(*f.coefficients_, tmp, i);
  }

protected:
  const Vector* coefficients_;
  const Vector origin_;
  const Vector* offset_;
};


/** \brief Directional restriction of the Tresca friction functional
 *
 *  \tparam V Global vector type
 *  \tparam R Range type
 */
template <class V, class R=double>
class TrescaFrictionDirectionalRestriction
{
  /** \todo Use the implementation in TrescaFrictionFunctional instead */
  template <int dim>
  static Dune::FieldVector<double,dim-1> tangentialPart(const Dune::FieldVector<double,dim>& v)
  {
    Dune::FieldVector<double,dim-1> result;
    std::copy(v.begin()+1,v.end(),result.begin());
    return result;
  }

public:

  using GlobalVector = V;

  using Vector = typename GlobalVector::field_type;
  using Range = R;

  TrescaFrictionDirectionalRestriction(const GlobalVector& coefficients,
                                       const GlobalVector& origin,
                                       const GlobalVector& direction)
  : coefficients_(&coefficients),
    origin_(&origin),
    direction_(&direction)
  {}

  Range operator()(const Vector& v) const
  {
    Range result = 0.0;

    auto globalPos = *origin_;
    globalPos.axpy(v, *direction_);

    // Norm part (i.e., the actual friction)
    for (size_t row=0; row<origin_->size(); ++row)
    {
      auto p = (*origin_)[row];
      p.axpy(v, (*direction_)[row]);
      result += (*coefficients_)[row][0] * tangentialPart(p).two_norm();
    }

    return result;
  }

  Dune::Solvers::Interval<double> domain() const
  {
    // Our domain is the entire space
    return Dune::Solvers::Interval<double>{-std::numeric_limits<double>::infinity(),
                                            std::numeric_limits<double>::infinity()};
  }

  Dune::Solvers::Interval<double> subDifferential(double z) const
  {
    // \todo It should be possible to set this globally
    double tolerance = 1e-15;

    Dune::Solvers::Interval<double> dJ = 0.0;

    for(size_t row=0; row<origin_->size(); ++row)
    {
      auto p = (*origin_)[row];
      p.axpy(z,(*direction_)[row]);

      double tangentialNorm = tangentialPart(p).two_norm();

      if (tangentialNorm >= tolerance){
        for (std::size_t j=1; j<(*origin_)[row].size(); j++) {
          dJ[0] += (*coefficients_)[row][0] * (p[j]*(*direction_)[row][j]) / tangentialNorm;
          dJ[1] += (*coefficients_)[row][0] * (p[j]*(*direction_)[row][j]) / tangentialNorm;
        }
      } else {
        for (std::size_t j=1; j<(*origin_)[row].size(); j++) {
          dJ[0] += (*coefficients_)[row][0] * (*direction_)[row][j];
          dJ[1] -= (*coefficients_)[row][0] * (*direction_)[row][j];
        }
      }
    }
    return dJ;
  }

protected:
  const GlobalVector* coefficients_;
  const GlobalVector* origin_;
  const GlobalVector* direction_;
};



/** \brief Functional of linear elasticity with one-sided contact and Tresca friction
 *
 *  \tparam V Vector type
 *  \tparam R Range type
 */
template <class V, class R=double>
class TrescaFrictionFunctional
{
  template <int dim>
  static Dune::FieldVector<double,dim-1> tangentialPart(const Dune::FieldVector<double,dim>& v)
  {
    Dune::FieldVector<double,dim-1> result;
    std::copy(v.begin()+1,v.end(),result.begin());
    return result;
  }

public:

  using Vector = V;
  using Range = R;

  TrescaFrictionFunctional(const Vector& coefficients)
  : coefficients_(coefficients)
  {}

  Range operator()(const Vector& v) const
  {
    Range result = 0.0;

    // Norm part (i.e., the actual friction)
    for (size_t row=0; row<v.size(); ++row)
      result += coefficients_[row][0] * tangentialPart(v[row]).two_norm();

    return result;
  }

  const Vector& coefficients() const
  {
    return coefficients_;
  }

  friend auto directionalRestriction(const TrescaFrictionFunctional& f, const Vector& origin, const Vector& direction)
  {
    return TrescaFrictionDirectionalRestriction<Vector, Range>(f.coefficients_,
                                                              origin, direction);
  }

  friend ShiftedTrescaFrictionFunctional<Vector, Range>
    shift(const TrescaFrictionFunctional& f, const Vector& origin)
  {
    Vector zero;
    Dune::Solvers::resizeInitialize(zero, origin, 0.0);
    return ShiftedTrescaFrictionFunctional<Vector, Range>(f.coefficients_, std::move(zero), origin);
  }

protected:
  const Vector coefficients_;
};

#endif

