// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set ts=8 sw=2 et sts=2:
#ifndef DUNE_TNNMG_FUNCTIONALS_QUADRATICFUNCTIONAL_HH
#define DUNE_TNNMG_FUNCTIONALS_QUADRATICFUNCTIONAL_HH

#include <optional>

#include <dune/common/concept.hh>
#include <dune/common/exceptions.hh>
#include <dune/common/typeutilities.hh>
#include <dune/common/hybridutilities.hh>
#include <dune/common/version.hh>

#include <dune/matrix-vector/algorithm.hh>

#include <dune/solvers/common/resize.hh>
#include <dune/solvers/common/copyorreference.hh>

#include <dune/tnnmg/functionals/affineoperator.hh>
#include <dune/tnnmg/iterationsteps/nonlineargsstep.hh>



namespace Dune {
namespace TNNMG {



template<class M, class V, class R=double>
class ShiftedQuadraticFunctional;

template<class M, class V, class R=double>
class QuadraticFunctional;


/** \brief Coordinate restriction of a quadratic functional
 *
 *  \tparam M Global matrix type
 *  \tparam V global vector type
 *  \tparam R Range type
 */
template<class M, class V, class R>
class ShiftedQuadraticFunctional
{
public:

  using Matrix = M;
  using Vector = V;

  using Range = R;

  ShiftedQuadraticFunctional(const Matrix& quadraticPart, const Vector& linearPart, const Vector& origin) :
    quadraticPart_(&quadraticPart),
    originalLinearPart_(&linearPart),
    origin_(&origin)
  {}

  Range operator()(const Vector& v) const
  {
    Vector w = origin();
    w += v;
    Vector temp;
    Dune::Solvers::resizeInitializeZero(temp,v);
    quadraticPart().umv(w, temp);
    temp *= 0.5;
    temp -= originalLinearPart();
    return temp*w;
  }

  Vector& origin()
  {
    return *origin_;
  }

  const Vector& origin() const
  {
    return *origin_;
  }

  void updateOrigin()
  {}

  void updateOrigin(std::size_t i)
  {}

  const Matrix& quadraticPart() const
  {
    return *quadraticPart_;
  }

  const Vector& originalLinearPart() const
  {
    return *originalLinearPart_;
  }

protected:
  const Matrix* quadraticPart_;
  const Vector* originalLinearPart_;
  const Vector* origin_;
};

// Clang 3.8 refuses to compile this function as a part of the
// ShiftedQuadraticFunctional class declaration/definition because
// in order to determine the return type, we use calls to methods of
// the then incomplete class.
template<class M, class V, class R, class Index>
auto coordinateRestriction(const ShiftedQuadraticFunctional<M,V,R>& f, const Index& i)
{
  using LocalMatrix = std::decay_t<decltype(f.quadraticPart()[i][i])>;
  using LocalVector = std::decay_t<decltype(f.originalLinearPart()[i])>;

  using namespace Dune::MatrixVector;
  namespace H = Dune::Hybrid;

  LocalVector ri = f.originalLinearPart()[i];
  const LocalMatrix* Aii_p = nullptr;

  const auto& Ai = f.quadraticPart()[i];
  sparseRangeFor(Ai, [&](auto&& Aij, auto&& j) {
    // TODO Here we must implement a wrapper to guarantee that this will work with proxy matrices!
#if DUNE_VERSION_LTE(DUNE_COMMON, 2, 9)
    H::ifElse(H::equals(j, i), [&](auto&& id){
#else
    H::ifElse(H::equal_to(j, i), [&](auto&& id){
#endif
      Aii_p = id(&Aij);
#ifdef NEW_TNNMG_COMPUTE_ITERATES_DIRECTLY
    }, [&](auto&& id) {;
      Imp::mmv(Aij, f.origin()[j], ri, PriorityTag<1>());
    });
#else
    });
    Imp::mmv(Aij, f.origin()[j], ri, PriorityTag<1>());
#endif
  });

  return QuadraticFunctional<LocalMatrix&, LocalVector, R>(*Aii_p, std::move(ri));
}

/** \brief Coordinate restriction of a quadratic functional
 *
 *  \tparam M Global matrix type
 *  \tparam V global vector type
 *  \tparam R Range type
 */
template<class M, class V, class R=double>
class QuadraticDirectionalRestriction
{
public:

  using GlobalMatrix = M;
  using GlobalVector = V;

  using Matrix = typename GlobalVector::field_type;
  using Vector = typename GlobalVector::field_type;


  using Range = R;

  QuadraticDirectionalRestriction(const GlobalMatrix& matrix, const GlobalVector& linearTerm, const GlobalVector& origin, const GlobalVector& direction) :
    globalMatrix_(&matrix),
    globalLinearTerm_(&linearTerm),
    origin_(&origin),
    direction_(&direction)
  {
    GlobalVector temp = linearTerm;
    matrix.mv(direction, temp);

    quadraticPart_ = temp*direction;
    linearPart_ = linearTerm*direction - temp*origin;
  }

  Range operator()(const Vector& z) const
  {
    if (not constantPart_.has_value())
    {
      GlobalVector temp = *origin_;
      globalMatrix_->mv(*origin_, temp);
      constantPart_ = .5 * (temp * (*origin_));
    }
    return 0.5 * quadraticPart_*z*z - linearPart_*z + constantPart_.value();
  }

  Range difference(const Vector& z) const
  {
    return 0.5 * quadraticPart_*z*z - linearPart_*z;
  }

  const Matrix& quadraticPart() const
  {
    return quadraticPart_;
  }

  const Vector& linearPart() const
  {
    return linearPart_;
  }

  const GlobalVector& origin() const
  {
    return *origin_;
  }

  const GlobalVector& direction() const
  {
    return *direction_;
  }

protected:
  Matrix quadraticPart_;
  Vector linearPart_;
  const GlobalMatrix* globalMatrix_;
  const GlobalVector* globalLinearTerm_;
  const GlobalVector* origin_;
  const GlobalVector* direction_;
  mutable std::optional<R> constantPart_;
};



/** \brief A quadratic functional
 *
 *  \tparam M Matrix type
 *  \tparam V Vector type
 *  \tparam R Range type
 */
template<class M, class V, class R>
class QuadraticFunctional
{
public:

  using Matrix = std::decay_t<M>;
  using Vector = std::decay_t<V>;
  using Range = R;

  template<class MM, class VV>
  QuadraticFunctional(MM&& matrix, VV&& linearTerm) :
    matrix_(std::forward<MM>(matrix)),
    linearPart_(std::forward<VV>(linearTerm))
  {
    Dune::Solvers::resizeInitializeZero(temp_,linearPart());
  }

  Range operator()(const Vector& v) const
  {
    Dune::Solvers::resizeInitializeZero(temp_,v);
    quadraticPart().umv(v, temp_);
    temp_ *= 0.5;
    temp_ -= linearPart();
    return temp_*v;
  }

  const Matrix& quadraticPart() const
  {
    return matrix_.get();
  }

  const Vector& linearPart() const
  {
    return linearPart_.get();
  }

  /**
   * \brief Access derivative
   */
  friend AffineOperator<Matrix, Vector> derivative(const QuadraticFunctional& f)
  {
    return AffineOperator<Matrix, Vector>(f.quadraticPart(), f.linearPart());
  }

  friend auto directionalRestriction(const QuadraticFunctional& f, const Vector& origin, const Vector& direction)
    -> QuadraticDirectionalRestriction<Matrix, Vector, Range>
  {
    return QuadraticDirectionalRestriction<Matrix, Vector, Range>(f.quadraticPart(), f.linearPart(), origin, direction);
  }

  friend auto shift(const QuadraticFunctional& f, const Vector& origin)
    -> ShiftedQuadraticFunctional<Matrix, Vector, Range>
  {
    return ShiftedQuadraticFunctional<Matrix, Vector, Range>(f.quadraticPart(), f.linearPart(), origin);
  }

protected:

  Solvers::ConstCopyOrReference<M> matrix_;
  Solvers::ConstCopyOrReference<V> linearPart_;
  mutable Vector temp_;
};




} // end namespace TNNMG
} // end namespace Dune

#endif // DUNE_TNNMG_FUNCTIONALS_QUADRATICFUNCTIONAL_HH
