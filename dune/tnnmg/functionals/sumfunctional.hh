#ifndef DUNE_TNNMG_FUNCTIONALS_SUMFUNCTIONAL_HH
#define DUNE_TNNMG_FUNCTIONALS_SUMFUNCTIONAL_HH

#include <memory>
#include <tuple>

#include <dune/common/shared_ptr.hh>
#include "dune/tnnmg/functionals/nonsmoothconvexfunctional.hh"
#include <dune/common/hybridutilities.hh>

namespace Dune {
namespace TNNMG {

// Forward declaration
template<class... F>
class ShiftedSumFunctional;


/** \brief Sum of several functionals
 *
 *  \tparam F List of functionals
 */
template<class... F>
class SumFunctional
{
  using FunctionTuple = std::tuple<F...>;
  using F0 = std::tuple_element_t<0,FunctionTuple>;

public:

  using Functions = FunctionTuple;
  using Vector = typename F0::Vector;
  using Range = typename F0::Range;

  SumFunctional(const F&... f) :
    functions_(f...)
  {}

  Range operator()(const Vector& v) const
  {
    Range y{0};
    Dune::Hybrid::forEach(functions_, [&](auto&& f) {
      y += f(v);
    });
    return y;
  }

  const FunctionTuple& functions() const
  {
    return functions_;
  }

  friend ShiftedSumFunctional<F...>
    shift(const SumFunctional& f, const Vector& origin)
  {
    return std::apply([&](const auto&... args) {
      return ShiftedSumFunctional<F...>(args..., origin);
    }, f.functions());
  }

private:
  FunctionTuple functions_;
};

template<class... F, class Vector>
auto directionalRestriction(const SumFunctional<F...>& f, const Vector& origin, const Vector& direction)
{
  return std::apply([&](const auto&... args) {
    return SumFunctional<decltype(directionalRestriction(args, origin, direction))...>(directionalRestriction(args, origin, direction)...);
  }, f.functions());
}



/** \brief Sum of several functionals
 *
 *  \tparam F List of functionals
 */
template<class... F>
class ShiftedSumFunctional
{
  using FunctionTuple = std::tuple<F...>;
  using F0 = std::tuple_element_t<0,FunctionTuple>;

public:

  using Vector = typename F0::Vector;
  using Range = typename F0::Range;

private:

  using ShiftedFunctionTuple = std::tuple<decltype(shift(std::declval<F>(), std::declval<Vector>()))...>;

public:

  using Functions = ShiftedFunctionTuple;

  ShiftedSumFunctional(const F&... f, const Vector& origin) :
    shiftedFunctions_(shift(f, origin)...)
  {}

  Range operator()(const Vector& v) const
  {
    Range y{0};
    Dune::Hybrid::forEach(shiftedFunctions_, [&](auto&& f) {
      y += f(v);
    });
    return y;
  }

  void updateOrigin()
  {
    Dune::Hybrid::forEach(shiftedFunctions_, [&](auto&& f) {
      f.updateOrigin();
    });
  }

  template<class Index>
  void updateOrigin(Index i)
  {
    Dune::Hybrid::forEach(shiftedFunctions_, [&](auto&& f) {
      f.updateOrigin(i);
    });
  }

  const ShiftedFunctionTuple& functions() const
  {
    return shiftedFunctions_;
  }

private:
  ShiftedFunctionTuple shiftedFunctions_;
};



template<class... F, class Index>
auto coordinateRestriction(const ShiftedSumFunctional<F...>& f, const Index& i)
{
  return std::apply([&](const auto&... args) {
    return SumFunctional<decltype(coordinateRestriction(args, i))...>(coordinateRestriction(args, i)...);
  }, f.functions());
}

} // end namespace TNNMG
} // end namespace Dune

namespace Dune {

  namespace TNNMG {

    template < class LocalVectorType=Dune::FieldVector<double,1>, class LocalMatrixType=Dune::FieldMatrix<double,1,1> >
    class VintageSumFunctional: public NonsmoothConvexFunctional<LocalVectorType, LocalMatrixType>
    {
    public:
      typedef NonsmoothConvexFunctional<LocalVectorType, LocalMatrixType> NonlinearityType;

      using NonlinearityType::block_size;

      typedef typename NonlinearityType::VectorType VectorType;
      typedef typename NonlinearityType::MatrixType MatrixType;
      typedef typename NonlinearityType::IndexSet IndexSet;

      VintageSumFunctional(NonlinearityType& phi, NonlinearityType& psi)
      {
        phi_ = Dune::stackobject_to_shared_ptr(phi);
        psi_ = Dune::stackobject_to_shared_ptr(psi);
      }

      VintageSumFunctional(std::shared_ptr<NonlinearityType>& phi, std::shared_ptr<NonlinearityType>& psi) :
      phi_(phi),
      psi_(psi)
      {}

      double operator()(const VectorType& v) const
      {
        double r = 0.0;
        r += (*phi_)(v);
        r += (*psi_)(v);
        return r;
      }

      void addGradient(const VectorType& v, VectorType& gradient) const
      {
        phi_->addGradient(v, gradient);
        psi_->addGradient(v, gradient);
      }

      void addHessian(const VectorType& v, MatrixType& hessian) const
      {
        phi_->addHessian(v, hessian);
        psi_->addHessian(v, hessian);
      }

      void addHessianIndices(IndexSet& indices) const
      {
        phi_->addHessianIndices(indices);
        psi_->addHessianIndices(indices);
      }

      void directionalDomain(const VectorType& u, const VectorType& v, Dune::Solvers::Interval<double>& dom)
      {
        Dune::Solvers::Interval<double> dom2;
        phi_->directionalDomain(u, v, dom);
        psi_->directionalDomain(u, v, dom2);
        dom[0] = std::max(dom[0],dom2[0]);
        dom[1] = std::min(dom[1],dom2[1]);
      }

      void directionalSubDiff(const VectorType& u, const VectorType& v, Dune::Solvers::Interval<double>& sd)
      {
        Dune::Solvers::Interval<double> sd2;
        phi_->directionalSubDiff(u, v, sd);
        psi_->directionalSubDiff(u, v, sd2);
        sd[0] += sd2[0];
        sd[1] += sd2[1];
      }

      void setVector(const VectorType& v)
      {
        phi_->setVector(v);
        psi_->setVector(v);
      }

      void updateEntry(int i, double x, int j)
      {
        phi_->updateEntry(i, x, j);
        psi_->updateEntry(i, x, j);
      }

      void subDiff(int i, double x, Dune::Solvers::Interval<double>& D, int j) const
      {
        Dune::Solvers::Interval<double> DD;
        phi_->subDiff(i, x, D, j);
        psi_->subDiff(i, x, DD, j);
        D[0] += DD[0];
        D[1] += DD[1];
      }

      double regularity(int i, double x, int j) const
      {
        return phi_->regularity(i, x, j) + psi_->regularity(i, x, j);
      }

      /** \brief The domain of definition, which is the intersection
       * of the domains of definition of the two addends.
       */
      void domain(int i, Dune::Solvers::Interval<double>& dom, int j) const
      {
        Dune::Solvers::Interval<double> dom2;
        phi_->domain(i, dom, j);
        psi_->domain(i, dom2, j);
        if (dom2[0] > dom[0])
          dom[0] = dom2[0];
        if (dom2[1] < dom[1])
          dom[1] = dom2[1];
      }

    private:
      std::shared_ptr<NonlinearityType> phi_;
      std::shared_ptr<NonlinearityType> psi_;
    };

  }   // namespace TNNMG

}   // namespace Dune

#endif
