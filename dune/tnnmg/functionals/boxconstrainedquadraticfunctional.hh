// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set ts=8 sw=2 et sts=2:
#ifndef DUNE_TNNMG_FUNCTIONALS_BOXCONSTRAINEDQUADRATICFUNCTIONAL_HH
#define DUNE_TNNMG_FUNCTIONALS_BOXCONSTRAINEDQUADRATICFUNCTIONAL_HH

#include <cstddef>
#include <type_traits>

#include <dune/common/concept.hh>
#include <dune/common/typetraits.hh>
#include <dune/common/hybridutilities.hh>
#include <dune/common/version.hh>

#include <dune/matrix-vector/algorithm.hh>

#include <dune/solvers/common/copyorreference.hh>

#include <dune/tnnmg/concepts.hh>

#include <dune/tnnmg/iterationsteps/nonlineargsstep.hh>

#include <dune/tnnmg/functionals/quadraticfunctional.hh>





namespace Dune {
namespace TNNMG {



template<class V, class L, class U,
  std::enable_if_t<models<Concept::IsLessThanComparable, V, L>(), int> = 0>
bool checkInsideBox(const V& v, const L& lower, const U& upper)
{
  return (lower <= v) && (v <= upper);
}

template<class V, class L, class U,
  std::enable_if_t<not models<Concept::IsLessThanComparable, V, L>(), int> = 0>
bool checkInsideBox(const V& v, const L& lower, const U& upper)
{
  namespace H = Dune::Hybrid;
  bool result = true;
  H::forEach(H::integralRange(H::size(v)), [&](auto&& i) {
    result &= checkInsideBox(v[i], lower[i], upper[i]);
  });
  return result;
}



template<class V, class L, class U, class T,
  std::enable_if_t<std::is_arithmetic<V>::value, int> = 0>
void directionalObstacles(const V& x, const V& v, const L& lower, const U& upper, T& defectLower, T& defectUpper)
{
  if (v>0)
  {
    defectLower = std::max(defectLower, (lower - x)/v);
    defectUpper = std::min(defectUpper, (upper - x)/v);
  }
  else if (v<0)
  {
    defectLower = std::max(defectLower, (upper - x)/v);
    defectUpper = std::min(defectUpper, (lower - x)/v);
  }
}

template<class V, class L, class U, class T,
  std::enable_if_t<not std::is_arithmetic<V>::value, int> = 0>
void directionalObstacles(const V& x, const V& v, const L& lower, const U& upper, T& defectLower, T& defectUpper)
{
  namespace H = Dune::Hybrid;
  H::forEach(H::integralRange(H::size(v)), [&](auto&& i) {
    directionalObstacles(x[i], v[i], lower[i], upper[i], defectLower, defectUpper);
  });
}



template<class M, class V, class R>
class ShiftedBoxConstrainedQuadraticFunctional;

template<class M, class V, class L, class U, class R>
class BoxConstrainedQuadraticFunctional;




/** \brief Coordinate restriction of a quadratic functional
 *
 *  \tparam M Global matrix type
 *  \tparam V global vector type
 *  \tparam R Range type
 */
template<class M, class V, class R>
class ShiftedBoxConstrainedQuadraticFunctional : public
  ShiftedQuadraticFunctional<M,V,R>
{
  using Base = ShiftedQuadraticFunctional<M,V,R>;
public:

  using Matrix = M;
  using Vector = V;
  using LowerObstacle = Vector;
  using UpperObstacle = Vector;
  using Range = R;

  ShiftedBoxConstrainedQuadraticFunctional(const Matrix& quadraticPart, const Vector& linearPart, const Vector& lowerObstacle, const Vector& upperObstacle, const Vector& origin) :
    Base(quadraticPart, linearPart, origin),
    originalLowerObstacle_(&lowerObstacle),
    originalUpperObstacle_(&upperObstacle)
  {}

  Range operator()(const Vector& v) const
  {
    auto temp = Base::origin();
    temp += v;
    if (checkInsideBox(temp, originalLowerObstacle(), originalUpperObstacle()))
      return Base::operator()(v);
    return std::numeric_limits<Range>::max();
  }

  const Vector& originalLowerObstacle() const
  {
    return *originalLowerObstacle_;
  }

  const Vector& originalUpperObstacle() const
  {
    return *originalUpperObstacle_;
  }

protected:
  const Vector* originalLowerObstacle_;
  const Vector* originalUpperObstacle_;
};

// \ToDo This should be an inline friend of ShiftedBoxConstrainedQuadraticFunctional
// but gcc-4.9.2 shipped with debian jessie does not accept
// inline friends with auto return type due to bug-59766.
// Notice, that this is fixed in gcc-4.9.3.
template<class Index, class M, class V, class R>
auto coordinateRestriction(const ShiftedBoxConstrainedQuadraticFunctional<M, V, R>& f, const Index& i)
{
  using Range = R;
  using LocalMatrix = std::decay_t<decltype(f.quadraticPart()[i][i])>;
  using LocalVector = std::decay_t<decltype(f.originalLinearPart()[i])>;
  using LocalLowerObstacle = std::decay_t<decltype(f.originalLowerObstacle()[i])>;
  using LocalUpperObstacle = std::decay_t<decltype(f.originalUpperObstacle()[i])>;

  using namespace Dune::MatrixVector;
  namespace H = Dune::Hybrid;

  LocalVector ri = f.originalLinearPart()[i];
  const LocalMatrix* Aii_p = nullptr;

  const auto& Ai = f.quadraticPart()[i];
  sparseRangeFor(Ai, [&](auto&& Aij, auto&& j) {
    // TODO Here we must implement a wrapper to guarantee that this will work with proxy matrices!
#if DUNE_VERSION_LTE(DUNE_COMMON, 2, 9)
    H::ifElse(H::equals(j, i), [&](auto&& id){
#else
    H::ifElse(H::equal_to(j, i), [&](auto&& id){
#endif
      Aii_p = id(&Aij);
    });
    Imp::mmv(Aij, f.origin()[j], ri, PriorityTag<1>());
  });

  LocalLowerObstacle dli = f.originalLowerObstacle()[i];
  LocalUpperObstacle dui = f.originalUpperObstacle()[i];
  dli -= f.origin()[i];
  dui -= f.origin()[i];

  return BoxConstrainedQuadraticFunctional<LocalMatrix&, LocalVector, LocalLowerObstacle, LocalUpperObstacle, Range>(*Aii_p, std::move(ri), std::move(dli), std::move(dui));
}



/** \brief Coordinate restriction of a quadratic functional
 *
 *  \tparam M Global matrix type
 *  \tparam V Global vector type
 *  \tparam L Global lower obstacle type
 *  \tparam U Global upper obstacle type
 *  \tparam R Range type
 */
template<class M, class V, class L, class U, class R=double>
class BoxConstrainedQuadraticDirectionalRestriction :
  public QuadraticDirectionalRestriction<M,V,R>
{
  using Base = QuadraticDirectionalRestriction<M,V,R>;
public:

  using GlobalMatrix = typename Base::GlobalMatrix;
  using GlobalVector = typename Base::GlobalVector;
  using GlobalLowerObstacle = L;
  using GlobalUpperObstacle = U;

  using Matrix = typename Base::Matrix;
  using Vector = typename Base::Vector;

  using LowerObstacle = Vector;
  using UpperObstacle = Vector;

  using Range = typename Base::Range;

  BoxConstrainedQuadraticDirectionalRestriction(const GlobalMatrix& matrix, const GlobalVector& linearTerm, const GlobalLowerObstacle& lower, const GlobalLowerObstacle& upper, const GlobalVector& origin, const GlobalVector& direction) :
    Base(matrix, linearTerm, origin, direction)
  {
    defectLower_ = -std::numeric_limits<Vector>::max();
    defectUpper_ = std::numeric_limits<Vector>::max();
    directionalObstacles(origin, direction, lower, upper, defectLower_, defectUpper_);
  }

  Range operator()(const Vector& v) const
  {
    if (checkInsideBox(v, lowerObstacle(), upperObstacle()))
      return Base::operator()(v);
    return std::numeric_limits<Range>::max();
  }

  const LowerObstacle& lowerObstacle() const
  {
    return defectLower_;
  }

  const UpperObstacle& upperObstacle() const
  {
    return defectUpper_;
  }

protected:
  LowerObstacle defectLower_;
  UpperObstacle defectUpper_;
};



/** \brief Box constrained quadratic functional
 *
 *  \tparam M Matrix type
 *  \tparam V Vector type
 *  \tparam L Lower obstacle type
 *  \tparam U Upper obstacle type
 *  \tparam R Range type
 */
template<class M, class V, class L, class U, class R>
class BoxConstrainedQuadraticFunctional :
  public QuadraticFunctional<M,V,R>
{
  using Base = QuadraticFunctional<M,V,R>;

public:

  using Matrix = typename Base::Matrix;
  using Vector = typename Base::Vector;
  using Range = typename Base::Range;
  using LowerObstacle = std::decay_t<L>;
  using UpperObstacle = std::decay_t<U>;

  template <class MM, class VV, class LL, class UU>
  BoxConstrainedQuadraticFunctional(MM&& matrix, VV&& linearPart, LL&& lower, UU&& upper) :
    Base(std::forward<MM>(matrix), std::forward<VV>(linearPart)),
    lower_(std::forward<LL>(lower)),
    upper_(std::forward<UU>(upper))
  {}

  Range operator()(const Vector& v) const
  {
    if (checkInsideBox(v, lower_.get(), upper_.get()))
      return Base::operator()(v);
    return std::numeric_limits<Range>::max();
  }

  const LowerObstacle& lowerObstacle() const
  {
    return lower_.get();
  }

  const UpperObstacle& upperObstacle() const
  {
    return upper_.get();
  }

  friend auto directionalRestriction(const BoxConstrainedQuadraticFunctional& f, const Vector& origin, const Vector& direction)
    -> BoxConstrainedQuadraticDirectionalRestriction<Matrix, Vector, LowerObstacle, UpperObstacle, Range>
  {
    return BoxConstrainedQuadraticDirectionalRestriction<Matrix, Vector, LowerObstacle, UpperObstacle, Range>(f.quadraticPart(), f.linearPart(), f.lowerObstacle(), f.upperObstacle(), origin, direction);
  }

  friend auto shift(const BoxConstrainedQuadraticFunctional& f, const Vector& origin)
    -> ShiftedBoxConstrainedQuadraticFunctional<Matrix, Vector, Range>
  {
    return ShiftedBoxConstrainedQuadraticFunctional<Matrix, Vector, Range>(f.quadraticPart(), f.linearPart(), f.lowerObstacle(), f.upperObstacle(), origin);
  }

protected:

  Solvers::ConstCopyOrReference<L> lower_;
  Solvers::ConstCopyOrReference<U> upper_;
};



} // end namespace TNNMG
} // end namespace Dune

#endif // DUNE_TNNMG_FUNCTIONALS_BOXCONSTRAINEDQUADRATICFUNCTIONAL_HH
