// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_TNNMG_FUNCTIONALS_AFFINEOPERATOR_HH
#define DUNE_TNNMG_FUNCTIONALS_AFFINEOPERATOR_HH



namespace Dune {
namespace TNNMG {

namespace Imp {

  template<class MM, class X, class Y>
  static auto mmv(const MM& m, const X& x, Y&& y, PriorityTag<1>) -> void_t<decltype(m.mmv(x, y))>
  {
    m.mmv(x, y);
  }

  template<class MM, class X, class Y>
  static void mmv(const MM& m, const X& x, Y&& y, PriorityTag<0>)
  {
    y -= m*x;
  }
} // namespace Imp


/** \brief The first derivative of a functional
 *
 * The functional is thought to be smooth here: truncation and other ways to deal
 * with nonsmoothness happens elsewhere.
 *
 * \tparam M Matrix type
 * \tparam V Vector type
 */
template<class M, class V>
class AffineOperator
{
  /** \brief The second derivative of a functional: A fixed matrix wrapped as a function */
  class ConstantMatrixFunction
  {
  public:
    ConstantMatrixFunction(const M& value) :
      value_(&value)
    {}

    const M& operator()(const V& d)
    { return *value_; }

  private:
    const M* value_;
  };

public:

  using Matrix = M;
  using Vector = V;

  /** \brief Constructor */
  AffineOperator(const Matrix& matrix, const Vector& linearTerm) :
    matrix_(&matrix),
    linearTerm_(&linearTerm)
  {}

  /** \brief Evaluate the first derivative of the functional at a given point */
  Vector operator()(const Vector& v) const
  {
    Vector result;
    Solvers::resizeInitializeZero(result,v);
    matrix_->umv(v, result);
    result -= *linearTerm_;
    return result;
  }

  /** \brief Get the second derivative of the functional
   * \todo This could be a lambda but gcc-4.9.2 (shipped with Debian
   * Jessie) does not accept inline friends with auto return type
   * due to bug-59766. This is fixed in gcc-4.9.3.
   */
  friend ConstantMatrixFunction derivative(const AffineOperator& f)
  {
    return ConstantMatrixFunction(*f.matrix_);
  }

protected:
  const Matrix* matrix_;
  const Vector* linearTerm_;
};


}   // TNNMG
}   // Dune

#endif   // DUNE_TNNMG_FUNCTIONALS_AFFINEOPERATOR_HH
