// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_TNNMG_FUNCTIONAL_BCQFCONSTRAINEDLINEARIZATION_HH
#define DUNE_TNNMG_FUNCTIONAL_BCQFCONSTRAINEDLINEARIZATION_HH


#include <cstddef>

#include <dune/common/typetraits.hh>
#include <dune/common/hybridutilities.hh>

#include <dune/matrix-vector/algorithm.hh>



namespace Dune {
namespace TNNMG {



/**
 * \brief A constrained linearization for BoxConstrainedQuadraticFunctional
 */
template<class F, class BV>
class BoxConstrainedQuadraticFunctionalConstrainedLinearization
{
  using This = BoxConstrainedQuadraticFunctionalConstrainedLinearization<F,BV>;

  template<class NV, class NBV, class T>
  static void determineTruncation(const NV& x, const NV& lower, const NV& upper, NBV&& truncationFlags, const T& truncationTolerance)
  {
    namespace H = Dune::Hybrid;
    if constexpr (IsNumber<NV>())
    {
      if ((x <= lower+truncationTolerance) || (x >= upper - truncationTolerance))
        truncationFlags = true;
    }
    else
    {
      H::forEach(H::integralRange(H::size(x)), [&](auto&& i) {
        This::determineTruncation(x[i], lower[i], upper[i], truncationFlags[i], truncationTolerance);
      });
    }
  }

  template<class NV, class NBV, class T>
  static void determineGradientAwareTruncation(const NV& x, const NV& lower, const NV& upper, const NV& negativeGradient, NBV&& truncationFlags, const T& truncationTolerance)
  {
    namespace H = Dune::Hybrid;
    if constexpr (IsNumber<NV>())
    {
      if (((x <= lower+truncationTolerance) and (negativeGradient<0) ) || ((x >= upper - truncationTolerance) and (negativeGradient>0)))
        truncationFlags = true;
    }
    else
    {
      H::forEach(H::integralRange(H::size(x)), [&](auto&& i) {
        This::determineGradientAwareTruncation(x[i], lower[i], upper[i], negativeGradient[i], truncationFlags[i], truncationTolerance);
      });
    }
  }

  template<class NV, class NBV>
  static void truncateVector(NV& x, const NBV& truncationFlags)
  {
    namespace H = Dune::Hybrid;
    if constexpr (IsNumber<NV>())
    {
      if (truncationFlags)
        x = 0;
    }
    else
    {
      H::forEach(H::integralRange(H::size(x)), [&](auto&& i) {
        This::truncateVector(x[i], truncationFlags[i]);
      });
    }
  }

  template<class NM, class RBV, class CBV>
  static void truncateMatrix(NM& A, const RBV& rowTruncationFlags, const CBV& colTruncationFlags, bool regularizeDiagonal)
  {
    namespace H = Dune::Hybrid;
    using namespace Dune::MatrixVector;
    if constexpr (IsNumber<NM>())
    {
      if (rowTruncationFlags or colTruncationFlags)
      {
        if (regularizeDiagonal)
          A = 1;
        else
          A = 0;
      }
    }
    else
    {
      H::forEach(H::integralRange(H::size(rowTruncationFlags)), [&](auto&& i) {
        auto&& Ai = A[i];
        sparseRangeFor(Ai, [&](auto&& Aij, auto&& j) {
            This::truncateMatrix(Aij, rowTruncationFlags[i], colTruncationFlags[j], regularizeDiagonal and (i==j));
        });
      });
    }
  }

public:
  using Matrix = typename F::Matrix;
  using Vector = typename F::Vector;
  using BitVector = BV;
  using ConstrainedMatrix = Matrix;
  using ConstrainedVector = Vector;
  using ConstrainedBitVector = BitVector;


  /** \brief Construct linearization for a given functional
   */
  BoxConstrainedQuadraticFunctionalConstrainedLinearization(const F& f) :
    f_(f),
    truncationTolerance_(1e-10),
    regularizeDiagonal_(false),
    gradientAwareTruncation_(false)
  {}

  /** \brief Construct linearization for a given functional and set of degrees of freedom to ignore
   */
  BoxConstrainedQuadraticFunctionalConstrainedLinearization(const F& f, const BitVector& ignore) :
    f_(f),
    ignore_(&ignore),
    truncationTolerance_(1e-10),
    regularizeDiagonal_(false),
    gradientAwareTruncation_(false)
  {}

  /** \brief Specify what degrees of freedom to ignore
   */
  void setIgnore(const BitVector& ignore)
  {
    ignore_ = &ignore;
  }

  void setTruncationTolerance(double tolerance)
  {
    truncationTolerance_ = tolerance;
  }

  /**
   * \brief Enable/disable regularization of truncated diagonal entries
   *
   * If this is enabled, diagonal entries of truncated rows are set
   * to 1. In case of nested matrices this is applied recursively,
   * such that only the scalar diagonal entries of nontrivial
   * diagonal blocks are modified. This can be handy if the truncated
   * linearized problem should be treated by a linear solver
   * that is not capable of handling truncated rows and columns.
   * By default this is disabled, such that linear solvers for the
   * linearized problem have to be robust with respect to singular
   * problems due to truncation.
   */
  void enableDiagonalRegularization(bool regularizeDiagonal)
  {
    regularizeDiagonal_ = regularizeDiagonal;
  }

  /**
   * \brief Enable/disable gradient aware truncation
   *
   * If this is disabled (default) entries are truncated
   * if they are close enough to the upper or lower obstacle.
   *
   * If this is enabled, it is additionally checked, if the
   * gradient points inside of the admissible set for those
   * entries. I.e. in order to be truncated, entries
   * touching the lower obstacle are required to have a
   * positive partial derivative, while entries touching
   * the upper obstacle are required to have a negative
   * partial derivative.
   * This may lead to a less pessimistic active set.
   */
  void enableGradientAwareTruncation(bool gradientAwareTruncation)
  {
    gradientAwareTruncation_ = gradientAwareTruncation;
  }

  void bind(const Vector& x)
  {
    negativeGradient_ = derivative(f_)(x);
    negativeGradient_ *= -1;
    hessian_ = derivative(derivative(f_))(x);
    truncationFlags_ = *ignore_;

    // determine which components to truncate
    if (gradientAwareTruncation_)
      determineGradientAwareTruncation(x, f_.lowerObstacle(), f_.upperObstacle(), negativeGradient_, truncationFlags_, truncationTolerance_);
    else
      determineTruncation(x, f_.lowerObstacle(), f_.upperObstacle(), truncationFlags_, truncationTolerance_);

    // truncate gradient and hessian
    truncateVector(negativeGradient_, truncationFlags_);
    truncateMatrix(hessian_, truncationFlags_, truncationFlags_, regularizeDiagonal_);
  }

  void extendCorrection(ConstrainedVector& cv, Vector& v) const
  {
    v = cv;
    truncateVector(v, truncationFlags_);
  }

  const BitVector& truncated() const
  {
    return truncationFlags_;
  }

  const auto& negativeGradient() const
  {
    return negativeGradient_;
  }

  const auto& hessian() const
  {
    return hessian_;
  }

private:
  const F& f_;
  const BitVector* ignore_;

  double truncationTolerance_;
  bool regularizeDiagonal_;
  bool gradientAwareTruncation_;

  Vector negativeGradient_;
  Matrix hessian_;
  BitVector truncationFlags_;
};



} // end namespace TNNMG
} // end namespace Dune




#endif // DUNE_TNNMG_FUNCTIONAL_BCQFCONSTRAINEDLINEARIZATION
