// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_TNNMG_LINEARSOLVERS_LDLSOLVER_HH
#define DUNE_TNNMG_LINEARSOLVERS_LDLSOLVER_HH

#include <memory>

#include <dune/istl/solvers.hh>
#include <dune/istl/ldl.hh>



namespace Dune {
namespace TNNMG {



struct LDLSolver
{
  template<class Matrix, class Vector>
  void operator()(Vector& x, const Matrix& A, const Vector& r) const
  {
#if HAVE_SUITESPARSE_LDL
    Dune::InverseOperatorResult statistics;
    Dune::LDL<Matrix> ldlSolver(A);
    auto mutableR = r;
    ldlSolver.apply(x, mutableR, statistics);
#else
    DUNE_THROW(Dune::NotImplemented, "LDLTSolver is not available without SuiteSparses' LDL.");
#endif
  }
};



} // end namespace TNNMG
} // end namespace Dune



#endif // DUNE_TNNMG_LINEARSOLVERS_LDLSOLVER_HH
