// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_TNNMG_LINEARSOLVERS_ILU0CGSOLVER_HH
#define DUNE_TNNMG_LINEARSOLVERS_ILU0CGSOLVER_HH



#include <dune/istl/preconditioners.hh>
#include <dune/istl/solvers.hh>



namespace Dune {
namespace TNNMG {



class ILU0CGSolver
{
public:

  ILU0CGSolver(double residualReduction, std::size_t maxSteps) :
    residualReduction_(residualReduction),
    maxSteps_(maxSteps)
  {}

  template<class Matrix, class Vector>
  void operator()(Vector& x, const Matrix& A, const Vector& r) const
  {
    Dune::InverseOperatorResult statistics;
    Dune::MatrixAdapter<Matrix,Vector,Vector> op(A);
    Dune::SeqILU<Matrix,Vector,Vector> ilu0(A,0,1.0);
    Dune::CGSolver<Vector> cg(op, ilu0, residualReduction_, maxSteps_, 0);
    x = 0;
    auto mutableR = r;
    cg.apply(x, mutableR, statistics);
  }

private:
  double residualReduction_;
  std::size_t maxSteps_;
};




} // end namespace TNNMG
} // end namespace Dune



#endif // DUNE_TNNMG_LINEARSOLVERS_ILU0CGSOLVER_HH
