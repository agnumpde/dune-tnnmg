// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_TNNMG_LOCALSOLVERS_SCALARQUADRATICSOLVER_HH
#define DUNE_TNNMG_LOCALSOLVERS_SCALARQUADRATICSOLVER_HH



namespace Dune {
namespace TNNMG {



/**
 * \brief A local solver for scalar quadratic problems
 *
 * \todo Add concept check for the function interface
 */
class ScalarQuadraticSolver
{
public:
  template<class Vector, class Functional, class BitVector>
  constexpr void operator()(Vector& x, const Functional& f, const BitVector& ignore) const
  {
    if (not ignore)
      x = f.linearPart()/f.quadraticPart();
  }
};



} // end namespace TNNMG
} // end namespace Dune



#endif // DUNE_TNNMG_LOCALSOLVERS_SCALARQUADRATICSOLVER_HH
