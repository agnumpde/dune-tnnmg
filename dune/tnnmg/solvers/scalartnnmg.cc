#ifdef HAVE_CONFIG_H
# include "config.h"     
#endif

#include <cmath>
#include <iomanip>

#include <dune/common/bitsetvector.hh>
#include <dune/common/timer.hh>
#include <dune/istl/io.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/matrixindexset.hh>

#include <dune/tnnmg/problem-classes/bisection.hh>
#include <dune/tnnmg/problem-classes/nonlinearity.hh>
#include <dune/tnnmg/problem-classes/convexproblem.hh>
#include <dune/tnnmg/problem-classes/directionalconvexfunction.hh>

#include "scalartnnmg.hh"


ScalarTNNMG::ScalarTNNMG(std::vector<Transfer*>& _transfer, const Dune::ParameterTree& _config):
	config(_config),
	transfer(_transfer)
{
	if (config.hasSub("cycle"))
	{
// 		verbosity - output
// 		0    - nothing
// 		>=5  - short, nothing remains
// 		>=10 - short, one line remains
// 		>=20 - long, all remains
// 		>=30 - everything
		verbosity = config.get("verbosity", 0);
		indent = config.get("indent", "");
		print_smoothing_time_flag = config.get("print_smoothing_time", false);
	
		maxIter = config.get("maxiter", 100);
		tol = config.get("tol", 1e-10);
		
		contactOnly = config.get("contactonly", false);
		
		// cycle properties
		cycleType.finepre = config.get("cycle.finepre", 3);
		cycleType.finepost = config.get("cycle.finepost", 3);
		cycleType.linearfinesmoothing_flag = config.get("cycle.linearfinesmoothing", false);
		cycleType.pre = config.get("cycle.pre", 3);
		cycleType.post = config.get("cycle.post", 3);
		cycleType.coarse = config.get("cycle.coarse", 100);
		
		simplegs = config.get("cycle.simplegs", false);
		additiv_flag = config.get("cycle.additive", false);
		
		
		// smoother properties
		smootherType.sgs_flag = config.get("smoother.symmetric", false);
		smootherType.jacobi_flag = config.get("smoother.jacobi", false);
		
		smootherType.gs_acceptance = config.get("smoother.gs_acceptance", 1.0);
		smootherType.gs_tol = config.get("smoother.gs_tol", 1e-15);
		smootherType.gs_safety = config.get("smoother.gs_safety", 1e-14);
		smootherType.nonlinear_jacobi_flag = config.get("smoother.nonlinearjacobi", false);
		smootherType.damp_fine_smoother_flag = config.get("smoother.dampfinesmoother", false);
		
		fast_quadratic_minimize_flag = config.get("smoother.fast_quadratic_minimize", true);
		bisection_flag = config.get("smoother.bisection", true);
		
		
		// nonlinearity handling
		truncate_flag = config.get("nonlinearity.truncate", true);
		truncatefineonly_flag = config.get("nonlinearity.truncatefineonly", false);
		regularity_tol = config.get("nonlinearity.regularity_tol", 1e10);
		obst_tol = config.get("nonlinearity.obst_tol", 1e-15);
		
		if (config.get("nonlinearity.coarseobstacles", "none") == "monotone")
			coarseobstacles = monotone;
		else if (config.get("nonlinearity.coarseobstacles", "none") == "exact")
			coarseobstacles = exact;
		else if (config.get("nonlinearity.coarseobstacles", "none") == "apriori")
			coarseobstacles = apriori;
		else
			coarseobstacles = none;
		
		if (config.get("nonlinearity.exactobstacles", false))
			coarseobstacles = exact;
		if (config.get("nonlinearity.allowcoarseviolation", false))
			coarseobstacles = none;


		// postprocessing
		projectcorrection_flag = config.get("postprocess.projectcorrection", false);
		gsprojectsteps = config.get("postprocess.gsprojectsteps", 0);
		
		linesearchType.enable = config.get("postprocess.linesearch", true);
		linesearchType.acceptance = config.get("postprocess.linesearch_acceptance", 1.0);
		linesearchType.tol = config.get("postprocess.linesearch_tol", 1e-15);
		linesearchType.safety = config.get("postprocess.linesearch_safety", 1e-14);

		experimentalFlags.linesearch_damponly = config.get("postprocess.linesearch_damponly", false);
		experimentalFlags.linesearch_normed = config.get("postprocess.linesearch_normed", true);
		experimentalFlags.linesearch_startbyone = config.get("postprocess.linesearch_startbyone", true);
		experimentalFlags.linesearch_descentonly = config.get("postprocess.linesearch_descentonly", false);
		
		
		// experimental
		activesetvcycle_flag = config.get("experimental.activesetvcycle", false);
		adaptiveactivesetvcycle_flag = config.get("experimental.adaptiveactivesetvcycle", false);
		convexcombination_flag = config.get("experimental.convexcombination", false);
		cg_flag = config.get("experimental.cg", false);
	}
	else
	{
		verbosity = config.get("verbosity", 0);
		indent = config.get("indent", "");
		print_smoothing_time_flag = config.get("print_smoothing_time", false);
	
		maxIter = config.get("maxiter", 100);
		tol = config.get("tol", 1e-10);

		cycleType.finepre = config.get("finepre", 3);
		cycleType.finepost = config.get("finepost", 3);
		cycleType.linearfinesmoothing_flag = config.get("linearfinesmoothing", false);
		cycleType.pre = config.get("pre", 3);
		cycleType.post = config.get("post", 3);
		cycleType.coarse = config.get("coarse", 100);
	
		smootherType.sgs_flag = config.get("symmetric", false);
		smootherType.jacobi_flag = config.get("jacobi", false);
		
		smootherType.gs_acceptance = config.get("gs_acceptance", 1.0);
		smootherType.gs_tol = config.get("gs_tol", 1e-15);
		smootherType.gs_safety = config.get("gs_safety", 1e-14);
		smootherType.nonlinear_jacobi_flag = config.get("nonlinearjacobi", false);
		smootherType.damp_fine_smoother_flag = config.get("dampfinesmoother", false);
		
		truncate_flag = config.get("truncate", true);
		truncatefineonly_flag = config.get("truncatefineonly", true);
		projectcorrection_flag = config.get("projectcorrection", true);
		gsprojectsteps = config.get("gsprojectsteps", 0);
		convexcombination_flag = config.get("convexcombination", false);
		
		activesetvcycle_flag = config.get("activesetvcycle", false);
		adaptiveactivesetvcycle_flag = config.get("adaptiveactivesetvcycle", false);
	
		if (config.get("coarseobstacles", "none") == "monotone")
			coarseobstacles = monotone;
		else if (config.get("coarseobstacles", "none") == "exact")
			coarseobstacles = exact;
		else if (config.get("coarseobstacles", "none") == "apriori")
			coarseobstacles = apriori;
		else
			coarseobstacles = none;
		
		if (config.get("exactobstacles", false))
			coarseobstacles = exact;
		if (config.get("allowcoarseviolation", true))
			coarseobstacles = none;
		
		regularity_tol = config.get("regularity_tol", 1e10);
		
		cg_flag = config.get("cg", false);
		additiv_flag = config.get("additive", false);
		
		linesearchType.enable = config.get("linesearch", true);
		linesearchType.acceptance = config.get("linesearch_acceptance", 1.0);
		linesearchType.tol = config.get("linesearch_tol", 1e-15);
		linesearchType.safety = config.get("linesearch_safety", 1e-14);
		experimentalFlags.linesearch_damponly = config.get("linesearch_damponly", false);
		experimentalFlags.linesearch_normed = config.get("linesearch_normed", true);
		experimentalFlags.linesearch_startbyone = config.get("linesearch_startbyone", true);
		experimentalFlags.linesearch_descentonly = config.get("linesearch_descentonly", false);
		
		
		obst_tol = config.get("obst_tol", 1e-15);
		
		fast_quadratic_minimize_flag = config.get("fast_quadratic_minimize", true);
		simplegs = config.get("simplegs", false);
		contactOnly = config.get("contactonly", false);
		bisection_flag = config.get("bisection", true);
	}
	
	if (verbosity >= 30)
		std::cout << "ScalarTNNMG created" << std::endl;

	return;
}



void ScalarTNNMG::solve(
	ConvexProblem<>& problem,
	const Norm<Vector> &norm,
	int _fine_level,
	int _coarse_level,
	const Dune::BitSetVector<1>& isHanging,
	const Dune::BitSetVector<1>& _isDirichlet,
	Vector* u_exact, std::vector<Transfer*>* _transfer2)
{
	solve(problem.a, problem.A, problem.am, problem.Am, problem.u, problem.f, problem.phi,
		norm, _fine_level, _coarse_level, isHanging, _isDirichlet, u_exact, _transfer2);
}


void ScalarTNNMG::solve(double a1, const Matrix& _mat, double a2, const Vector& _mat_vec, Vector& _u, const Vector& _rhs,
				ScalarNonlinearity& phi, const Norm<Vector> &norm,
				int _fine_level, int _coarse_level, const Dune::BitSetVector<1>& isHanging, const Dune::BitSetVector<1>& _isDirichlet, Vector* u_exact, std::vector<Transfer*>* _transfer2)
{
	if (_transfer2 != 0)
		transfer2 = _transfer2;
	else
		transfer2 = &transfer;

	mat = _mat;
	mat *= a1;
	
	mat_vec = _mat_vec;
	mat_vec *= std::sqrt(std::abs(a2));
	
	rhs = _rhs;
	u = _u;
	
	fine_level = _fine_level;
	coarse_level = _coarse_level;
	
	if (verbosity >= 30)
		std::cout << indent <<"ScalarTNNMG start solve" << std::endl;
	
	A.resize(fine_level+1);
	m.resize(fine_level+1);
	
	v.resize(fine_level+1);
	v_sum.resize(fine_level+1);
	res.resize(fine_level+1);
	lower.resize(fine_level+1);
	upper.resize(fine_level+1);
	
	truncationlist.resize(fine_level+1);
	modificationlist.resize(fine_level+1);
	isDirichlet.resize(fine_level+1);
	
	// setup matrix and vectors an each level
	if (verbosity >= 30)
		std::cout << indent << "ScalarTNNMG setup" << std::endl;
	for(int i = fine_level; i>=coarse_level; --i)
	{
		size_t size;
		
		if (i==fine_level)
		{
			ScalarNonlinearity::IndexSet indices(mat.N(), mat.M());
			
			indices.import(mat);
			phi.addHessianIndices(indices);
			
			indices.exportIdx(A[i]);
			
			A[i] = 0.0;
			A[i] += mat;
			m[i] = mat_vec;
			
			size = A[i].N();
			
			if (_isDirichlet.size()==size)
				isDirichlet[i] = _isDirichlet;
			else
			{
				isDirichlet[i].resize(size);
				isDirichlet[i].unsetAll();
			}
			
		}
		else
		{
			transfer[i]->galerkinRestrictSetOccupation(A[i+1], A[i]);
			transfer[i]->galerkinRestrict(A[i+1], A[i]);
			
			size = A[i].N();
			
			m[i].resize(size);
			transfer[i]->restrict(m[i+1], m[i]);
			
			transfer[i]->restrict(isDirichlet[i+1], isDirichlet[i]);
		}
		
		truncationlist[i].resize(size);
		truncationlist[i].unsetAll();
		
		modificationlist[i].resize(size);
		modificationlist[i].unsetAll();

		lower[i].resize(size);
		lower[i] = -1e100;
		
		upper[i].resize(size);
		upper[i] = 1e100;
		
		res[i].resize(size);
		res[i] = 0.0;
		
		v[i].resize(size);
		v[i] = 0.0;
		
		v_sum[i].resize(size);
		v_sum[i] = 0.0;
		
	}
	
	Dune::BitSetVector<1> contact(mat.N());
	truncation.resize(mat.N());
	contact.unsetAll();
	truncation.unsetAll();
	
#ifdef MMG_ENABLE_EXACT_OBSTACLES
	if (coarseobstacles == exact)
	{
		fineLevelInterpolation.resize(fine_level);
		
		for (int level=fine_level-1; level>=0; --level)
		{
			fineLevelInterpolation[level].resize(transfer[level]->getMatrix().M());
			
			if (level == fine_level-1)
			{
				for (size_t row=0; row < transfer[level]->getMatrix().N(); ++row)
				{
					Matrix::row_type::ConstIterator it = transfer[level]->getMatrix()[row].begin();
					Matrix::row_type::ConstIterator end = transfer[level]->getMatrix()[row].end();
					for(; it!=end; ++it)
						fineLevelInterpolation[level][it.index()][row] = *it;
				}
			}
			else
			{
				for (size_t row=0; row < transfer[level]->getMatrix().N(); ++row)
				{
					Matrix::row_type::ConstIterator it = transfer[level]->getMatrix()[row].begin();
					Matrix::row_type::ConstIterator end = transfer[level]->getMatrix()[row].end();
					for(; it!=end; ++it)
					{
						std::map<int, double>::iterator fineLevelIt = fineLevelInterpolation[level+1][row].begin();
						std::map<int, double>::iterator fineLevelEnd = fineLevelInterpolation[level+1][row].end();
						for (; fineLevelIt!=fineLevelEnd; ++fineLevelIt)
							fineLevelInterpolation[level][it.index()][fineLevelIt->first] = (*it) * (fineLevelIt->second);
					}
				}
			}
		}
		actual_lower = u;
		actual_upper = u;
		actual_lower *= 0.0;
		actual_upper *= 0.0;
	}
#endif
	
	Vector u_old = u;
	
	double nan = 0.0/0.0;
	
	iter = 0;
	err = tol + 1;
	level = fine_level;
	
	if (verbosity >= 5)
	{
		std::cout << indent << "ScalarTNNMG" << std::endl;
		std::cout << indent << "iter truncated project   dist(0,dj(u))     |u-u_old|    |u-u_exact|       stepsize  pre  coarse post   av.rate" << std::endl;
		std::cout << indent << "--------------------------------------------------------------------------------------------------------------" << std::endl;
// 		std::cout << indent << "####  #######  #######  -############  -############  -############  -############  ##.#  ##.#  ##.#   #.#####" << std::endl;
	}
	
	double res_l2norm;
	double corr_norm;
	double exact_error_norm;
	
	
	double bisectionsteps_pre;
	double bisectionsteps_post;
	double bisectionsteps_coarse;
	
	double initial_error;
	
	double alpha;
	
    Dune::Solvers::Interval<double> D;
	int truncated = 0;
	int projected = 0;
	
	Vector smoother_corr;
	Vector smoother_temp;
	
	// cg stuff
	Vector old_corr;
	Vector v_buffer;
	
	Dune::BitSetVector<1> truncation_buffer = truncation;
	Vector active_u_buffer = u;
	
	bool truncate_flag_save = truncate_flag;
	bool allowcoarseviolation_flag_save = allowcoarseviolation_flag;
	bool linesearch_flag_save = linesearchType.enable;
	int gsprojectsteps_save = gsprojectsteps;
	bool nextStepStandard = true;
	bool thisStepStandard = false;
	
	Dune::Timer smoothing_time;
	smoothing_time.stop();
	
	iter = 0;
	while ((iter < maxIter) && (err > tol))
	{
		++iter;
		
		u_old = u;
		
		if (smootherType.damp_fine_smoother_flag)
			smoother_temp = u;
		
		// apply nonlinear fine level smoother
		// this acts on u directly !
		bisectionsteps = 0;
		for (int i=0; i<cycleType.finepre; ++i)
		{
			smoothing_time.start();
			if (smootherType.nonlinear_jacobi_flag)
				nonlinearJacobi(phi, isHanging);
			else
				nonlinearGaussSeidel(phi, isHanging);
			smoothing_time.stop();
		}
		bisectionsteps_pre = bisectionsteps;
		
		if (smootherType.damp_fine_smoother_flag)
		{
			smoother_corr = u;
			u = smoother_temp;
			smoother_corr -= u;
			linesearch(u, smoother_corr, phi);
		}
		
		
		if (not(simplegs))
		{
			if (activesetvcycle_flag or adaptiveactivesetvcycle_flag)
			{
				thisStepStandard = nextStepStandard;
				nextStepStandard = not(thisStepStandard);
				
				if (adaptiveactivesetvcycle_flag)
					nextStepStandard = false;
				
				truncate_flag = truncate_flag_save;
				allowcoarseviolation_flag = allowcoarseviolation_flag_save;
				linesearchType.enable = linesearch_flag_save;
				gsprojectsteps = gsprojectsteps_save;
				
				if (thisStepStandard)
				{
					truncate_flag = false;
					allowcoarseviolation_flag = false;
// 					linesearchType.enable = false;
					gsprojectsteps = 0;
				}
			}
			
			// assemble truncated system and obstacle
			// the truncated system is 
			// 
			//    TAT v* = A* v* = r* = Tr
			//
			// the final correction with respect to A
			// is thus obtained by truncation v = Tv*
			
			assemble(phi, truncated, isHanging);
			
			if (adaptiveactivesetvcycle_flag)
			{
				if (not(thisStepStandard) and not(nextStepStandard))
				{
					for (size_t row = 0; row < mat.N(); ++row)
					{
						if (truncation[row][0]!=truncation_buffer[row][0])
							nextStepStandard = true;
						else if (truncation[row][0] and (u[row]!=active_u_buffer[row]))
							nextStepStandard = true;
						
						truncation_buffer[row][0] = truncation[row][0];
						active_u_buffer[row] = u[row];
					}
				}
			}
			
			// apply v-cycle for truncated system A* v* = r*
			level = fine_level;
			v_cycle(true);
			
			// post process coarse correction
			for (size_t row = 0; row < mat.N(); ++row)
			{
				// truncation of correction v = Tv*
				if (truncation[row][0])
					v_sum[fine_level][row] = 0.0;
				
				if (isHanging[row][0])
					v_sum[fine_level][row] = 0.0;
			}
			
			if (convexcombination_flag)
				v_buffer = v_sum[fine_level];
			
			if (adaptiveactivesetvcycle_flag)
			{
				if (not(thisStepStandard) and not(nextStepStandard))
				{
					for (size_t row = 0; row < mat.N(); ++row)
					{
						phi.domain(row, D, 0);
						if (v_sum[fine_level][row] < D[0] - u[row])
							nextStepStandard = true;
						if (v_sum[fine_level][row] > D[1] - u[row])
							nextStepStandard = true;
					}
				}
			}
			
			if (gsprojectsteps>0)
			{
				Vector u_dummy = u;
				u += v_sum[fine_level];
				
				for (int i=0; i<gsprojectsteps; ++i)
					nonlinearGaussSeidel(phi, isHanging);
				
				v_sum[fine_level] = u;
				v_sum[fine_level] -= u_dummy;
				u = u_dummy;
			}
			
			// project correction in convex set
			if (projectcorrection_flag)
			{
				projected = 0;
				for (size_t row = 0; row < mat.N(); ++row)
				{
					phi.domain(row, D, 0);
					if (v_sum[fine_level][row] < D[0] - u[row])
					{
						v_sum[fine_level][row] = D[0] - u[row];
						++projected;
					}
					if (v_sum[fine_level][row] > D[1] - u[row])
					{
						v_sum[fine_level][row] = D[1] - u[row];
						++projected;
					}
				}
			}
			
			if (convexcombination_flag)
			{
				linesearch(u, v_buffer, phi);
				u += v_buffer;
				v_sum[fine_level] -= v_buffer;
			}
			
			// cg-acceleration of linear correction
			if (cg_flag)
			{
				old_corr -= u;
				if (iter>1)
				{
					// if no smoothing use simple cg
					if (cycleType.pre+cycleType.post==0)
						v_sum[fine_level] = res[fine_level];
					
					// res = Av_old
					res[fine_level] = 0.0;
/*					A[fine_level].umv(old_corr, res[fine_level]);
					res[fine_level].axpy(m[fine_level]*old_corr, m[fine_level]);*/
					mat.umv(old_corr, res[fine_level]);
					res[fine_level].axpy(mat_vec*old_corr, mat_vec);
					
					// factor = <Av_old, B^-1r> / <Av_old, v_old>
					double factor = (res[fine_level]*v_sum[fine_level]) / (res[fine_level]*old_corr);
					
					v_sum[fine_level].axpy(-factor, old_corr);
					
					// project correction in convex set
					if (projectcorrection_flag)
					{
						for (size_t row = 0; row < mat.N(); ++row)
						{
							phi.domain(row, D, 0);
							if (v_sum[fine_level][row] < D[0] - u[row])
								v_sum[fine_level][row] = D[0] - u[row];
							if (v_sum[fine_level][row] > D[1] - u[row])
								v_sum[fine_level][row] = D[1] - u[row];
						}
					}
				}
				old_corr = u;
			}
			
			bisectionsteps = 0;
			if (linesearchType.enable)
				alpha = linesearch(u, v_sum[fine_level], phi);
			else
				alpha = 1.0;
			bisectionsteps_coarse = bisectionsteps;
			
			// apply coarse correction
			u += v_sum[fine_level];
		}
		
		
		if (smootherType.damp_fine_smoother_flag)
			smoother_temp = u;
		
		// apply nonlinear fine level smoother
		// this acts on u directly !
		bisectionsteps = 0;
		for (int i=0; i<cycleType.finepost; ++i)
		{
			if (smootherType.nonlinear_jacobi_flag)
				nonlinearJacobi(phi, isHanging);
			else
				nonlinearGaussSeidel(phi, isHanging);
		}
		bisectionsteps_post = bisectionsteps;
		
		if (smootherType.damp_fine_smoother_flag)
		{
			smoother_corr = u;
			u = smoother_temp;
			smoother_corr -= u;
			linesearch(u, smoother_corr, phi);
		}

		
		// compute residual
		res[fine_level] = rhs;
		
		mat.mmv<Vector, Vector>(u, res[fine_level]);
		res[fine_level].axpy(-(mat_vec*u), mat_vec);
		
		phi.setVector(u);
		double residual_border_truncation = config.get("residual_border_truncation",1e-15);
		for(size_t row = 0; row < mat.N(); ++row)
		{
			phi.subDiff(row, u[row], D, 0);
			D[0] = -D[0] + res[fine_level][row];
			D[1] = -D[1] + res[fine_level][row];
			res[fine_level][row] = 0.0;
			if (0.0 < D[0])
				res[fine_level][row] = D[0];
			if (D[1] < 0.0)
				res[fine_level][row] = D[1];
			
			phi.domain(row, D, 0);
			if ((isDirichlet[fine_level][row][0]) or (std::abs(D[0]-u[row]) < residual_border_truncation) or (fabs(D[1]-u[row]) < residual_border_truncation))
				res[fine_level][row] = 0.0;
		}
		
		// count contact nodes
// 		findContactSet(u, u_old, upper_obst, lower_obst, contact, contact_count, contact_diff);
		
		res_l2norm = std::sqrt(res[fine_level] * res[fine_level]);
		corr_norm = norm.diff(u_old, u);
		
		err = corr_norm;
		
		if (u_exact!=0)
		{
			exact_error_norm = norm.diff(*u_exact, u);
			err = exact_error_norm;
		}
		else
			exact_error_norm = nan;
		
		if (iter==1)
			initial_error = err;
		
		
		
		if (verbosity >= 5)
		{
			std::cout << std::scientific << std::setprecision(6);
			std::cout << indent;
			std::cout << std::setw(4) << iter;
			if (truncate_flag)
				std::cout << std::setw(9) << truncated;
			else
				std::cout << std::setw(9) << nan;
			std::cout << std::setw(9) << projected;
// 			std::cout << std::setw(9) << contact_count;
			std::cout << std::setw(15) << res_l2norm;
			std::cout << std::setw(15) << corr_norm;
			std::cout << std::setw(15) << exact_error_norm;
			std::cout << std::setw(15) << alpha;
			
	 		std::cout << std::fixed << std::setprecision(1);
			std::cout << std::setw(6) << bisectionsteps_pre/(u.size()*cycleType.finepre);
			std::cout << std::setw(6) << bisectionsteps_coarse;
			std::cout << std::setw(6) << bisectionsteps_post/(u.size()*cycleType.finepost);
			
			
			std::cout << std::fixed << std::setprecision(5);
			std::cout << std::setw(10) << pow(err/initial_error, 1.0/(iter-1));
			
			std::cout << std::scientific << std::setprecision(3);
			
			if (print_smoothing_time_flag)
				std::cout << std::setw(10) << smoothing_time.elapsed();
			
			std::cout << std::scientific << std::setprecision(6);
			std::cout << std::endl;
		}
		
/*		if (contactOnly and (contact_diff == 0))
			break;*/
	}
	if (verbosity >= 5)
		std::cout << indent << "-----------------------------------------------------------------------------------------------" << std::endl;
	
	if (verbosity < 20)
	{
		for(int i=0; i<iter+4; ++i)
			std::cout << "\033[1A\033[K";
		if (verbosity >= 10)
		{
			std::cout << indent << "ScalarTNNMG ";
			std::cout.setf(std::ios::scientific);
			std::cout << std::setw(4) << iter;
			std::cout << std::setw(15) << res_l2norm;
			std::cout << std::setw(15) << corr_norm;
			std::cout << std::endl;
		}
	}
	
	if (verbosity >= 30)
		std::cout << indent << "ScalarTNNMG end solve" << std::endl;
	_u = u;
	return;
}


// apply v-cycle
void ScalarTNNMG::v_cycle(bool updateOperator)
{
	int steps, recursive_step;
	
	// if we are on coarse level
	//    apply defined number of smoothing steps
	// else
	//    apply pre-smoothing, then recursive v-cycle, then post-smoothing
	steps = cycleType.pre + 1 + cycleType.post;
	recursive_step = cycleType.pre;
	if (not(cycleType.linearfinesmoothing_flag) and (level == fine_level))
	{
		steps = 1;
		recursive_step = 0;
	}
	if (level == coarse_level)
	{
		steps = cycleType.coarse;
		recursive_step = -1;
	}
	if (coarseobstacles == none)
	{
		upper[level] = 1e100;
		lower[level] = -1e100;
	}
	
	// reset sum of corrections
	v_sum[level] = 0.0;
	
	// loop for pre-smoothing, recursive v-cycle and post-smoothing
	for (int i=0; i<steps; ++i)
	{
		// do either smoothing step or coarse level step
		if (i==recursive_step)
		{
			// if obstacle violation on coarse levels is not allowed
			// truncate and update obstacles
			if (coarseobstacles != none)
			{
				// mark trucation contact nodes
				// and check if operator has to be updated
				if (truncate_flag and ((level==fine_level) or not(truncatefineonly_flag)))
				{
					bool contact = false;
					for(size_t j = 0; j < v[level].size(); ++j)
					{
						// check if node has contact to obstacle
						contact = ((0 >= (-obst_tol+upper[level][j])) || (0 <= (obst_tol+lower[level][j])));
						
						// if contact property changes truncation has to be modified
						if (contact != truncationlist[level][j][0])
						{
							modificationlist[level][j][0] = true;
							updateOperator = true;
						}
						// remember truncation to check for changes in next iteration
						truncationlist[level][j][0] = contact;
					}
				}
				else
					truncationlist[level].unsetAll();
				
				// restrict obstacles
				restrictObstacles(upper[level], upper[level-1], lower[level], lower[level-1], truncationlist[level], transfer[level-1]->getMatrix());
				if (coarseobstacles == apriori)
					restrictObstacles(upper[level], upper[level-1], lower[level], lower[level-1], truncationlist[level], (*transfer2)[level-1]->getMatrix());
			}
			else
				truncationlist[level].unsetAll();
			
			// update coarse matrix if necessary
			if (updateOperator)
			{
				transfer[level-1]->galerkinRestrict(A[level], A[level-1], truncationlist[level]);
				transfer[level-1]->restrict(m[level], m[level-1], truncationlist[level]);
			}
			
			// restrict residual
			transfer[level-1]->restrict(res[level], res[level-1], truncationlist[level]);
			
			// apply v-cycle on coarser level
			//
			// if operator has been modified on this level
			// this must also be done on coarser levels
			--level;
			v_cycle(updateOperator);
			++level;
			
			// prolongate correction
			transfer[level-1]->prolong(v_sum[level-1], v[level], truncationlist[level]);
			
		}
		else
		{
			// apply smoother
			if (coarseobstacles == apriori)
			{
				Vector upper_temp = upper[level];
				Vector lower_temp = lower[level];
				if (level != coarse_level)
				{
					restrictObstacles(upper[level], upper[level-1], lower[level], lower[level-1], truncationlist[level], (*transfer2)[level-1]->getMatrix());

					Vector temp = upper[level];

//                    transfer[level-1]->prolong(upper[level-1], temp, truncationlist[level]);
//                    upper[level] -= temp;
		
					transfer[level-1]->prolong(lower[level-1], temp, truncationlist[level]);
					lower[level] -= temp;
				
//                    upper[level] *= 1.0 / (cycleType.pre + cycleType.post);
					lower[level] *= 1.0 / (cycleType.pre + cycleType.post);
				
					smooth();
					
					upper[level] = upper_temp;
					lower[level] = lower_temp;
				}
				else
				{
//                    upper[level] *= 1.0 / cycleType.coarse;
					lower[level] *= 1.0 / cycleType.coarse;
				
					smooth();

					upper[level] = upper_temp;
					lower[level] = lower_temp;
				}
			}
			else
				smooth();
		}
		
		// update residual
		if (not(additiv_flag))
		{
			A[level].mmv(v[level], res[level]);
			res[level].axpy(-(m[level]*v[level]), m[level]);
		}
		
		if ((coarseobstacles != none) and (coarseobstacles != apriori))
		{
			// update obstacle
			upper[level] -= v[level];
			lower[level] -= v[level];
		}
		
		// collect correction
		v_sum[level] += v[level];
	}
	return;
}

void ScalarTNNMG::assemble(const ScalarNonlinearity& phi, int& truncated, const Dune::BitSetVector<1>& isHanging)
{
	Matrix::row_type::Iterator it;
	Matrix::row_type::Iterator end;
	
    Dune::Solvers::Interval<double> D;
	
	truncated = 0;
	
	// compute quadratic part of hessian
	A[fine_level] = 0.0;
	A[fine_level] += mat;
	m[fine_level] = mat_vec;
	
	// compute nonlinearity part of hessian
	phi.addHessian(u, A[fine_level]);
	
	// compute quadratic part of gradient
	res[fine_level] = 0.0;
	mat.umv<Vector, Vector>(u, res[fine_level]);
	res[fine_level].axpy((mat_vec*u), mat_vec);
	res[fine_level] -= rhs;
	
	// compute nonlinearity part of gradient
	phi.addGradient(u, res[fine_level]);
	
	// -grad is needed for Newton step
	res[fine_level] *= -1.0;
	
	// determine truncation for non-smooth components
	// and setup obstacles
	truncation.unsetAll();
	for(size_t row = 0; row < mat.N(); ++row)
	{
		{
			phi.domain(row, D, 0);
			
			// if phi is not regular at u or u is not in smooth domain 
			// truncate this component
			if ((phi.regularity(row, u[row], 0) > regularity_tol) or (u[row] < D[0]) or (u[row] > D[1]))
			{
				if (truncate_flag)
				{
					truncation[row][0] = true;
					++truncated;
				}
			}
		}

		
		// if violation on coarse grids is not allowed
		// setup obstacles for untruncated components
		if (coarseobstacles != none)
		{
			if (truncation[row][0])
			{
				lower[fine_level][row] = -1e100;
				upper[fine_level][row] = 1e100;
			}
			else
			{
				lower[fine_level][row] = D[0] - u[row];
				upper[fine_level][row] = D[1] - u[row];
			}
#ifdef MMG_ENABLE_EXACT_OBSTACLES
			if (coarseobstacles == exact)
			{
				actual_lower[row] = D[0] - u[row];
				actual_upper[row] = D[1] - u[row];
			}
#endif
		}
	}
	
	// apply trunation to system
	for(size_t row = 0; row < A[fine_level].N(); ++row)
	{
		// truncate matrix A* = TAT
		it = A[fine_level][row].begin();
		end = A[fine_level][row].end();
		for(; it !=end; ++it)
		{
			if (truncation[row][0] or truncation[it.index()][0])
				(*it) = 0.0;
			if (isHanging[row][0] or isHanging[it.index()][0])
				(*it) = 0.0;
		}
		
		// truncate rank-1 term m* m*' = (Tm)(Tm)' = Tm m'T
		// and righthandside r* = Tr
		if (truncation[row][0] or isHanging[row][0])
		{
			m[fine_level][row] = 0.0;
			res[fine_level][row] = 0.0;
		}
	}
	return;
}

double ScalarTNNMG::linesearch(const Vector& u, Vector& v, ScalarNonlinearity& phi)
{
	Matrix::row_type::ConstIterator it;
	Matrix::row_type::ConstIterator end;
	
	double norm_v = 1.0;
	if (experimentalFlags.linesearch_normed)
	{
		norm_v = std::sqrt(v*v);
		if (norm_v > 0.0)
			v /= norm_v;
		else
			norm_v = 1.0;
	}
	
	res[fine_level] = rhs;
	mat.mmv<Vector, Vector>(u, res[fine_level]);
	res[fine_level].axpy(-(mat_vec*u), mat_vec);
	
	double Avv = 0.0;
	double mvv = 0.0;
	double resv = 0.0;
	double alpha = 0.0;
	
	for(size_t row = 0; row < v.size(); ++row)
	{
		// compute <r,v>
		resv += res[fine_level][row] * v[row];
		
		// compute <Av,v>
		it = mat[row].begin();
		end = mat[row].end();
		for(; it !=end; ++it)
			Avv += (*it) * v[it.index()] * v[row];
		
		mvv += mat_vec[row] * v[row];
	}
	Avv += mvv*mvv;
	
//    LineSearchPotential psi(u, v, phi);
    DirectionalConvexFunction<ScalarNonlinearity> psi(Avv, resv, phi, u, v);
	
	if (experimentalFlags.linesearch_descentonly)
	{
        double e;
        Dune::Solvers::Interval<double> D;
        
        psi.subDiff(alpha, D);
		
		
		e = phi(u) + 0.5*Avv*alpha*alpha - resv*alpha;
		
		Vector z =u;
		
		if (D[1]<0.0)
			alpha = 1.0;
		if (D[0]>0.0)
			alpha = -1.0;
		
		while (phi(z) + 0.5*Avv*alpha*alpha - resv*alpha > e)
		{
			alpha *= 0.5;
			z = v;
			z *= alpha;
			z += u;
		}
		
	}
	else
	{
		if (experimentalFlags.linesearch_startbyone)
            alpha = norm_v;
        else
            alpha = 0.0;

        Bisection bisection(0.0, linesearchType.acceptance, linesearchType.tol, fast_quadratic_minimize_flag, linesearchType.safety);
        alpha = bisection.minimize<DirectionalConvexFunction<ScalarNonlinearity> >(psi, alpha, 0.0, bisectionsteps);

	}
	
	if (experimentalFlags.linesearch_damponly)
	{
		double damp = alpha/norm_v;
		if (damp < 0.0)
			damp = 0.0;
		if (damp > 1.0)
			damp = 1.0;
		
		alpha = damp*norm_v;
		v *= alpha;
		return damp;
	}
	
	v*= alpha;
	return alpha/norm_v;
}

void ScalarTNNMG::restrictObstacles(const Vector& upperFine, Vector& upperCoarse, const Vector& lowerFine, Vector& lowerCoarse, const Dune::BitSetVector<1> &contact, const Matrix& op) const
{
	Matrix::row_type::ConstIterator it;
	Matrix::row_type::ConstIterator end;
	
	upperCoarse = 1e100;
	lowerCoarse = -1e100;
	
	for(size_t row=0; row<op.N(); ++row)
	{
		if (not(contact[row][0]))
		{
			it = op[row].begin();
			end = op[row].end();
			
			for(; it !=end; ++it)
			{
//                if (*it != 0.0)
				{
					if (upperCoarse[it.index()] > upperFine[row])
						upperCoarse[it.index()] = upperFine[row];
					if (lowerCoarse[it.index()] < lowerFine[row])
						lowerCoarse[it.index()] = lowerFine[row];
				}
			}
		}
	}
	return;
}


void ScalarTNNMG::nonlinearGaussSeidel(ScalarNonlinearity& phi, const Dune::BitSetVector<1>& isHanging)
{
	double r, s;
	double Aii;
	double old_value;
	
	Matrix::row_type::ConstIterator it;
	Matrix::row_type::ConstIterator end;

	s = mat_vec*u;
	
	phi.setVector(u);
	for(size_t row = 0; row < mat.N(); ++row)
	{
		if (not(isHanging[row][0]) and not(isDirichlet[fine_level][row][0]))
		{
			r = rhs[row];
			
			Aii = 0.0;
			
			it = mat[row].begin();
			end = mat[row].end();
			for(; it !=end; ++it)
			{
				size_t col = it.index();
				if (col == row)
					Aii = (*it);
				else
					r -= (*it) * u[col];
			}
			s -= u[row] * mat_vec[row];
			
			Aii += mat_vec[row]*mat_vec[row];
			r -= s * mat_vec[row];
			
			u[row] = minimize(Aii, r, row, phi, u[row], u[row], smootherType.gs_acceptance, smootherType.gs_tol, smootherType.gs_safety);
			
			phi.updateEntry(row, u[row], 0);
		}
		s += u[row] * mat_vec[row];
	}
	if (smootherType.sgs_flag)
	{
		s = mat_vec*u;
		
		phi.setVector(u);
		for(size_t row = mat.N() - 1; row > -1; --row)
		{
			if (not(isHanging[row][0]) and not(isDirichlet[fine_level][row][0]))
			{
				r = rhs[row];
				
				Aii = 0.0;
				
				it = mat[row].begin();
				end = mat[row].end();
				for(; it !=end; ++it)
				{
					size_t col = it.index();
					if (col == row)
						Aii = (*it);
					else
						r -= (*it) * u[col];
				}
				s -= u[row] * mat_vec[row];
				
				Aii += mat_vec[row]*mat_vec[row];
				r -= s * mat_vec[row];
				
				u[row] = minimize(Aii, r, row, phi, u[row], u[row], smootherType.gs_acceptance, smootherType.gs_tol, smootherType.gs_safety);
				
				phi.updateEntry(row, u[row], 0);
			}
			s += u[row] * mat_vec[row];
		}
	}
	return;
}


void ScalarTNNMG::nonlinearJacobi(ScalarNonlinearity& phi, const Dune::BitSetVector<1>& isHanging)
{
	double r, s;
	double Aii;
	double old_value;
	
	Matrix::row_type::ConstIterator it;
	Matrix::row_type::ConstIterator end;
	
	Vector u_old = u;
	
	s = mat_vec*u_old;
	
	phi.setVector(u);
	for(size_t row = 0; row < mat.N(); ++row)
	{
		if (not(isHanging[row][0]) and not(isDirichlet[fine_level][row][0]))
		{
			r = rhs[row];
			
			Aii = 0.0;
			
			it = mat[row].begin();
			end = mat[row].end();
			for(; it !=end; ++it)
			{
				size_t col = it.index();
				if (col == row)
					Aii = (*it);
				else
					r -= (*it) * u_old[col];
			}
			
			Aii += mat_vec[row]*mat_vec[row];
			r -= (s - mat_vec[row] * u_old[row]) * mat_vec[row];
			
			u[row] = minimize(Aii, r, row, phi, u[row], u[row], smootherType.gs_acceptance, smootherType.gs_tol, smootherType.gs_safety);
		}
	}
	return;
}


double ScalarTNNMG::minimize(double A, double r, int i, const ScalarNonlinearity& phi, double x, double old, double factor, double tol, double safety)
{
	Bisection bisection(0.0, factor, tol, fast_quadratic_minimize_flag, safety);
	ConvexFunction J(A, r, phi, i, 0);
	return bisection.minimize<ConvexFunction>(J, x, old, bisectionsteps);
#if 0
	double x0, x1;
	double a, b;
	
	double dom_lower, dom_upper;
	
	const double safety = 1e-14;
	
	phi.domain(i, dom_lower, dom_upper);

	if (fast_quadratic_minimize_flag)
	{
		double z = r/A;
		if (z < dom_lower)
			z = dom_lower;
		if (z > dom_upper)
			z = dom_upper;
		phi.subDiff(i, z, a, b);
		a += A*z - r;
		b += A*z - r;
		
		if ((a <= safety) and (-safety <= b))
			return z;
	}

	if (x < dom_lower)
		x = dom_lower;
	if (x > dom_upper)
		x = dom_upper;
	
	if (bisection_flag)
	{
		++bisectionsteps;
		phi.subDiff(i, x, a, b);
		a += A*x - r;
		b += A*x - r;
		
		if ((a <= safety) and (-safety <= b))
			return x;
		
		if (a > 0.0)
		{
			x1 = x;
			if (A > 0.0)
			{
				x0 = x - a/A;
				if (x0 < dom_lower)
					x0 = dom_lower;
			}
			else
			{
				x0 = x1 - a;
				if (x0 < dom_lower)
					x0 = dom_lower;
				
				++bisectionsteps;
				phi.subDiff(i, x0, a, b);
				a += A*x0 - r;
				b += A*x0 - r;
				while (a > safety)
				{
					x0 = x1 - 2.0*(x1-x0);
					if (x0 < dom_lower)
					{
						x0 = dom_lower;
						break;
					}
					++bisectionsteps;
					phi.subDiff(i, x0, a, b);
					a += A*x0 - r;
					b += A*x0 - r;
				}
			}
		}
		else
		{
			x0 = x;
			if (A > 0.0)
			{
				x1 = x - b/A;
				if (x1 > dom_upper)
					x1 = dom_upper;
			}
			else
			{
				x1 = x0 - b;
				if (x1 > dom_upper)
					x1 = dom_upper;
				
				++bisectionsteps;
				phi.subDiff(i, x1, a, b);
				a += A*x1 - r;
				b += A*x1 - r;
				while (b < -safety)
				{
					x1 = x0 + 2.0*(x1-x0);
					if (x1 > dom_upper)
					{
						x1 = dom_upper;
						break;
					}
					++bisectionsteps;
					phi.subDiff(i, x1, a, b);
					a += A*x1 - r;
					b += A*x1 - r;
				}
			}
		}
		double z;
 		while (true)
		{
			x = (x0 + x1) / 2.0;
			
			++bisectionsteps;
			phi.subDiff(i, x, a, b);
			a += A*x - r;
			b += A*x - r;
			
			/*
			if ((x1<=old) and (b>=0.0) and (a<=tol))
				break;
			
			if ((x0>=old) and (a<=0.0) and (-tol <= b))
				break;
			*/
			
			// stop if at least 'factor' of full minimization step is realized
			if ((x1<=old) and (b>=0.0) and (std::abs((x-old)/(x0-old)) >= factor) and (a <= tol))
				break;
			
			// stop if at least 'factor' of full minimization step is realized
			if ((x0>=old) and (a<=0.0) and (std::abs((x-old)/(x1-old)) >= factor) and (-tol <= b))
				break;
			
			// stop if 0 in [a,b] = subdiff
			if ((a <= safety) and (-safety <= b))
				break;
			
			// stop if (numerically) x=x1
			if (std::abs(x1-x)<safety)
				break;
			
			// stop if (numerically) x=x0
			if (std::abs(x0-x)<safety)
				break;
			
			// adjust bounds
			if (a > 0.0)
				x1 = x;
			else
				x0 = x;
		}
	}
	
	if (x > dom_upper)
		x = dom_upper;
	if (x < dom_lower)
		x = dom_lower;
	
	return x;
#endif
}

void ScalarTNNMG::smooth()
{
	double s = 0.0;
	double Aii;
	double c;
	Matrix::row_type::ConstIterator it;
	Matrix::row_type::ConstIterator end;

	
	const Matrix& A = this->A[level];
	const Vector& m = this->m[level];
	const Vector& res = this->res[level];
	const Vector& upper = this->upper[level];
	const Vector& lower = this->lower[level];
	const Dune::BitSetVector<1>& isDirichlet = this->isDirichlet[level];
	
	Vector& v = this->v[level];
	
	if (smootherType.jacobi_flag)
	{
		for(size_t row = 0; row < v.size(); ++row)
		{
			if (not(isDirichlet[row][0]))
			{
				Aii = 0.0;
				
				it = A[row].begin();
				end = A[row].end();
				for(; it !=end; ++it)
				{
					if (it.index() == row)
					{
						Aii = (*it) + m[row] * m[row];
						break;
					}
				}
				
				if (Aii == 0)
					v[row] = 0.0;
				else
				{
					v[row] = res[row] / Aii;
					// project in convex set
					if (v[row] > upper[row])
						v[row] = upper[row];
					
					if (v[row] < lower[row])
						v[row] = lower[row];
				}
			}
		}
	}
	else
	{
		for(size_t row = 0; row < v.size(); ++row)
		{
			if (not(isDirichlet[row][0]))
			{
				c = res[row];
				
				Aii = 0.0;
				
				it = A[row].begin();
				end = A[row].end();
				for(; it !=end; ++it)
				{
					size_t col = it.index();
					if (col < row)
						c -= (*it) * v[col];
					if (col == row)
						Aii = (*it);
				}
				c -= s * m[row];
				
				c /= Aii + m[row] * m[row];
				
				if (Aii + m[row] * m[row] == 0)
					c = 0;

#ifdef MMG_ENABLE_EXACT_OBSTACLES
				if (coarseobstacles == exact)
				{
					{
						double local_upper;
						double local_lower;
						
						std::map<int, double>::iterator fineLevelIt = fineLevelInterpolation[level][row].begin();
						std::map<int, double>::iterator fineLevelEnd = fineLevelInterpolation[level][row].end();
						for (; fineLevelIt!=fineLevelEnd; ++fineLevelIt)
						{
							local_upper = actual_upper[fineLevelIt->first] / fineLevelIt->second;
							local_lower = actual_lower[fineLevelIt->first] / fineLevelIt->second;
							if (c > local_upper)
								c = local_upper;
							if (c < local_lower)
								c = local_lower;
						}
					}
					{
						std::map<int, double>::iterator fineLevelIt = fineLevelInterpolation[level][row].begin();
						std::map<int, double>::iterator fineLevelEnd = fineLevelInterpolation[level][row].end();
						for (; fineLevelIt!=fineLevelEnd; ++fineLevelIt)
						{
							actual_lower[fineLevelIt->first] -= c * fineLevelIt->second;
							actual_upper[fineLevelIt->first] -= c * fineLevelIt->second;
						}
					}
				}
				else
#endif
				{
					// project in convex set
					if (c > upper[row])
						c = upper[row];
					
					if (c < lower[row])
						c = lower[row];
				}
				
				s += c * m[row];
				
				v[row] = c;
			}
		}
		if (smootherType.sgs_flag)
		{
			for(size_t row = v.size()-1; row > -1; --row)
			{
				if (not(isDirichlet[row][0]))
				{
					c = res[row];
					
					Aii = 0.0;
					
					it = A[row].begin();
					end = A[row].end();
					for(; it !=end; ++it)
					{
						size_t col = it.index();
						
						c -= (*it) * v[col];
						
						if (col == row)
							Aii = (*it);
					}
					c -= s * m[row];
					
					c /= Aii + m[row] * m[row];
					
					if (Aii + m[row] * m[row] == 0)
						c = 0;

#ifdef MMG_ENABLE_EXACT_OBSTACLES
					if (coarseobstacles == exact)
					{
						{
							double local_upper;
							double local_lower;
							
							std::map<int, double>::iterator fineLevelIt = fineLevelInterpolation[level][row].begin();
							std::map<int, double>::iterator fineLevelEnd = fineLevelInterpolation[level][row].end();
							for (; fineLevelIt!=fineLevelEnd; ++fineLevelIt)
							{
								local_upper = actual_upper[fineLevelIt->first] / fineLevelIt->second;
								local_lower = actual_lower[fineLevelIt->first] / fineLevelIt->second;
								if (c > local_upper)
									c = local_upper;
								if (c < local_lower)
									c = local_lower;
							}
						}
						{
							std::map<int, double>::iterator fineLevelIt = fineLevelInterpolation[level][row].begin();
							std::map<int, double>::iterator fineLevelEnd = fineLevelInterpolation[level][row].end();
							for (; fineLevelIt!=fineLevelEnd; ++fineLevelIt)
							{
								actual_lower[fineLevelIt->first] -= c * fineLevelIt->second;
								actual_upper[fineLevelIt->first] -= c * fineLevelIt->second;
							}
						}
					}
					else
#endif
					{
						// project in convex set
						if (c > upper[row] - v[row])
							c = upper[row] - v[row];
						
						if (c < lower[row] - v[row])
							c = lower[row] - v[row];
					}
					
					s += c * m[row];
					
					v[row] += c;
				}
			}
		}
	}
	return;
}



void ScalarTNNMG::findContactSet(const Vector& u_new, const Vector& u_old, const Vector& upper, const Vector& lower,
						Dune::BitSetVector<1>& contact, int& contact_count, int& contact_diff)
{
	contact_diff = 0;
	contact_count = 0;
	
	bool upper_old;
	bool lower_old;
	
	bool upper_new;
	bool lower_new;
	
	for (size_t i=0; i<u_new.size(); ++i)
	{
		upper_new = (u_new[i] >= (-obst_tol+upper[i]));
		lower_new = (u_new[i] <= ( obst_tol+lower[i]));
		
		upper_old = (u_old[i] >= (-obst_tol+upper[i]));
		lower_old = (u_old[i] <= ( obst_tol+lower[i]));
		
		if ((upper_new != upper_old) or (lower_new != lower_old))
			++contact_diff;
		
		contact[i][0] = (upper_new || lower_new);
		if (contact[i][0])
			++contact_count;
	}

	return;
}


