#ifndef GENERIC_NONLINEAR_JACOBI_HH
#define GENERIC_NONLINEAR_JACOBI_HH

#include <dune/solvers/iterationsteps/iterationstep.hh>

/** \brief Generic nonlinear Jacobi smoothing step
 *
 *  To get a simple Jacobi iteration for problem with diagonal nonlinearity instantiate
 *  with a BlockNonliearGSProblem.
 *
 *  \param TNNMGProblemType Problem class implementing the IterateObject
 */

template<class TNNMGProblemType>
class GenericNonlinearJacobi : public IterationStep<typename TNNMGProblemType::VectorType>
{
        typedef typename TNNMGProblemType::VectorType::size_type SizeType;

    public:

        typedef typename TNNMGProblemType::VectorType VectorType;

        //! default constructor
        GenericNonlinearJacobi() {};

        //! destructor
        ~GenericNonlinearJacobi() {};

        /** \brief sets the problem and solution vector
         *
         *  \param x vector which holds the initial iterate (will be overwritten by new iterates)
         *  \param problem object of TNNMGProblemType implementing the IterateObject
         */
        void setProblem(VectorType& x, TNNMGProblemType& problem)
        {
            this->x_ = &x;
            this->problem = &problem;
        };

        /** \brief makes one Jacobi iteration step for the given problem
         *
         *  Upon completion x_ holds the new iterate accessible by getSol().
         *
         */
        void iterate()
        {
            typedef typename TNNMGProblemType::IterateObject IterateObject;
            VectorType& x = *x_;

            IterateObject iterateObject = problem->getIterateObject();

            iterateObject.setIterate(x);
            for(SizeType i=0; i<x.size(); ++i)
            {
                iterateObject.solveLocalProblem(x[i], i, (*ignoreNodes_)[i]);
            }

            for(SizeType i=0; i<x.size(); ++i)
            {
                iterateObject.updateIterate(x[i], i);
            }
        };

        using IterationStep<VectorType>::ignoreNodes_;

    private:

        TNNMGProblemType* problem;
        using IterationStep<VectorType>::x_;
};

#endif

