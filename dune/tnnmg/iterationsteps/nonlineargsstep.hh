// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_TNNMG_ITERATIONSTEPS_NONLINEARGSSTEP_HH
#define DUNE_TNNMG_ITERATIONSTEPS_NONLINEARGSSTEP_HH

#include <dune/common/indices.hh>
#include <dune/common/typetraits.hh>
#include <dune/common/hybridutilities.hh>

#include <dune/solvers/iterationsteps/iterationstep.hh>
#include <dune/solvers/common/defaultbitvector.hh>



namespace Dune {
namespace TNNMG {

namespace Impl {

template<class C, class = void>
struct IsContainer : std::false_type {};

template<class C>
struct IsContainer<C, std::void_t<decltype(std::declval<C>().size())>> : std::true_type {};

template<class C>
constexpr bool IsHybridContainer_v = Impl::IsContainer<C>::value or IsTuple<C>::value;

}




class CoordinateDirection
{
public:
  CoordinateDirection(std::size_t index) :
    index_(index)
  {}

  std::size_t index() const
  {
    return index_;
  }

private:
  std::size_t index_;
};



/**
 * \brief A nonlinear Gauss-Seidel loop
 *
 * \param x Vector storing the iterate
 * \param f Functional to minimize
 * \param ignore Bit-vector marking ignored components
 * \param localSolver Solver used to solve the local defect problems
 *
 * This is not a full iteration step but just its algorithmic
 * core. Having this separately allows to easily implement
 * a nested Gauss-Seidel by calling gaussSeidelLoop()
 * recursively inside of a localSolver.
 */
template<class V, class F, class BV, class LS,
  std::enable_if_t<not Impl::IsHybridContainer_v<LS>, int> = 0>
void gaussSeidelLoop(V& x, const F& f, const BV& ignore, const LS& localSolver)
{
  namespace H = Dune::Hybrid;

  auto&& shiftedF = shift(f, x);

  H::forEach(H::integralRange(H::size(x)), [&](auto&& i) {
    auto&& fLocal = coordinateRestriction(shiftedF, i);
#ifdef NEW_TNNMG_COMPUTE_ITERATES_DIRECTLY
    localSolver(x[i], fLocal, ignore[i]);
#else
    auto localCorrection = x[i];
    localCorrection = 0;
    localSolver(localCorrection, fLocal, ignore[i]);
    x[i] += localCorrection;
#endif
    shiftedF.updateOrigin(i);
  });
}



/**
 * \brief A nonlinear Gauss-Seidel loop
 *
 * \param x Vector storing the iterate
 * \param f Functional to minimize
 * \param ignore Bit-vector marking ignored components
 * \param localSolver Solver used to solve the local defect problems
 *
 * This is not a full iteration step but just its algorithmic
 * core. Having this separately allows to easily implement
 * a nested Gauss-Seidel by calling gaussSeidelLoop()
 * recursively inside of a localSolver.
 */
template<class V, class F, class BV, class LS,
  std::enable_if_t<Impl::IsHybridContainer_v<LS>, int> = 0>
void gaussSeidelLoop(V& x, const F& f, const BV& ignore, const LS& localSolvers)
{
  namespace H = Dune::Hybrid;

  auto&& shiftedF = shift(f, x);

  H::forEach(H::integralRange(H::size(x)), [&](auto&& i) {
    auto&& fLocal = coordinateRestriction(shiftedF, i);
#ifdef NEW_TNNMG_COMPUTE_ITERATES_DIRECTLY
    localSolver(x[i], fLocal, ignore[i]);
#else
    auto localCorrection = x[i];
    localCorrection = 0;
    H::elementAt(localSolvers, i)(localCorrection, fLocal, ignore[i]);
    x[i] += localCorrection;
#endif
    shiftedF.updateOrigin(i);
  });
}


template<class LS>
class GaussSeidelLocalSolver
{
public:
  GaussSeidelLocalSolver(const LS& localSolver) :
    localSolver_(localSolver)
  {}

  template<class Vector, class Functional, class BitVector>
  constexpr void operator()(Vector& x, const Functional& f, const BitVector& ignore) const
  {
    gaussSeidelLoop(x, f, ignore, localSolver_);
  }

private:
  LS localSolver_;
};


template<class LS>
auto gaussSeidelLocalSolver(LS&& localSolver) -> decltype(GaussSeidelLocalSolver<std::decay_t<LS>>(std::forward<LS>(localSolver)))
{
  return GaussSeidelLocalSolver<std::decay_t<LS>>(std::forward<LS>(localSolver));
}


/**
 * \brief A nonlinear Gauss-Seidel step
 *
 * \tparam F Functional to minimize
 * \tparam LS Local solver type
 * \tparam BV Bit-vector type for marking ignored components
 */
template<class F, class LS, class BV = typename Solvers::DefaultBitVector_t<typename F::Vector> >
class NonlinearGSStep :
  public IterationStep<typename F::Vector, BV>
{
  using Base = IterationStep<typename F::Vector, BV>;

public:

  using Vector = typename F::Vector;
  using BitVector = typename Base::BitVector;
  using Functional = F;
  using LocalSolver = LS;

  //! default constructor
  template<class LS_T>
  NonlinearGSStep(const Functional& f, Vector& x, LS_T&& localSolver) :
    Base(x),
    f_(&f),
    localSolver_(std::forward<LS_T>(localSolver))
  {}

  //! destructor
  ~NonlinearGSStep()
  {}

  using Base::getIterate;

  /**
   * \brief Do one Gauss-Seidel step
   */
  void iterate() override
  {
    auto& x = *getIterate();
    const auto& ignore = (*this->ignoreNodes_);
    gaussSeidelLoop(x, *f_, ignore, localSolver_);
  }

private:

  const Functional* f_;
  LocalSolver localSolver_;
};



} // end namespace TNNMG
} // end namespace Dune



#endif // DUNE_TNNMG_ITERATIONSTEPS_NONLINEARGSSTEP_HH

