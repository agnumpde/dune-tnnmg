#ifndef DUNE_TNNMG_ITERATIONSTEPS_LINEARCORRECTION_HH
#define DUNE_TNNMG_ITERATIONSTEPS_LINEARCORRECTION_HH 1

#include <functional>
#include <memory>

#include <dune/solvers/iterationsteps/lineariterationstep.hh>
#include <dune/solvers/solvers/iterativesolver.hh>
#include <dune/solvers/solvers/linearsolver.hh>
#include <dune/solvers/common/canignore.hh>

namespace Dune {
namespace TNNMG {

namespace Impl {

template<typename Vector>
Solvers::DefaultBitVector_t<Vector>
emptyIgnore(const Vector& v)
{
  // TNNMGStep assumes that the linearization and the solver for the
  // linearized problem will not use the ignoreNodes field
  Solvers::DefaultBitVector_t<Vector> ignore;
  Solvers::resizeInitialize(ignore, v, false);
  return ignore;
}

} /* namespace Impl */

/**
 * \brief linear correction step for use by \ref TNNMGStep
 *
 * The function object should solve the linear equation \f$ A x = b \f$.
 * Be aware that full rows or columns of `A` might contain only zeroes.
 */
template<typename Matrix, typename Vector>
using LinearCorrection = std::function<void(const Matrix& A, Vector& x, const Vector& b)>;

template<typename Matrix, typename Vector>
LinearCorrection<Matrix, Vector>
makeLinearCorrection(std::shared_ptr< Solvers::LinearSolver<Matrix, Vector> > linearSolver)
{
  return [=](const Matrix& A, Vector& x, const Vector& b) {

    auto canIgnoreCast = std::dynamic_pointer_cast<CanIgnore<Solvers::DefaultBitVector_t<Vector>>>( linearSolver );
    auto emptyIgnore = Impl::emptyIgnore(x);
    if (canIgnoreCast)
      canIgnoreCast->setIgnore(emptyIgnore);

    linearSolver->setProblem(A, x, b);
    linearSolver->preprocess();
    linearSolver->solve();
  };
}

template<typename Matrix, typename Vector>
LinearCorrection<Matrix, Vector>
makeLinearCorrection(std::shared_ptr< Solvers::IterativeSolver<Vector> > iterativeSolver)
{
  return [=](const Matrix& A, Vector& x, const Vector& b) {
    using LinearIterationStep = Solvers::LinearIterationStep<Matrix, Vector>;

    auto emptyIgnore = Impl::emptyIgnore(x);

    auto linearIterationStep = dynamic_cast<LinearIterationStep*>(&iterativeSolver->getIterationStep());
    if (not linearIterationStep)
      DUNE_THROW(Exception, "iterative solver must use a linear iteration step");

    linearIterationStep->setIgnore(emptyIgnore);
    linearIterationStep->setProblem(A, x, b);
    iterativeSolver->preprocess();
    iterativeSolver->solve();
  };
}

template<typename Matrix, typename Vector>
LinearCorrection<Matrix, Vector>
makeLinearCorrection(std::shared_ptr< Solvers::LinearIterationStep<Matrix, Vector> > linearIterationStep, int nIterationSteps = 1)
{
  return [=](const Matrix& A, Vector& x, const Vector& b) {
    auto emptyIgnore = Impl::emptyIgnore(x);

    linearIterationStep->setIgnore(emptyIgnore);
    linearIterationStep->setProblem(A, x, b);
    linearIterationStep->preprocess();

    for (int i = 0; i < nIterationSteps; ++i)
      linearIterationStep->iterate();
  };
}

} /* namespace TNNMG */
} /* namespace Dune */

#endif
