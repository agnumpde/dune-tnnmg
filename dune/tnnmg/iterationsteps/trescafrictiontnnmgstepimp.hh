#ifndef DUNE_TNNMG_EXAMPLES_TRESCAFRICTIONTNNMGSTEPIMP_HH
#define DUNE_TNNMG_EXAMPLES_TRESCAFRICTIONTNNMGSTEPIMP_HH

#include <cmath>
#include <string>
#include <sstream>
#include <iomanip>

#include <dune/common/parametertree.hh>
#include <dune/common/bitsetvector.hh>
#include <dune/istl/bcrsmatrix.hh>

#include <dune/matrix-vector/axpy.hh>

#include <dune/solvers/operators/lowrankoperator.hh>
#include <dune/solvers/operators/sumoperator.hh>
#include <dune/solvers/operators/nulloperator.hh>

#include <dune/solvers/common/staticmatrixtools.hh>

#include <dune/tnnmg/problem-classes/nonlinearity.hh>
#include <dune/tnnmg/problem-classes/convexproblem.hh>
#include <dune/tnnmg/problem-classes/directionalconvexfunction.hh>
#include <dune/tnnmg/problem-classes/bisection.hh>
#include <dune/tnnmg/problem-classes/onedconvexfunction.hh>



template <class ConvexProblemTypeTEMPLATE>
class TrescaFrictionTNNMGStepImp
{
    public:
        typedef ConvexProblemTypeTEMPLATE ConvexProblemType;
        typedef typename ConvexProblemType::NonlinearityType NonlinearityType;
        typedef typename ConvexProblemType::VectorType VectorType;
        typedef typename ConvexProblemType::MatrixType MatrixType;
        typedef typename ConvexProblemType::LowRankFactorType LowRankFactorType;
        typedef typename LowRankFactorType::block_type LowRankMatrixBlockType;
        typedef typename ConvexProblemType::LocalVectorType LocalVectorType;
        typedef typename ConvexProblemType::LocalMatrixType LocalMatrixType;

        static const unsigned int block_size = ConvexProblemType::block_size;
        static const unsigned int coarse_block_size = block_size;

        /** \brief class encapsulating the linearized problem for the coarse grid correction
         *
         *  The linear problem here is the sum of a SparseMatrix and a LowRankOperator<LowRankFactor>.
         *  If no low rank matrix is contained in the problem data, i.e. the LowRankFactorType is a NullOperator<...>,
         *  a specialization is used.
         *
         *  \tparam LowRankFactor the type of the matrix factor used to represent a low rank matrix
         */
        template <class LowRankFactor>
        class LinearizedProblem
        {
            public:
                static const unsigned int block_size = coarse_block_size;

                //! The block type of the sparse matrix of the linearized problem. It needs to accommodate sums of blocks of the (nonlinear) problem sparse matrix and the Hessian of the Nonlinearity.
                typedef typename StaticMatrix::template Promote <typename ConvexProblemType::LocalMatrixType, typename NonlinearityType::LocalMatrixType>::Type LocalMatrixType;
                //! The type of the sparse matrix of the linearized problem
                typedef Dune::BCRSMatrix< typename LinearizedProblem::LocalMatrixType > SparseMatrixType;
                //! export the LowRankFactorType
                typedef LowRankFactor LowRankFactorType;
                //! the type of the low rank operator
                typedef LowRankOperator<LowRankFactorType> LowRankMatrixType;
                //! the type of the overall linear operator
                typedef SumOperator<SparseMatrixType, LowRankMatrixType> MatrixType;
                typedef Dune::BlockVector< Dune::FieldVector<double,LinearizedProblem::block_size> > VectorType;
                typedef Dune::BitSetVector< LinearizedProblem::block_size > BitVectorType;

                typename LinearizedProblem::VectorType b;
                typename LinearizedProblem::BitVectorType ignore;
                typename LinearizedProblem::BitVectorType truncation;

                /* \brief default constructor */
                LinearizedProblem():
                    lowrank_m_(lr_fac_),
                    A(sparse_m_, lowrank_m_)
                {}

                /* \brief returns a reference to the sparse matrix */
                SparseMatrixType& sparseMatrix()
                {
                    return sparse_m_;
                }

                /* \brief returns a reference to the low rank matrix */
                LowRankMatrixType& lowRankMatrix()
                {
                    return lowrank_m_;
                }

                /* \brief returns a reference to the low rank factor */
                LowRankFactorType& lowRankFactor()
                {
                    return lr_fac_;
                }

            private:
                //! the sparse matrix
                typename LinearizedProblem::SparseMatrixType sparse_m_;
                //! the low rank factor
                typename LinearizedProblem::LowRankFactorType lr_fac_;
                //! the low rank operator
                typename LinearizedProblem::LowRankMatrixType lowrank_m_;
            public:
                //! the overall linear operator
                typename LinearizedProblem::MatrixType A;
        };

        /** \brief class encapsulating the linearized problem for the coarse grid correction
         *
         *  Specialization for the case when no low rank matrix is contained in the problem data, i.e. the LowRankFactorType is a NullOperator<...>.
         *
         *  \tparam LRBlockType some valid block type such as DiagonalMatrix etc.
         */
        template <class LRBlockType>
        class LinearizedProblem< NullOperator<LRBlockType> >
        {
            public:
                static const int unsigned block_size = coarse_block_size;

                typedef typename StaticMatrix::template Promote <typename ConvexProblemType::LocalMatrixType, typename NonlinearityType::LocalMatrixType>::Type LocalMatrixType;
                typedef Dune::BCRSMatrix< typename LinearizedProblem::LocalMatrixType > SparseMatrixType;
                typedef NullOperator<LRBlockType> LowRankFactorType;
                typedef LowRankOperator<LowRankFactorType> LowRankMatrixType;
                //! the overall operator type is the sparse matrix type here
                typedef SparseMatrixType MatrixType;
                typedef Dune::BlockVector< Dune::FieldVector<double,LinearizedProblem::block_size> > VectorType;
                typedef Dune::BitSetVector< LinearizedProblem::block_size > BitVectorType;

                typename LinearizedProblem::VectorType b;
                typename LinearizedProblem::BitVectorType ignore;
                typename LinearizedProblem::BitVectorType truncation;

                LinearizedProblem():
                    lowrank_m_(lr_fac_),
                    A(sparse_m_)
                {}

                SparseMatrixType& sparseMatrix()
                {
                    return sparse_m_;
                }

                LowRankMatrixType& lowRankMatrix()
                {
                    return lowrank_m_;
                }

                LowRankFactorType& lowRankFactor()
                {
                    return lr_fac_;
                }

            private:
                typename LinearizedProblem::SparseMatrixType sparse_m_;
                typename LinearizedProblem::LowRankFactorType lr_fac_;
                typename LinearizedProblem::LowRankMatrixType lowrank_m_;
            public:
                typename LinearizedProblem::MatrixType& A;
        };

        typedef LinearizedProblem<LowRankFactorType> Linearization;

        /** \brief Constructor
         *
         *  \param _config a parameter set containing the solver parameters
         *  \param problem the ConvexProblem to solve
         */
        TrescaFrictionTNNMGStepImp(const Dune::ParameterTree& _config, ConvexProblemType& problem) :
            problem_(problem),
            config(_config)
        {
            // nonlinearity handling
            regularity_tol = config.get("nonlinearity.regularity_tol", 1e10);

            // postprocessing
            linesearch.enable_flag = config.get("postprocess.linesearch", true);
            linesearch.acceptance = config.get("postprocess.linesearch_acceptance", 1.0);
            linesearch.tol = config.get("postprocess.linesearch_tol", 1e-15);
            linesearch.damponly_flag = config.get("postprocess.linesearch_damponly", false);
            linesearch.normed_flag = config.get("postprocess.linesearch_normed", true);
            linesearch.descentonly_flag = config.get("postprocess.linesearch_descentonly", false);
            linesearch.descentonly_reduction = config.get("postprocess.linesearch_descentonly_reduction", 0.5);
            linesearch.fast_quadratic_minimize_flag = config.get("postprocess.linesearch_fast_quadratic_minimize", true);
            linesearch.try_one_flag = config.get("postprocess.linesearch_try_one", false);
        };


        /** \brief Solves one local system using a gradient method */
        class IterateObject;

        /** \brief Constructs and returns an iterate object */
        IterateObject getIterateObject()
        {
            return IterateObject(config,problem_);
        }


        /** \brief assembles and truncates the linearized problem for the coarse grid correction
         *
         *  \param[in] u solution vector / current iterate
         *  \param[out] linearization the Linearization object encapsulating the coarse grid problem
         *  \param[in] ignore BitSetVector of DOFs to ignore (e.g. Dirichlet nodes, hanging nodes)
         */
        void assembleTruncate(const VectorType& u, Linearization& linearization, const Dune::BitSetVector<block_size>& ignore) const
        {
            setupTruncationPattern(u, linearization, ignore);
            assembleCoarseSystem(u, linearization, ignore);
            applyTruncationPattern(linearization);
        }



        /** \brief assembles linearized problem for the coarse grid correction without truncation
         *
         *  \param[in] u solution vector / current iterate
         *  \param[out] linearization the Linearization object encapsulating the coarse grid problem
         *  \param[in] ignore BitSetVector of DOFs to ignore (e.g. Dirichlet nodes, hanging nodes)
         */
        void setupTruncationPattern(const VectorType& u, Linearization& linearization, const Dune::BitSetVector<block_size>& ignore) const
        {
            // determine truncation pattern
            linearization.truncation.resize(u.size());
            linearization.truncation.unsetAll();
            Dune::Solvers::Interval<double> ab;
            for (size_t i=0; i<u.size(); ++i)
            {
                for(unsigned int j=0; j<block_size; ++j)
                {
                    problem_.phi.domain(i, ab, j);

                    // if (phi is not regular at u) or (u is not in domain) or (component should be ignored)
                    // truncate this component
                    if ((problem_.phi.regularity(i, u[i][j], j) > regularity_tol) or (u[i][j] < ab[0]) or (u[i][j] > ab[1]) or ignore[i][j])
                        linearization.truncation[i][j] = true;
                }
            }
        }



        /** \brief assembles linearized problem for the coarse grid correction without truncation
         *
         *  \param[in] u solution vector / current iterate
         *  \param[out] linearization the Linearization object encapsulating the coarse grid problem
         *  \param[in] ignore BitSetVector of DOFs to ignore (e.g. Dirichlet nodes, hanging nodes)
         */
        void assembleCoarseSystem(const VectorType& u, Linearization& linearization, const Dune::BitSetVector<block_size>& ignore) const
        {
            // we can just copy the ignore information
            linearization.ignore = ignore;

            // Assemble Hessian part of linearization **********************************************
            //
            // This consists of a sparse part incorporating the Hessian of
            // the nonlinearity and a low rank part.

            // construct sparsity pattern for linearization
            typename NonlinearityType::IndexSet indices(problem_.A.N(), problem_.A.M());
            indices.import(problem_.A);
            problem_.phi.addHessianIndices(indices);

            // construct sparse matrix from pattern and initialize it
            indices.exportIdx(linearization.sparseMatrix());
            linearization.sparseMatrix() = 0.0;

            // compute quadratic part of hessian
            // linearization.A += problem_.A;
            for (size_t i=0; i<problem_.A.N(); ++i)
            {
                typename MatrixType::row_type::ConstIterator it = problem_.A[i].begin();
                typename MatrixType::row_type::ConstIterator end = problem_.A[i].end();
                for(; it!=end; ++it)
                    Dune::MatrixVector::addProduct(linearization.sparseMatrix()[i][it.index()], problem_.a, *it);
            }

            // compute nonlinearity part of hessian
            problem_.phi.addHessian(u, linearization.sparseMatrix());

            // Assemble gradient part of linearization *********************************************
            //
            // For symplicity we first assemble the gradient and multiply by -1
            // to get -gradient as needed for a Newton correction.

            // compute quadratic part of gradient
            linearization.b.resize(u.size());
            linearization.b = 0.0;
            problem_.A.template usmv<VectorType, VectorType>(problem_.a, u, linearization.b);

            // compute linear part
            // the factor -1 will be removed by later multiplication with -1
            linearization.b -= problem_.f;

            // compute nonlinearity part of gradient
            problem_.phi.addGradient(u, linearization.b);

            // -grad is needed for Newton step
            linearization.b *= -1.0;
        }



        /** \brief truncate linearized problem for the coarse grid correction
         *
         * This will use the truncation pattern that is already set up in the linearization
         *
         *  \param[out] linearization the Linearization object encapsulating the coarse grid problem
         */
        void applyTruncationPattern(Linearization& linearization) const
        {
            // apply truncation to sparse matrix and rhs
            typename Linearization::SparseMatrixType::row_type::Iterator col_it;
            typename Linearization::SparseMatrixType::row_type::Iterator col_end;
            for (size_t row = 0; row < linearization.sparseMatrix().N(); ++row)
            {
                col_it = linearization.sparseMatrix()[row].begin();
                col_end = linearization.sparseMatrix()[row].end();
                for(; col_it !=col_end; ++col_it)
                {
                    int col = col_it.index();
                    for (size_t i=0; i<(*col_it).N(); ++i)
                    {
                        typename Linearization::SparseMatrixType::block_type::row_type::Iterator blockIt=(*col_it)[i].begin();
                        typename Linearization::SparseMatrixType::block_type::row_type::Iterator blockEnd=(*col_it)[i].end();
                        for(; blockIt!=blockEnd; ++blockIt)
                            if (linearization.truncation[row][i] or linearization.truncation[col][blockIt.index()])
                                (*blockIt) = 0.0;
                    }
                }

                for(unsigned int j=0; j<block_size; ++j)
                    if (linearization.truncation[row][j])
                        linearization.b[row][j] = 0.0;
            }

            if (config.get("logging.truncated", true))
            {
                for(unsigned int j=0; j<block_size; ++j)
                    outStream << std::setw(9) << linearization.truncation.countmasked(j);
            }
        }



        /** \brief Project a given linear correction onto the admissible set
         *
         *  \param[in] u The presmoothed iterate
         *  \param[in] v The linear correction
         *  \param[out] projected_v The result, which is v projected onto the admissible set
         *  \param[in] linearization The linearized (coarse grid) problem
         */
        void projectCoarseCorrection(
            const VectorType& u,
            const typename Linearization::VectorType& v,
            VectorType& projected_v,
            const Linearization& linearization) const
        {
            projected_v = v;

            // truncation of correction v = Tv*
            for (size_t row = 0; row < projected_v.size(); ++row)
            {
                for(unsigned int j=0; j<block_size; ++j)
                    if (linearization.truncation[row][j])
                        projected_v[row][j] = 0.0;
            }

            // project correction in convex set
            Dune::Solvers::Interval<double> ab;
            int projected = 0.0;
            for (size_t row = 0; row < projected_v.size(); ++row)
            {
                for(unsigned int j=0; j<block_size; ++j)
                {
                    problem_.phi.domain(row, ab, j);
                    if (projected_v[row][j] < ab[0] - u[row][j])
                    {
                        projected_v[row][j] = ab[0] - u[row][j];
                        ++projected;
                    }
                    if (projected_v[row][j] > ab[1] - u[row][j])
                    {
                        projected_v[row][j] = ab[1] - u[row][j];
                        ++projected;
                    }
                }
            }
            if (config.get("logging.projected", true))
                outStream << std::setw(9) << projected;
        };


        /** \brief Compute a suitable global damping parameter by a line search 
         *
         *  \param[in] u Current iterate
         *  \param[in] projected_v Direction to do the line search in
         *
         *  Type and exactness of the line search can be controlled by the parameters
         *  given to the constructor.
         */
        double computeDampingParameter(const VectorType& u, const VectorType& projected_v) const
        {
            double alpha = 1.0;

            if (linesearch.enable_flag)
            {
                // normalize vector v to increase stability
                VectorType v = projected_v;
                double scalingFactor = 1.0;
                if (linesearch.normed_flag)
                {
                    scalingFactor = v.two_norm();
                    if (scalingFactor > 0.0)
                        v /= scalingFactor;
                    else
                        return 0.0;
                }

                // initialize alpha by 1 (up to rescaling)
                alpha = scalingFactor;

                // setup directional convex functional
                double Avv;
                double resv;
                {
                    VectorType temp(u.size());

                    // compute quadratic part a*<Av,v>
                    problem_.A.mv(v, temp);
                    Avv = problem_.a*(temp*v);

                    // compute linear part <f-a*Au,v>
                    temp = problem_.f;
                    problem_.A.template usmv<VectorType, VectorType>(-problem_.a, u, temp);
                    resv = temp*v;

                    // only touch the lowRankFactor if its coefficient is not zero; it might not be initialized otherwise
                    if (problem_.am != 0.0)
                    {

                        // compute quadratic part from low rank factor am*<Am'*Am v,v> = am*<Am v, Am v>
                        temp.resize(problem_.lowRankFactor_.N());
                        problem_.lowRankFactor_.mv(v,temp);
                        Avv += problem_.am * (temp * temp);

                        // compute linear part -am*<Am'*Am u,v> = -am*a<Am u, Am v>
                        VectorType temp2(problem_.lowRankFactor_.N());
                        problem_.lowRankFactor_.mv(u,temp2);
                        resv -= problem_.am * (temp * temp2);
                    }
                }
                DirectionalConvexFunction<NonlinearityType> psi(Avv, resv, problem_.phi, u, v);

                // if necessary compute energy before coarse correction
                double oldEnergy;
                if (linesearch.descentonly_flag or linesearch.try_one_flag)
                    oldEnergy = psi(0);

                // if necessary compute energy at alpha
                double energy=0;
                if (linesearch.descentonly_flag or linesearch.try_one_flag or config.get("logging.energy", true))
                    energy = psi(alpha);

                if (linesearch.descentonly_flag)
                {
                    while (energy>oldEnergy)
                    {
                        alpha *= linesearch.descentonly_reduction;
                        oldEnergy = psi(0);
                        energy = psi(alpha);
                    }

                }
                else
                {
                    if (not(linesearch.try_one_flag) or (energy > oldEnergy))
                    {
                        int bisectionsteps = 0;
                        Bisection bisection(0.0, linesearch.acceptance, linesearch.tol, linesearch.fast_quadratic_minimize_flag);
                        alpha = bisection.minimize(psi, scalingFactor, 0.0, bisectionsteps);
                        if (config.get("logging.energy", true))
                            energy = psi(alpha);
                    }
                }

                // restrict alpha to [0,1]
                if (linesearch.damponly_flag)
                {
                    if (alpha < 0.0)
                        alpha = 0.0;
                    if (alpha > scalingFactor)
                        alpha = scalingFactor;
                    if (config.get("logging.energy", true))
                        energy = psi(alpha);
                }

                if (config.get("logging.energy", true))
                {
                    outStream.setf(std::ios::scientific);
                    outStream << std::setw(15) << energy;
                }

                // rescale alpha
                alpha = alpha/scalingFactor;
            }

            return alpha;
        };

        virtual std::string getOutput(bool header=false) const
        {
            if (header)
            {
                outStream.str("");
                for(unsigned int j=0; j<block_size; ++j)
                    outStream << "  trunc" << std::setw(2) << j;
                outStream << "  project";
                if (config.get("logging.energy", true))
                    outStream << "  energy       ";
            }
            std::string s = outStream.str();
            outStream.str("");
            return s;
        }

        /** \brief Compute the value of the functional we are minimizing
         *
         * \deprecated This method shouldn't be here, and will be removed eventually.
         */
        double computeEnergy(const VectorType& v) const
        {
            return problem_(v);
        }


    protected:
        struct LineSearch
        {
            bool enable_flag;
            double acceptance;
            double tol;
            bool damponly_flag;
            bool normed_flag;
            bool descentonly_flag;
            bool fast_quadratic_minimize_flag;
            double descentonly_reduction;
            bool try_one_flag;
        };

        //! problem data
        ConvexProblemType& problem_;

        //! solver parameters
        const Dune::ParameterTree& config;

        LineSearch linesearch;
        double regularity_tol;

        mutable std::ostringstream outStream;

};


/** \brief Solves one local system using a modified scalar Gauss-Seidel method */
template <class ConvexProblemTypeTEMPLATE>
class TrescaFrictionTNNMGStepImp<ConvexProblemTypeTEMPLATE>::IterateObject
{
    friend class TrescaFrictionTNNMGStepImp;


    protected:
        /** \brief Constructor, protected so only friends can instantiate it
         *
         *  \param bisection The class used to do a scalar bisection
         *  \param problem The problem including quadratic part and nonlinear/nonsmooth part
         */
        IterateObject(const Dune::ParameterTree& config, ConvexProblemType& problem) :
            problem_(problem),
            local_J(1.0, 0.0, problem_.phi, 0, 0)
        {
            bisection = Bisection(0.0,
                                  config.get("smoother.gs_acceptance", 1.0),
                                  config.get("smoother.gs_tol", 1e-15),
                                  config.get("smoother.fast_quadratic_minimize", true),
                                  config.get("smoother.safety", 1e-14));
        };


    public:
        /** \brief Set the current iterate */
        void setIterate(VectorType& u)
        {
            // only touch the lowrankFactor if its coefficient is not zero; it might not be initialized otherwise
            if (problem_.am != 0.0)
            {
                // s = problem_.Am*u;
                // so far we only allow rank block_size terms
                s.resize(problem_.lowRankFactor_.N());
                problem_.lowRankFactor_.mv(u,s);
            }

            problem_.phi.setVector(u);

            this->u = u;
        };

        /** \brief Update the i-th block of the current iterate */
        void updateIterate(const LocalVectorType& ui, int i)
        {
            // only touch the lowrankFactor if its coefficient is not zero; it might not be initialized otherwise
            if (problem_.am != 0.0)
            {
                // s += (ui-u[i]) * problem_.Am[i];
                for (size_t k=0; k<problem_.lowRankFactor_.N(); ++k)
                    problem_.lowRankFactor_[k][i].umv(ui-u[i],s[k]);
            }

            for(unsigned int j=0; j<block_size; ++j)
                problem_.phi.updateEntry(i, ui[j], j);

            u[i] = ui;
        };


        /** \brief Do one iteration of a scalar Gauss-Seidel method for the local problem
         *
         * \param[out] ui The solution
         * \param[in] i Block number
         * \param[in] ignore Set of degrees of freedom to leave untouched
         *
         * \return The minimizer of the local functional in the variable ui
         */
        void solveLocalProblem(LocalVectorType& ui, int i, typename Dune::BitSetVector<block_size>::const_reference ignore)
        {
            /*  Here we compute the residual for index i as
             *
             *  b = f[i] - Ru^n - Lu^n+1
             *
             *  and extract the i-th block of the matrix.
             *  Note that the D in Du^n+1 = b is block-diagonal.
             *
             *  When considering the low rank term special care must be taken to sum up
             *  correctly, i.e. not include the diagonal entry in the residual.
             */
            const LocalMatrixType* Aii=0;
            LocalVectorType b(0.0);

            typename MatrixType::row_type::ConstIterator it = problem_.A[i].begin();
            typename MatrixType::row_type::ConstIterator end = problem_.A[i].end();
            for(; it !=end; ++it)
            {
                int col = it.index();
                if (col == i)
                    Aii = &(*it);
                else
                    it->mmv(u[col], b);
            }
            b *= problem_.a;

            if (problem_.am != 0.0)
            {
                VectorType sMinusDiagonalElement = s;
                problem_.lowRankFactor_[0][i].mmv(u[i], sMinusDiagonalElement[0]);
                problem_.lowRankFactor_[0][i].usmtv(-problem_.am, sMinusDiagonalElement[0], b);
            }
            b += problem_.f[i];
            /* - */

            ui = u[i];
            LocalVectorType ui_old = ui;
            local_J.i = i;

            // TODO: We need to start from (0,0,0)  (i.e., on the Tresca nondifferentiability,
            // for this solver to work reliably.
             // [...]

            // Do one step within the one-dimensional non-differentiability of the Tresca functional


            // We are now on the lowest point within the Tresca nondifferentiability.
            // We now do one special gradient-descent step to get below the nondifferentiability.




            // We are now below the Tresca nondifferentiability.  Any descent method will therefore
            // stay away from it.  We therefore do a scalar projected Gauss-Seidel (which handles
            // the contact constraint for us.

            for(unsigned int j=0; j<block_size; ++j)
            {
                if(ignore.test(j))
                    continue;

                local_J.A = problem_.a * (*Aii)[j][j];
                local_J.b = b[j];
                local_J.j = j;

                /* Do the same as above, just on the block level. We don't need to do this for the low rank term since its blocks are assumed to be diagonal anyway */
                typename LocalMatrixType::row_type::ConstIterator it = (*Aii)[j].begin();
                typename LocalMatrixType::row_type::ConstIterator end = (*Aii)[j].end();
                for(; it!=end; ++it)
                    if (it.index()!=j)
                        local_J.b -= (*it) * problem_.a * ui[it.index()];

                ui[j] = bisection.minimize(local_J, ui[j], ui[j], bisectionsteps);

                // we need to update the intermediate local values ...
                problem_.phi.updateEntry(i, ui[j], j);
            }
            // ... and to restore the old ones after computation
            for(unsigned int j=0; j<block_size; ++j)
                problem_.phi.updateEntry(i, ui_old[j], j);
        };


    private:

        //! problem data
        ConvexProblemType& problem_;

        // commonly used minimization stuff
        Bisection bisection;
        OneDConvexFunction<NonlinearityType> local_J;

        /** state data for smoothing procedure used by:
          * setIterate, updateIterate, solveLocalProblem
          */
        VectorType u;

        //! temporary for handling low rank terms
        VectorType s;

        /** \brief Keeps track of the total number of bisection steps that were performed */
        int bisectionsteps;
};


#endif

