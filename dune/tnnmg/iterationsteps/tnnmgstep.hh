#ifndef DUNE_TNNMG_ITERATIONSTEPS_TRUNCATED_NONSMOOTH_NEWTON_MULTIGRID_HH
#define DUNE_TNNMG_ITERATIONSTEPS_TRUNCATED_NONSMOOTH_NEWTON_MULTIGRID_HH

#include <string>
#include <sstream>
#include <vector>
#include <iomanip>

#include <dune/common/timer.hh>

#include <dune/solvers/common/resize.hh>
#include "dune/solvers/iterationsteps/iterationstep.hh"
#include "dune/solvers/iterationsteps/lineariterationstep.hh"
#include <dune/solvers/solvers/iterativesolver.hh>
#include <dune/solvers/solvers/linearsolver.hh>

#include <dune/tnnmg/iterationsteps/linearcorrection.hh>

namespace Dune {
namespace TNNMG {

/**
 * \brief One iteration of the TNNMG method
 *
 * \tparam F Functional to minimize
 * \tparam BV Bit-vector type for marking ignored components
 */
template<class F, class BV, class Linearization,
                                  class DefectProjection,
                                  class LineSearchSolver>
class TNNMGStep :
  public IterationStep<typename F::Vector, BV>
{
  using Base = IterationStep<typename F::Vector, BV>;

public:

  using Vector = typename F::Vector;
  using ConstrainedVector = typename Linearization::ConstrainedVector;
  using ConstrainedMatrix = typename Linearization::ConstrainedMatrix;
  using BitVector = typename Base::BitVector;
  using ConstrainedBitVector = typename Linearization::ConstrainedBitVector;
  using Functional = F;
  using IterativeSolver = Solvers::IterativeSolver< ConstrainedVector, Solvers::DefaultBitVector_t<ConstrainedVector> >;
  using LinearSolver = Solvers::LinearSolver< ConstrainedMatrix,  ConstrainedVector >;

  /** \brief Constructor with an iterative solver object for the linear correction
   * \param iterativeSolver This is a callback used to solve the constrained linearized system
   * \param projection This is a callback used to compute a projection into a defect-admissible set
   * \param lineSolver This is a callback used to minimize a directional restriction of the functional
   *        for computing a damping parameter
   */
  TNNMGStep(const Functional& f,
            Vector& x,
            std::shared_ptr<IterationStep<Vector,BitVector> > nonlinearSmoother,
            std::shared_ptr<IterativeSolver> iterativeSolver,
            const DefectProjection& projection,
            const LineSearchSolver& lineSolver)
  : Base(x),
    f_(&f),
    nonlinearSmoother_(nonlinearSmoother),
    linearCorrection_(makeLinearCorrection<ConstrainedMatrix>(iterativeSolver)),
    projection_(projection),
    lineSolver_(lineSolver)
  {}

  /** \brief Constructor with a linear solver object for the linear correction
   * \param linearSolver This is a callback used to solve the constrained linearized system
   * \param projection This is a callback used to compute a projection into a defect-admissible set
   * \param lineSolver This is a callback used to minimize a directional restriction of the functional
   *        for computing a damping parameter
   */
  TNNMGStep(const Functional& f,
            Vector& x,
            std::shared_ptr<IterationStep<Vector,BitVector> > nonlinearSmoother,
            std::shared_ptr<LinearSolver> linearSolver,
            const DefectProjection& projection,
            const LineSearchSolver& lineSolver)
  : Base(x),
    f_(&f),
    nonlinearSmoother_(nonlinearSmoother),
    linearCorrection_(makeLinearCorrection(linearSolver)),
    projection_(projection),
    lineSolver_(lineSolver)
  {}

  /** \brief Constructor with a LinearIterationStep object for the linear correction
   * \param linearIterationStep This is a callback used to solve the constrained linearized system
   * \param projection This is a callback used to compute a projection into a defect-admissible set
   * \param lineSolver This is a callback used to minimize a directional restriction of the functional
   *        for computing a damping parameter
   */
  TNNMGStep(const Functional& f,
            Vector& x,
            std::shared_ptr<Solvers::IterationStep<Vector,BitVector> > nonlinearSmoother,
            std::shared_ptr<Solvers::LinearIterationStep<ConstrainedMatrix,ConstrainedVector> > linearIterationStep,
            unsigned int noOfLinearIterationSteps,
            const DefectProjection& projection,
            const LineSearchSolver& lineSolver)
  : Base(x),
    f_(&f),
    nonlinearSmoother_(nonlinearSmoother),
    linearCorrection_(makeLinearCorrection(linearIterationStep, noOfLinearIterationSteps)),
    projection_(projection),
    lineSolver_(lineSolver)
  {}

  using Base::getIterate;

  void preprocess() override
  {
    nonlinearSmoother_->setIgnore(this->ignore());
    nonlinearSmoother_->preprocess();
  }

  void setPreSmoothingSteps(std::size_t i)
  {
    preSmoothingSteps_ = i;
  }

  void setLinearization(std::shared_ptr<Linearization> linearization)
  {
    linearization_ = linearization;
  }

  /**
   * \brief Do one TNNMG step
   */
  void iterate() override
  {

    const auto& f = *f_;
    const auto& ignore = (*this->ignoreNodes_);
    auto& x = *getIterate();

    // Nonlinear presmoothing
    for (std::size_t i=0; i<preSmoothingSteps_; ++i)
        nonlinearSmoother_->iterate();

    // Compute constraint/truncated linearization
    if (not linearization_)
      // TODO: Don't hand over the 'ignore' field.  Requires new constructors for all linearizations!
      linearization_ = std::make_shared<Linearization>(f, ignore);
    linearization_->setIgnore(ignore);

    linearization_->bind(x);

    auto&& A = linearization_->hessian();
    auto&& r = linearization_->negativeGradient();

    // Compute inexact solution of the linearized problem
    Solvers::resizeInitializeZero(correction_, x);
    Solvers::resizeInitializeZero(constrainedCorrection_, r);

    linearCorrection_(A, constrainedCorrection_, r);
    linearization_->extendCorrection(constrainedCorrection_, correction_);

    // Project onto admissible set
    projection_(f, x, correction_);

    // Line search
    auto fv = directionalRestriction(f, x, correction_);
    dampingFactor_ = 0;
    lineSolver_(dampingFactor_, fv, false);
    if (std::isnan(dampingFactor_))
      dampingFactor_ = 0;
    correction_ *= dampingFactor_;

    x += correction_;
  }

  /**
   * \brief Export the last computed damping factor
   */
  double lastDampingFactor() const
  {
    return dampingFactor_;
  }

  /**
   * \brief Export the last used linearization
   */
  const Linearization& linearization() const
  {
    return *linearization_;
  }

private:

  const Functional* f_;

  std::shared_ptr<IterationStep<Vector,BitVector> > nonlinearSmoother_;
  std::size_t preSmoothingSteps_ = 1;

  std::shared_ptr<Linearization> linearization_;

  //! \brief linear correction
  LinearCorrection<ConstrainedMatrix, ConstrainedVector> linearCorrection_;

  typename Linearization::ConstrainedVector constrainedCorrection_;
  Vector correction_;
  DefectProjection projection_;
  LineSearchSolver lineSolver_;
  double dampingFactor_;
};


} // end namespace TNNMG
} // end namespace Dune

#endif
