// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_TNNMG_PROJECTIONS_TRIVIALDEFECTPROJECTION_HH
#define DUNE_TNNMG_PROJECTIONS_TRIVIALDEFECTPROJECTION_HH



namespace Dune {
namespace TNNMG {


/** \brief The projection onto the entire space, i.e., the projection that does nothing
 */
struct TrivialDefectProjection
{
  template<class Functional, class Vector>
  constexpr void operator()(const Functional& f, const Vector& x, Vector& v)
  {}
};



} // end namespace TNNMG
} // end namespace Dune



#endif // DUNE_TNNMG_PROJECTIONS_TRIVIALDEFECTPROJECTION_HH
