// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#include <config.h>

#include <iostream>
#include <sstream>

// dune-common includes
#include <dune/common/bitsetvector.hh>
#include <dune/common/stringutility.hh>

// dune-istl includes
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/multitypeblockvector.hh>
#include <dune/istl/multitypeblockmatrix.hh>


// dune-grid includes
#include <dune/grid/yaspgrid.hh>
#include <dune/grid/io/file/vtk/vtkwriter.hh>

// dune-solver includes
#include <dune/solvers/solvers/loopsolver.hh>
#include <dune/solvers/norms/energynorm.hh>
#include <dune/solvers/common/defaultbitvector.hh>

// dune-tnnmg includes
#include <dune/tnnmg/functionals/quadraticfunctional.hh>
#include <dune/tnnmg/functionals/boxconstrainedquadraticfunctional.hh>
#include <dune/tnnmg/iterationsteps/nonlineargsstep.hh>

#include <dune/tnnmg/localsolvers/scalarobstaclesolver.hh>

#include <dune/tnnmg/linearsolvers/ldlsolver.hh>
#include <dune/tnnmg/linearsolvers/ilu0cgsolver.hh>
#include <dune/tnnmg/linearsolvers/fastamgsolver.hh>

#include <dune/solvers/test/common.hh>

using namespace Dune;


template<class MatrixType, class VectorType, class BitVector>
void solveProblem(const MatrixType& mat, VectorType& x, const VectorType& rhs, const BitVector& ignore, int maxIterations=100, double tolerance=1.0e-8)
{
    using Vector = VectorType;
    using Matrix = MatrixType;

    auto lower = x;
    auto upper = x;

    lower = 0;
    upper = 1;

    using Functional = Dune::TNNMG::BoxConstrainedQuadraticFunctional<Matrix, Vector, Vector, Vector, double>;
    auto J = Functional(mat, rhs, lower, upper);

    auto localSubSolver = gaussSeidelLocalSolver(gaussSeidelLocalSolver(Dune::TNNMG::ScalarObstacleSolver()));
    auto localSolver = std::make_tuple(localSubSolver, localSubSolver);

    using Step = typename Dune::TNNMG::NonlinearGSStep<Functional, decltype(localSolver)>;
    using Solver = ::LoopSolver<Vector>;

    auto step = std::make_shared<Step>(J, x, localSolver);
    auto norm = std::make_shared< EnergyNorm<Matrix, Vector> >(mat);
    auto solver = Solver(step, 100, 0, norm, Solver::FULL);

    step->setIgnore(ignore);

    solver.addCriterion(
            [&](){
                return Dune::formatString("   % 12.5e", J(x));
            },
            "   energy      ");

    double initialEnergy = J(x);
    solver.addCriterion(
            [&](){
                static double oldEnergy=initialEnergy;
                double currentEnergy = J(x);
                double decrease = currentEnergy - oldEnergy;
                oldEnergy = currentEnergy;
                return Dune::formatString("   % 12.5e", decrease);
            },
            "   decrease    ");

    solver.addCriterion(
            [&](){
                using namespace Dune::Indices;
                std::size_t n00 = 0;
                for(std::size_t i=0; i<x[_0].size(); ++i)
                  n00 += ((x[_0][i][0] == lower[_0][i][0]) || (x[_0][i][0] == upper[_0][i][0]));
                std::size_t n01 = 0;
                for(std::size_t i=0; i<x[_0].size(); ++i)
                  n01 += ((x[_0][i][1] == lower[_0][i][1]) || (x[_0][i][1] == upper[_0][i][1]));
                std::size_t n10 = 0;
                for(std::size_t i=0; i<x[_0].size(); ++i)
                  n10 += ((x[_1][i][0] == lower[_1][i][0]) || (x[_1][i][0] == upper[_1][i][0]));
                return Dune::formatString("   % 8d   % 8d   % 8d", n00, n01, n10);
            },
            "   #tr(0*0)   #tr(0*1)   #tr(1*0)");

    std::vector<double> correctionNorms;
    solver.addCriterion(Dune::Solvers::correctionNormCriterion(*step, *norm, 1e-10, correctionNorms));

    solver.preprocess();
    solver.solve();
    std::cout << correctionNorms.size() << std::endl;
}





template <class GridType>
bool checkWithGrid(const GridType& grid, const std::string fileName="")
{
  bool passed = true;

  static const int dim = GridType::dimension;

  const int blocksize0 = 2;
  const int blocksize1 = 1;

  using Vector = MultiTypeBlockVector<BlockVector<FieldVector<double,blocksize0> >,
                                      BlockVector<FieldVector<double,blocksize1> > >;
  using BlockedMatrixRow0 = MultiTypeBlockVector<BCRSMatrix<FieldMatrix<double,blocksize0,blocksize0> >,
                                                 BCRSMatrix<FieldMatrix<double,blocksize0,blocksize1> > >;
  using BlockedMatrixRow1 = MultiTypeBlockVector<BCRSMatrix<FieldMatrix<double,blocksize1,blocksize0> >,
                                                 BCRSMatrix<FieldMatrix<double,blocksize1,blocksize1> > >;
  using BlockedMatrixType = MultiTypeBlockMatrix<BlockedMatrixRow0,BlockedMatrixRow1>;

  BlockedMatrixType A;
  Vector blockedX;

  using namespace Indices;

  typedef typename Dune::Solvers::DefaultBitVector_t<Vector> BitVector;

  typedef typename Dune::FieldVector<double, dim> DomainType;
  typedef typename Dune::FieldVector<double, 1> RangeType;

  const auto gridView = grid.leafGridView();

  constructPQ1Pattern(gridView, A[_0][_0]);
  constructPQ1Pattern(gridView, A[_1][_1]);
  A = 0.0;
  assemblePQ1Stiffness(gridView, A[_0][_0]);
  assemblePQ1Stiffness(gridView, A[_1][_1]);

  BlockedMatrixType M;
  constructPQ1Pattern(gridView, M[_0][_0]);
  constructPQ1Pattern(gridView, M[_1][_1]);
  M = 0.0;
  assemblePQ1Mass(gridView, M[_0][_0]);
  assemblePQ1Mass(gridView, M[_1][_1]);
  A[_0][_0] += M[_0][_0];
  A[_1][_1] += M[_1][_1];

  Vector rhs;
  rhs[_0].resize(A[_0][_0].N());
  rhs[_1].resize(A[_1][_1].N());
  rhs = 0;
  auto f = [](const DomainType& x) -> RangeType { return (x.two_norm() < .7) ? 200 : 0;};
  assemblePQ1RHS(gridView, rhs[_0], f);
  assemblePQ1RHS(gridView, rhs[_1], f);

  Vector u;
  Solvers::resizeInitializeZero(u,rhs);

  BitVector ignore;
  ignore[_0].resize(A[_0][_0].N());
  ignore[_1].resize(A[_1][_1].N());
  ignore[_0].unsetAll();
  ignore[_1].unsetAll();
  markBoundaryDOFs(gridView, ignore[_0]);
  markBoundaryDOFs(gridView, ignore[_1]);

  solveProblem(A, u, rhs, ignore);

//  if (fileName!="")
//  {
//    typename Dune::VTKWriter<GridView> vtkWriter(gridView);
//    vtkWriter.addVertexData(u[_0], "solution0");
//    vtkWriter.addVertexData(u[_1], "solution1");
//    vtkWriter.write(fileName);
//  }

  return passed;
}


template <int dim>
bool checkWithYaspGrid(int refine, const std::string fileName="")
{
  bool passed = true;

  typedef Dune::YaspGrid<dim> GridType;

  typename Dune::FieldVector<typename GridType::ctype,dim> L(1.0);
  typename std::array<int,dim> s;
  std::fill(s.begin(), s.end(), 1);

  GridType grid(L, s);

  for (int i = 0; i < refine; ++i)
    grid.globalRefine(1);

  std::cout << "Testing with YaspGrid<" << dim << ">" << std::endl;
  std::cout << "Number of levels: " << (grid.maxLevel()+1) << std::endl;
  std::cout << "Number of leaf nodes: " << grid.leafGridView().size(dim) << std::endl;

  passed = passed and checkWithGrid(grid, fileName);

  return passed;
}




int main(int argc, char** argv) try
{
  Dune::MPIHelper::instance(argc, argv);
  bool passed(true);

  int refine2d = 6;

  passed = passed and checkWithYaspGrid<2>(refine2d, "obstacletnnmgtest_yasp_2d_solution");

  return passed ? 0 : 1;
}
catch (const Dune::Exception& e) {

    std::cout << e << std::endl;
    return 1;

}

