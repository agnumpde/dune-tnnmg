// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#include <config.h>

#include <iostream>
#include <limits>

#include <dune/common/fvector.hh>
#include <dune/common/parallel/mpihelper.hh>

#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/bvector.hh>
#include <dune/istl/matrixindexset.hh>
#include <dune/istl/scaledidmatrix.hh>

#include <dune/solvers/common/defaultbitvector.hh>

#include <dune/tnnmg/functionals/boxconstrainedquadraticfunctional.hh>
#include <dune/tnnmg/iterationsteps/nonlineargsstep.hh>
#include <dune/tnnmg/localsolvers/simplexsolver.hh>

constexpr size_t N = 7;
using Field = double;
using Solver = Dune::TNNMG::SimplexSolver<Field>;

using LocalVector = Dune::FieldVector<Field, N>;
using LocalMatrix = Dune::ScaledIdentityMatrix<Field, N>;
using LocalFunctional = Dune::TNNMG::BoxConstrainedQuadraticFunctional<
    LocalMatrix&, LocalVector&, LocalVector&, LocalVector&, Field>;
using LocalIgnore = std::bitset<N>;

using Vector = Dune::BlockVector<LocalVector>;
using Matrix = Dune::BCRSMatrix<LocalMatrix>;
using Functional = Dune::TNNMG::BoxConstrainedQuadraticFunctional<
    Matrix, Vector, Vector, Vector, Field>;
using Ignore = Dune::Solvers::DefaultBitVector_t<Vector>;

constexpr Field tol = 1e-14;
bool equalsWithTol(Field a, Field b) { return fabs(b - a) < tol; }
bool hasSumWithTol(LocalVector& v, Field sum) {
  return fabs(std::accumulate(v.begin(), v.end(), -sum)) < tol;
}

int main(int argc, char** argv) try {
  Dune::MPIHelper::instance(argc, argv);
  bool passed(true);

  { // test locally
    LocalVector upper(std::numeric_limits<Field>::infinity());
    LocalVector lower(0.0);
    LocalMatrix matrix(1.0);
    LocalVector rhs(1.0);
    LocalFunctional f(matrix, rhs, lower, upper);
    LocalVector x(0.0);
    LocalIgnore ignore(false);
    { // all values one, constraint N
      Solver solver(N);
      solver(x, f, ignore);
      for (auto&& xi : x)
        passed = passed and equalsWithTol(xi, 1.0);
    }
    { // all values one, constraint 1 -> project
      Solver solver(1.0);
      solver(x, f, ignore);
      for (auto&& xi : x)
        passed = passed and equalsWithTol(xi, 1.0 / N);
    }
    { // one entry zero, others 1
      Solver solver(N - 1);
      rhs[0] = 0.0;
      solver(x, f, ignore);
      passed = passed and equalsWithTol(x[0], 0.0);
      for (size_t i = 1; i < N; ++i)
        passed = passed and equalsWithTol(x[i], 1.0);
    }
    { // everything ignored
      Solver solver;
      for (size_t i = 0; i < N; ++i)
        ignore[i] = true;
      x = std::numeric_limits<Field>::quiet_NaN();
      solver(x, f, ignore);
      for (auto&& xi : x)
        passed = passed and std::isnan(xi);
      for (size_t i = 0; i < N; ++i)
        ignore[i] = false;
    }
    { // partially ignored (throws)
      Solver solver;
      ignore[0] = true;
      try {
        solver(x, f, ignore);
        passed = passed and false;
      } catch (const Dune::NotImplemented&) {
        passed = passed and true;
      }
      ignore[0] = false;
    }
    { // different values, fitting constraint
      for (size_t i = 0; i < N; ++i)
        rhs[i] = i + 1;
      Field sum = N * (N + 1) / 2;
      Solver solver(sum);
      solver(x, f, ignore);
      for (size_t i = 0; i < N; ++i)
        passed = passed and equalsWithTol(x[i], i + 1);
    }
    { // different values, oversized constraint -> project
      for (size_t i = 0; i < N; ++i)
        rhs[i] = i + 1;
      Field sum = N * (N + 1) / 2;
      Solver solver(10 * sum);
      solver(x, f, ignore);
      Field sumShift = (sum * 10 - sum) / N;
      for (size_t i = 0; i < N; ++i)
        passed = passed and equalsWithTol(x[i], i + 1 + sumShift);
    }
    { // different values, undersized constraint -> project
      for (size_t i = 0; i < N; ++i)
        rhs[i] = i + 1;
      Field sum = N * (N + 1) / 2;
      Solver solver(sum - N);
      solver(x, f, ignore);
      for (size_t i = 0; i < N; ++i)
        passed = passed and equalsWithTol(x[i], i);
    }
    { // different and coinciding values, undersized constraint -> project
      rhs[0] = 1;
      for (size_t i = 1; i < N; ++i)
        rhs[i] = i;
      Field sum = (N - 1) * (N - 2) / 2;
      Solver solver(sum);
      solver(x, f, ignore);
      passed = passed and equalsWithTol(x[0], 0);
      for (size_t i = 1; i < N; ++i)
        passed = passed and equalsWithTol(x[i], i - 1);
    }
    { // non-zero obstacle
      lower = 1.0;
      for (size_t i = 0; i < N; ++i)
        rhs[i] = i;
      Field sum = N * (N - 1) / 2 + 1;
      Solver solver(sum);
      solver(x, f, ignore);
      passed = passed and equalsWithTol(x[0], 1);
      for (size_t i = 1; i < N; ++i)
        passed = passed and equalsWithTol(x[i], i);
    }
  }
  { // test globally
    size_t m = 10;

    Vector upper(m);
    Vector lower(m);
    Vector rhs(m);
    Vector x(m);
    Ignore ignore(m);

    Dune::MatrixIndexSet indices(m, m);
    for (size_t i = 0; i < m; ++i)
      indices.add(i, i);
    Matrix matrix;
    indices.exportIdx(matrix);

    upper = std::numeric_limits<Field>::infinity();
    lower = 0.0;
    matrix = 1.0;
    for (auto&& rhs_i : rhs)
      for (size_t j = 0; j < N; ++j)
        rhs_i[j] = j + 1;
    Functional f(matrix, rhs, lower, upper);
    x = 0.0;
    for (auto&& xi : x)
      xi[0] = 1.0; // make sure the local sum constraint is fulfilled initially
    ignore.unsetAll();

    Dune::TNNMG::gaussSeidelLocalSolver(Solver(1.0));

    for (auto&& xi : x)
      passed = passed and hasSumWithTol(xi, 1.0);
  }

  return passed ? 0 : 1;
} catch (const Dune::Exception& e) {
  std::cout << e << std::endl;
  return 1;
}
