// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:

#include <config.h>

#include <iostream>
#include <sstream>

// dune-common includes
#include <dune/common/bitsetvector.hh>
#include <dune/common/stringutility.hh>

// dune-istl includes
#include <dune/istl/bcrsmatrix.hh>



// dune-grid includes
#include <dune/grid/yaspgrid.hh>
#include <dune/grid/io/file/vtk/vtkwriter.hh>

// dune-solver includes
#include <dune/solvers/solvers/loopsolver.hh>
#include <dune/solvers/norms/energynorm.hh>
#include <dune/solvers/common/defaultbitvector.hh>
#include <dune/solvers/transferoperators/compressedmultigridtransfer.hh>
#include <dune/solvers/iterationsteps/truncatedblockgsstep.hh>

// dune-tnnmg includes
#include <dune/tnnmg/functionals/quadraticfunctional.hh>
#include <dune/tnnmg/functionals/boxconstrainedquadraticfunctional.hh>
#include <dune/tnnmg/functionals/bcqfconstrainedlinearization.hh>
#include <dune/tnnmg/iterationsteps/nonlineargsstep.hh>
#include <dune/tnnmg/iterationsteps/acceleratednonlineargsstep.hh>
#include <dune/tnnmg/iterationsteps/tnnmgacceleration.hh>

#include <dune/tnnmg/localsolvers/scalarobstaclesolver.hh>

#include <dune/tnnmg/linearsolvers/ldlsolver.hh>
#include <dune/tnnmg/linearsolvers/ilu0cgsolver.hh>
#include <dune/tnnmg/linearsolvers/fastamgsolver.hh>
#include <dune/tnnmg/linearsolvers/mgsolver.hh>

#include <dune/tnnmg/projections/obstacledefectprojection.hh>

#include <dune/solvers/test/common.hh>


// template<class F>
// std::tuple<double, double, int> bisection(const F& f, double start, double guess, double tol)
// {
//     double a = start;
//     double x = guess;
//     double fx = f(x);
//     int evaluations = 1;
//     while (fx<0)
//     {
//         x = x + (guess-start);
//         fx = f(x);
//         ++evaluations;
//     }
//     double b = x;
//     while (std::fabs(b-a)>tol)
//     {
//         x = (a+b)*.5;
//         fx = f(x);
//         ++evaluations;
//         if (fx<0)
//             a = x;
//         else
//             b = x;
//     }
//     return std::make_tuple(x, fx, evaluations);
// };



template<class MatrixType, class VectorType, class BitVector, class TransferOperators>
void solveProblem(const MatrixType& mat, VectorType& x, const VectorType& rhs, const BitVector& ignore, const TransferOperators& transfer, int maxIterations=100, double tolerance=1.0e-8)
{
    using Vector = VectorType;
    using Matrix = MatrixType;

    auto lower = Vector(rhs.size());
    auto upper = Vector(rhs.size());

    lower = 0;
    upper = 1;

    using Functional = Dune::TNNMG::BoxConstrainedQuadraticFunctional<Matrix&, Vector&, Vector&, Vector&, double>;
    auto J = Functional(mat, rhs, lower, upper);

//    auto localSolver = [](auto& xi, const auto& restriction, const auto& ignoreI) {
//        gaussSeidelLoop(xi, restriction, ignoreI, Dune::TNNMG::ScalarObstacleSolver());
//    };

//    Dune::TNNMG::GaussSeidelSolver<Dune::TNNMG::ScalarObstacleSolver> gss(Dune::TNNMG::ScalarObstacleSolver());

    auto localSolver = gaussSeidelLocalSolver(std::make_tuple(Dune::TNNMG::ScalarObstacleSolver(), Dune::TNNMG::ScalarObstacleSolver()));

//    auto localSolver = gaussSeidelLocalSolver(Dune::TNNMG::ScalarObstacleSolver());


    auto smoother = TruncatedBlockGSStep<Matrix, Vector, BitVector>();
    auto linearSolver = Dune::TNNMG::MGSolver<Matrix, Vector>(transfer, smoother, smoother, 3, 3);

//    auto linearSolver = Dune::TNNMG::FastAMGSolver<Matrix, Vector>();
//    auto linearSolver = Dune::TNNMG::ILU0CGSolver(1e-5, 5);
//    auto linearSolver = Dune::TNNMG::LDLSolver();

    using Linearization = Dune::TNNMG::BoxConstrainedQuadraticFunctionalConstrainedLinearization<Functional, BitVector>;
    using LinearSolver = decltype(linearSolver);
    using DefectProjection = Dune::TNNMG::ObstacleDefectProjection;
    using LineSearchSolver = Dune::TNNMG::ScalarObstacleSolver;
    using Acceleration = Dune::TNNMG::TNNMGAcceleration<Functional, BitVector, Linearization, LinearSolver, DefectProjection, LineSearchSolver>;

    auto acceleration = Acceleration(linearSolver, DefectProjection(), LineSearchSolver());

    using Step = typename Dune::TNNMG::AcceleratedNonlinearGSStep<Vector, Functional, decltype(localSolver), decltype(acceleration)>;
    using Solver = LoopSolver<Vector>;
    using Norm =  EnergyNorm<Matrix, Vector>;

    auto step = std::make_shared<Step>(J, x, localSolver, acceleration);
    auto norm = std::make_shared<Norm>(mat);
    auto solver = Solver(step, 1e9, 0, norm, Solver::FULL);

    step->setIgnore(ignore);
    step->setPreSmoothingSteps(3);
    step->setPostSmoothingSteps(3);

    solver.addCriterion(
            [&](){
                return Dune::formatString("   % 12.5e", J(x));
            },
            "   energy      ");

    double initialEnergy = J(x);
    solver.addCriterion(
            [&](){
                static double oldEnergy=initialEnergy;
                double currentEnergy = J(x);
                double decrease = currentEnergy - oldEnergy;
                oldEnergy = currentEnergy;
                return Dune::formatString("   % 12.5e", decrease);
            },
            "   decrease    ");

    solver.addCriterion(
            [&](){
                return Dune::formatString("   % 12.5e", step->accelerationStep().lastDampingFactor());
            },
            "   damping     ");


    solver.addCriterion(
            [&](){
                return Dune::formatString("   % 12d", step->accelerationStep().linearization().truncated().count());
            },
            "   truncated   ");


//    Vector temp(x.size());
//    auto gradientCriterion = Dune::Solvers::Criterion(
//            [&](){
//                temp = 0;
//                J.addGradient(x, temp);
//                auto err = temp.two_norm();
//                return std::make_tuple(err<tolerance, Dune::formatString("   % 12.5e", err));
//            },
//            "   |gradient|  ");

//    solver.addCriterion(
//            [&](){return Dune::formatString("   % 12.5e", J(x));},
//            "   energy      ");

//    double initialEnergy = J(x);
//    solver.addCriterion(
//            [&](){
//                static double oldEnergy=initialEnergy;
//                double currentEnergy = J(x);
//                double decrease = currentEnergy - oldEnergy;
//                oldEnergy = currentEnergy;
//                return Dune::formatString("   % 12.5e", decrease);
//            },
//            "   decrease    ");

//    solver.addCriterion(
//            [&](){return Dune::formatString("   % 12d", stepRule.evaluations_);},
//            "   evaluations ");

//    solver.addCriterion(
//            [&](){return Dune::formatString("   % 12.5e", stepRule.x_);},
//            "   step size   ");

//    solver.addCriterion(Dune::Solvers::maxIterCriterion(solver, maxIterations) | gradientCriterion);

    std::vector<double> correctionNorms;
    solver.addCriterion(Dune::Solvers::correctionNormCriterion(*step, *norm, 1e-10, correctionNorms));

//    solver.addCriterion(Dune::Solvers::correctionNormCriterion(step, norm, 1e-4));

    solver.preprocess();
    solver.solve();
    std::cout << correctionNorms.size() << std::endl;
}





template <class GridType>
bool checkWithGrid(const GridType& grid, const std::string fileName="")
{
    bool passed = true;

    static const int dim = GridType::dimension;

    typedef typename Dune::FieldMatrix<double, 2, 2> MatrixBlock;
    typedef typename Dune::FieldVector<double, 2> VectorBlock;
    typedef typename Dune::BCRSMatrix<MatrixBlock> Matrix;
    typedef typename Dune::BlockVector<VectorBlock> Vector;
    typedef typename Dune::Solvers::DefaultBitVector_t<Vector> BitVector;

    typedef typename GridType::LeafGridView GridView;
    typedef typename Dune::FieldVector<double, dim> DomainType;
    typedef typename Dune::FieldVector<double, 2> RangeType;


    const GridView gridView = grid.leafGridView();

    Matrix A;
    constructPQ1Pattern(gridView, A);
    A = 0.0;
    assemblePQ1Stiffness(gridView, A);

    Matrix M;
    constructPQ1Pattern(gridView, M);
    M = 0.0;
    assemblePQ1Mass(gridView, M);
    A += M;

    Vector rhs(A.N());
    rhs = 0;
    auto f = [](const DomainType& x) -> RangeType { return (x.two_norm() < .7) ? RangeType(200.0) : RangeType(0.0);};
    assemblePQ1RHS(gridView, rhs, f);

    Vector u(A.N());
    u = 0;

    BitVector ignore(A.N());
    ignore.unsetAll();
    markBoundaryDOFs(gridView, ignore);


    using TransferOperator = CompressedMultigridTransfer<Vector>;
    using TransferOperators = std::vector<std::shared_ptr<TransferOperator>>;

    TransferOperators transfer(grid.maxLevel());
    for (size_t i = 0; i < transfer.size(); ++i)
    {
        // create transfer operator from level i to i+1
        transfer[i] = std::make_shared<TransferOperator>();
        transfer[i]->setup(grid, i, i+1);
    }

    solveProblem(A, u, rhs, ignore, transfer);

//    if (fileName!="")
//    {
//        typename Dune::VTKWriter<GridView> vtkWriter(gridView);
//        vtkWriter.addVertexData(u, "solution");
//        vtkWriter.write(fileName);
//    }

    return passed;
}


template <int dim>
bool checkWithYaspGrid(int refine, const std::string fileName="")
{
    bool passed = true;

    typedef Dune::YaspGrid<dim> GridType;

    typename Dune::FieldVector<typename GridType::ctype,dim> L(1.0);
    typename std::array<int,dim> s;
    std::fill(s.begin(), s.end(), 1);

    GridType grid(L, s);

    for (int i = 0; i < refine; ++i)
        grid.globalRefine(1);

    std::cout << "Testing with YaspGrid<" << dim << ">" << std::endl;
    std::cout << "Number of levels: " << (grid.maxLevel()+1) << std::endl;
    std::cout << "Number of leaf nodes: " << grid.leafGridView().size(dim) << std::endl;

    passed = passed and checkWithGrid(grid, fileName);


    return passed;
}




int main(int argc, char** argv) try
{
    Dune::MPIHelper::instance(argc, argv);
    bool passed(true);


//    int refine1d = 16;
    int refine1d = 1;
    int refine2d = 6;
    int refine3d = 1;

    if (argc>1)
        std::istringstream(argv[1]) >> refine1d;
    if (argc>2)
        std::istringstream(argv[2]) >> refine2d;
    if (argc>3)
        std::istringstream(argv[3]) >> refine3d;

    passed = passed and checkWithYaspGrid<1>(refine1d, "obstacletnnmgtest_yasp_1d_solution");
    passed = passed and checkWithYaspGrid<2>(refine2d, "obstacletnnmgtest_yasp_2d_solution");
//    passed = passed and checkWithYaspGrid<3>(refine3d, "obstacletnnmgtest_yasp_3d_solution");

    return passed ? 0 : 1;
}
catch (const Dune::Exception& e) {

    std::cout << e << std::endl;
    return 1;

}

