#include "config.h"

#include <iostream>

#include <dune/common/exceptions.hh>

#include <dune/istl/matrix.hh>

#include <dune/tnnmg/functionals/trescafrictionfunctional.hh>
#include <dune/tnnmg/functionals/test/functionaltest.hh>

using namespace Dune;


template <typename Functional, typename TestPoints>
void testCoordinateRestrictionValues(const Functional& functional,
                                     const TestPoints& testPoints,
                                     const std::vector<double> testParameters)
{
  for (const auto& origin : testPoints)
  {
    auto shifted = shift(functional, origin);

    for (std::size_t i=0; i<origin.size(); i++)
    {
      auto intermediateRestriction = coordinateRestriction(shifted, i);
      auto shiftAgain = shift(intermediateRestriction, origin[i]);

      for (std::size_t j=0; j<origin[i].size(); j++)
      {
        auto restriction = coordinateRestriction(shiftAgain, j);

        // Test whether the restriction matches the functional
        for (auto v : testParameters)
        {
          // Test whether restriction really is a restriction
          auto probe = origin;
          probe[i][j] += v;

          auto restrictionValue = restriction(v);
          auto realValue        = functional(probe);

          if (fabs(restrictionValue - realValue) > 1e-6)
          {
            std::cerr << "Value of the coordinate restriction does not match the actual "
                      << "functional value." << std::endl
                      << "Restriction value at parameter " << v << " : " << restrictionValue << std::endl
                      << "Actual value: " << realValue << std::endl;
            abort();
          }

        }
      }
    }
  }
}


template <typename Functional, typename TestPoints>
void testCoordinateRestrictionSubdifferential(const Functional& functional,
                                              const TestPoints& testPoints,
                                              const std::vector<double> testParameters)
{
  for (const auto& origin : testPoints)
  {
    auto shifted = shift(functional, origin);

    for (std::size_t i=0; i<origin.size(); i++)
    {
      auto intermediateRestriction = coordinateRestriction(shifted, i);
      auto shiftAgain = shift(intermediateRestriction, origin[i]);

      for (std::size_t j=0; j<origin[i].size(); j++)
      {
        auto restriction = coordinateRestriction(shiftAgain, j);

        // Test whether the restriction matches the functional
        for (auto v : testParameters)
        {
          // Test the subdifferential of the restriction
          Solvers::Interval<double> subDifferential = restriction.subDifferential(v);

          // Step size. Best value: square root of the machine precision
          const double eps = std::sqrt(std::numeric_limits<double>::epsilon());

          auto forwardFDGradient  = (restriction(v+eps) - restriction(v)) / eps;
          auto backwardFDGradient = (restriction(v) - restriction(v-eps)) / eps;

          if (std::abs(backwardFDGradient - subDifferential[0]) > 1e4*eps
              or std::abs(forwardFDGradient - subDifferential[1]) > 1e4*eps)
          {
            std::cout << "Bug in subdifferential for coordinate restriction " << Dune::className(restriction) << ":" << std::endl;
            std::cerr << "Subdifferential doesn't match FD approximation" << std::endl;
            std::cerr << "SubDifferential: " << subDifferential
                      << ",   FD: [" << backwardFDGradient << ", " << forwardFDGradient << "]" << std::endl;
            std::cerr << "Origin:" << std::endl << origin << std::endl;
            std::cerr << "Test direction: (" << i << ", " << j << ")" << std::endl;
            std::cerr << "Parameter value: " << v << std::endl;
            DUNE_THROW(MathError,"");
          }
        }
      }
    }
  }
}

int main(int argc, char* argv[]) try
{
  // Create a Tresca friction functional on a grid with three vertices for testing
  using Vector = BlockVector<FieldVector<double,2> >;

  // TODO: TrescaFrictionFunctional currently uses the coefficient vector type
  // for the weights, too.  That's of course inappropriate, because there is
  // really only a scalar weight per Lagrange point.
  // Let's hack around that by simply having the same number twice for each Lagrange point.
  Vector weights = {{2.0, 2.0}, {3.0, 3.0}, {4.0, 4.0}};

  TrescaFrictionFunctional<Vector> functional(weights);

  // A set of local test points, i.e., values for a single vector block
  std::vector<Vector::value_type> localTestPoints = {{0,0},
                                                     {1,0},
                                                     {0,-2},
                                                     {3.14,-4}};

  // Create real test points (i.e., block vectors) from the given values for a single block
  std::vector<Vector> testPoints(localTestPoints.size()*localTestPoints.size());
  for (size_t i=0; i<testPoints.size(); i++)
    testPoints[i].resize(weights.size());

  for (size_t i=0; i<localTestPoints.size(); i++)
    for (size_t j=0; j<localTestPoints.size(); j++)
    {
      testPoints[j*localTestPoints.size()+i][0] = localTestPoints[i];
      testPoints[j*localTestPoints.size()+i][1] = localTestPoints[j];
    }

  // Test whether the functional is convex
  TNNMG::testConvexity(functional, testPoints);

  ///////////////////////////////////////////////////////////////////
  //  Test the directional restriction
  ///////////////////////////////////////////////////////////////////

  TNNMG::testDirectionalRestrictionValues(functional, testPoints, {-3, -2, -1, 0, 1, 2, 3});

  TNNMG::testDirectionalRestrictionSubdifferential(functional, testPoints, {-3, -2, -1, 0, 1, 2, 3});

  ///////////////////////////////////////////////////////////////////
  //  Test the coordinate restriction
  ///////////////////////////////////////////////////////////////////

  // TODO: FixMe
  //testCoordinateRestrictionValues(functional, testPoints, {-3, -2, -1, 0, 1, 2, 3});

  // Test whether the 'domain' method does something reasonable
  // TODO: FixMe
  //testCoordinateRestrictionSubdifferential(functional, testPoints, {-3, -2, -1, 0, 1, 2, 3});

  return 0;
} catch (Exception& e)
{
  std::cout << e.what() << std::endl;
}
