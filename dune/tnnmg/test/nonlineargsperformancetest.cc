// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:
#include <config.h>

#include <iostream>
#include <sstream>

// dune-common includes
#include <dune/common/bitsetvector.hh>
#include <dune/common/fmatrix.hh>
#include <dune/common/fvector.hh>
#include <dune/common/timer.hh>

// dune-istl includes
#include <dune/istl/bcrsmatrix.hh>

// dune-grid includes
#include <dune/grid/yaspgrid.hh>
#include <dune/grid/io/file/vtk/vtkwriter.hh>

// dune-solver includes
#include <dune/solvers/solvers/loopsolver.hh>
#include <dune/solvers/norms/energynorm.hh>
#include <dune/solvers/iterationsteps/truncatedblockgsstep.hh>

// dune-tnnmg includes
#include <dune/tnnmg/functionals/quadraticfunctional.hh>
#include <dune/tnnmg/iterationsteps/nonlineargsstep.hh>

#include <dune/solvers/test/common.hh>



template<class MatrixType, class VectorType, class BitVector>
void performanceTest(const MatrixType& mat, VectorType& x, const VectorType& rhs, const BitVector& ignore, int maxIterations=100, double tolerance=1.0e-8)
{
    using Vector = VectorType;
    using Matrix = MatrixType;

    double tNonlinearGS;
    double tTruncatedGS;
    {
        x = 0;

        using Functional = Dune::TNNMG::QuadraticFunctional<Matrix, Vector, double>;
        auto J = Functional(mat, rhs);

        auto scalarLocalSolver = [](auto& xi, const auto& restriction, const auto& ignoreI) {
            if (not ignoreI)
                xi = restriction.linearPart()/restriction.quadraticPart();
        };

        auto localSolver = [scalarLocalSolver](auto& xi, const auto& restriction, const auto& ignoreI) {
            gaussSeidelLoop(xi, restriction, ignoreI, scalarLocalSolver);
        };

        using Step = typename Dune::TNNMG::NonlinearGSStep<Functional, decltype(localSolver)>;
        using Solver = LoopSolver<Vector>;
        using Norm =  EnergyNorm<Matrix, Vector>;

        auto step = std::make_shared<Step>(J, x, localSolver);
        auto norm = std::make_shared<Norm>(mat);
        auto solver = Solver(step, 100, 0, norm, Solver::FULL);

        step->ignoreNodes_ = &ignore;

        solver.preprocess();

        for (int i=0; i<10; ++i)
        {
            x = 0;
            solver.solve();
        }
    }

    {
        x = 0;

        using Functional = Dune::TNNMG::QuadraticFunctional<Matrix, Vector, double>;
        auto J = Functional(mat, rhs);

        auto scalarLocalSolver = [](auto& xi, const auto& restriction, const auto& ignoreI) {
            if (not ignoreI)
                xi = restriction.linearPart()/restriction.quadraticPart();
        };

        auto localSolver = [scalarLocalSolver](auto& xi, const auto& restriction, const auto& ignoreI) {
            gaussSeidelLoop(xi, restriction, ignoreI, scalarLocalSolver);
        };

        using Step = typename Dune::TNNMG::NonlinearGSStep<Functional, decltype(localSolver)>;
        using Solver = LoopSolver<Vector>;
        using Norm =  EnergyNorm<Matrix, Vector>;

        auto step = std::make_shared<Step>(J, x, localSolver);
        auto norm = std::make_shared<Norm>(mat);
        auto solver = Solver(*step, 100, 0, *norm, Solver::FULL);

        step->ignoreNodes_ = &ignore;

        solver.preprocess();

        Dune::Timer timer;
        for (int i=0; i<10; ++i)
        {
            x = 0;
            solver.solve();
        }
        tNonlinearGS = timer.elapsed();
    }

    {
        x = 0;

        using Step = TruncatedBlockGSStep<Matrix, Vector, BitVector>;
        using Solver = LoopSolver<Vector>;
        using Norm =  EnergyNorm<Matrix, Vector>;

        auto step = std::make_shared<Step>(mat, x, rhs);
        auto norm = std::make_shared<Norm>(mat);
        auto solver = Solver(step, 100, 0, norm, Solver::FULL);

        step->ignoreNodes_ = &ignore;

        solver.preprocess();

        Dune::Timer timer;
        for (int i=0; i<10; ++i)
        {
            x = 0;
            solver.solve();
        }
        tTruncatedGS = timer.elapsed();
    }

    std::cout << "Solution with NonlinearGSStep took      :" << tNonlinearGS << "s" << std::endl;
    std::cout << "Solution with TruncatedBlockGSStep took :" << tTruncatedGS << "s" << std::endl;
    std::cout << "TruncatedBlockGSStep to NonlinearGSStep :" << ((tNonlinearGS / tTruncatedGS - 1.0) * 100.0) << "%" << std::endl;
}






template <class GridType>
bool checkWithGrid(const GridType& grid, const std::string fileName="")
{
    bool passed = true;

    static const int dim = GridType::dimension;

    typedef typename Dune::FieldMatrix<double, 1, 1> MatrixBlock;
    typedef typename Dune::FieldVector<double, 1> VectorBlock;
    typedef typename Dune::BCRSMatrix<MatrixBlock> Matrix;
    typedef typename Dune::BlockVector<VectorBlock> Vector;
    typedef typename Dune::BitSetVector<1> BitVector;

    typedef typename GridType::LeafGridView GridView;
    typedef typename Dune::FieldVector<double, dim> DomainType;
    typedef typename Dune::FieldVector<double, 1> RangeType;


    const GridView gridView = grid.leafGridView();

    Matrix A;
    constructPQ1Pattern(gridView, A);
    A = 0.0;
    assemblePQ1Stiffness(gridView, A);

    Matrix M;
    constructPQ1Pattern(gridView, M);
    M = 0.0;
    assemblePQ1Mass(gridView, M);
    A += M;

    Vector rhs(A.N());
    rhs = 0;
    auto f = [](const DomainType& x) -> RangeType { return (x.two_norm() < .7) ? 200 : 0;};
    assemblePQ1RHS(gridView, rhs, f);

    Vector u(A.N());
    u = 0;

    BitVector ignore(A.N());
    ignore.unsetAll();
    markBoundaryDOFs(gridView, ignore);

    performanceTest(A, u, rhs, ignore);

    if (fileName!="")
    {
        typename Dune::VTKWriter<GridView> vtkWriter(gridView);
        vtkWriter.addVertexData(u, "solution");
        vtkWriter.write(fileName);
    }

    return passed;
}


template <int dim>
bool checkWithYaspGrid(int refine, const std::string fileName="")
{
    bool passed = true;

    typedef Dune::YaspGrid<dim> GridType;

    typename Dune::FieldVector<typename GridType::ctype,dim> L(1.0);
    typename std::array<int,dim> s;
    std::fill(s.begin(), s.end(), 1);

    GridType grid(L, s);

    for (int i = 0; i < refine; ++i)
        grid.globalRefine(1);

    std::cout << "Testing with YaspGrid<" << dim << ">" << std::endl;
    std::cout << "Number of levels: " << (grid.maxLevel()+1) << std::endl;
    std::cout << "Number of leaf nodes: " << grid.leafGridView().size(dim) << std::endl;

    passed = passed and checkWithGrid(grid, fileName);


    return passed;
}




int main(int argc, char** argv) try
{
    Dune::MPIHelper::instance(argc, argv);
    bool passed(true);


//    int refine1d = 16;
    int refine1d = 1;
    int refine2d = 5;
    int refine3d = 1;

    if (argc>1)
        std::istringstream(argv[1]) >> refine1d;
    if (argc>2)
        std::istringstream(argv[2]) >> refine2d;
    if (argc>3)
        std::istringstream(argv[3]) >> refine3d;

    passed = passed and checkWithYaspGrid<1>(refine1d, "nonlineargsperformancetest_yasp_1d_solution");
    passed = passed and checkWithYaspGrid<2>(refine2d, "nonlineargsperformancetest_yasp_2d_solution");
//    passed = passed and checkWithYaspGrid<3>(refine3d, "nonlineargsperformancetest_yasp_3d_solution");

    return passed ? 0 : 1;
}
catch (const Dune::Exception& e) {

    std::cout << e << std::endl;
    return 1;

}

