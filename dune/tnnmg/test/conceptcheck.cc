// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#include <config.h>

#include <dune/common/concept.hh>
#include <dune/common/fvector.hh>
#include <dune/common/tuplevector.hh>
#include <dune/common/typetraits.hh>

#include <dune/istl/bvector.hh>

#include <dune/tnnmg/concepts.hh>

struct NonComparable {};



int main() {
  bool passed = true;
  using namespace Dune::TNNMG::Concept;

  using CT0 = int;
  using CT1 = std::tuple<double, double>;
  using CT2 = std::tuple<double, std::string>;
  using CT3 = std::tuple<CT1, CT2>;
  using CT4 = Dune::TupleVector<CT1, CT3>;

  using NCT0 = NonComparable;
  using NCT1 = std::complex<int>;
  using NCT2 = std::tuple<double, NonComparable>;
  using NCT3 = std::tuple<CT3, NCT1>;
  using NCT4 = std::tuple<NCT2, CT3>;
  using NCT5 = Dune::TupleVector<CT1, NCT3>;


  // Check comparable types
  {
    passed = passed and Dune::models<IsLessThanComparable, CT0, CT0>();
    passed = passed and Dune::models<IsLessThanComparable, CT1, CT1>();
    passed = passed and Dune::models<IsLessThanComparable, CT2, CT2>();
    passed = passed and Dune::models<IsLessThanComparable, CT3, CT3>();
    passed = passed and Dune::models<IsLessThanComparable, CT4, CT4>();
  }

  // Check non-comparable types
  {
    passed = passed and not Dune::models<IsLessThanComparable, NCT0, NCT0>();
    passed = passed and not Dune::models<IsLessThanComparable, NCT1, NCT1>();
    passed = passed and not Dune::models<IsLessThanComparable, NCT2, NCT2>();
    passed = passed and not Dune::models<IsLessThanComparable, NCT3, NCT3>();
    passed = passed and not Dune::models<IsLessThanComparable, NCT4, NCT4>();
    passed = passed and not Dune::models<IsLessThanComparable, NCT5, NCT5>();
  }

  return passed ? 0 : 1;
}
