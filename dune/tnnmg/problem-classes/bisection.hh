#ifndef BISECTION_HH
#define BISECTION_HH

#include <cmath>
#include <iomanip>

#include <dune/solvers/common/interval.hh>

/** \brief Minimize a scalar convex functional by bisection
 **/
class Bisection
{
    public:
        /** \brief Constructor
         * \param acceptError Stop if the search interval has become smaller than this number
         * \param fastQuadratic Before doing bisection assume once that the functional is quadratic
         *        and try to find the minimum that way.  This can lead to speedup.
         * \param safety acceptance factor for inexact minimization
         */
        Bisection(double acceptError = 0.0, double acceptFactor = 1.0, double requiredResidual = 1e-12,
                  double safety = 1e-14) :
            acceptFactor_(acceptFactor),
            acceptError_(acceptError),
            requiredResidual_(requiredResidual),
            safety_(safety)
        {};

        /** \brief minimize convex functional
        * 
        * Computes an approximate minimizer of the convex functional
        * @f[ f(x) = \frac 1 2 ax^2 - rx + \phi(x) @f]
        * using bisection. If \f$ x^* \f$ is the exact minimizer
        * and \f$ x^{old} \f$ is the old value the returned
        * approximation @f$ x @f$ satisfies
        * \f[
        * (x - x^{old}) \in [ factor , 1] (x^*- x^{old}).
        * \f]
        * This guarantees enough descent for every fixed \f$ factor \f$ and hence convergence of the Gauss-Seidel method.
        *
        * \param J The convex functional
        * \param x initial value
        * \param x_old old value, needed for inexact minimization
        * \param [out] count return the number of times the subdifferential of J was evaluated
        * \return approximate minimizer
        */
        template <class Functional>
        double minimize(const Functional& J, double x, double x_old, int& count, int verbosity=0) const
        {
            size_t n=0;

            Dune::Solvers::Interval<double> I;
            count = 0;

            // try if projected initial guess is sufficient
            Dune::Solvers::Interval<double> dom = J.domain();
            x = dom.projectIn(x);
            Dune::Solvers::Interval<double> DJ = J.subDifferential(x);
            ++count;
            if (DJ.containsZero(safety_))
            {
                if (verbosity > 0)
                    std::cout << "Bisection: initial iterate " << x << " accepted, DJ = " << DJ << std::endl;
                return x;
            }

            // compute initial interval
            // if quadratic part is strictly positive we can compute one bound from the other
            if (DJ[0] > 0.0)
            {
                I[1] = x;
                I[0] = dom.projectFromBelow(I[1] - DJ[0]);

                DJ = J.subDifferential(I[0]);
                ++count;
                while (DJ[0] > safety_)
                {
                    I[0] = I[1] - 2.0*(I[1]-I[0]);
                    if (I[0] < dom[0])
                    {
                        I[0] = dom[0];
                        break;
                    }
                    DJ = J.subDifferential(I[0]);
                    ++count;
                }
            }
            else
            {
                I[0] = x;
                I[1] = dom.projectFromAbove(I[0] - DJ[1]);

                DJ = J.subDifferential(I[1]);
                ++count;
                while (DJ[1] < -safety_)
                {
                    I[1] = I[0] - 2.0*(I[0]-I[1]);
                    if (I[1] > dom[1])
                    {
                        I[1] = dom[1];
                        break;
                    }
                    DJ = J.subDifferential(I[1]);
                    ++count;
                }
            }

            if (verbosity>0)
            {
                std::cout << " #  |      I[0]    |       x      |      I[1]    |   dJ(I[0])   |     dJ(x)    |   dJ(I[1])   |" << std::endl;
                std::cout << "----|--------------|--------------|--------------|--------------|--------------|--------------|" << std::endl;
            }

            // compute midpoint
            x = (I[0] + I[1]) / 2.0;

            // We remember the value of x from the last loop in order
            // to check if the iteration becomes stationary. This
            // is necessary to guarantee termination because it
            // might happen, that I[0] and I[1] are successive
            // floating point numbers, while the values at both
            // interval boundaries do not match the normal
            // termination criterion.
            //
            // Checking ((I[0] < x) and (x < I[1])) does not work
            // since this is true in the above case if x is taken
            // from the register of a floating point unit that
            // uses a higher accuracy internally
            //
            // The above does e.g. happen with the x87 floating
            // point unit that uses 80 bit internally. In this
            // specific case the problem could be avoided by
            // changing the x87 rounding mode with
            //
            //   int mode = 0x27F;
            //   asm ("fldcw %0" : : "m" (*&mode));
            //
            // In the general case one can avoid the problem at least
            // for the equality check using a binary comparison
            // that circumvents the floating point unit.
            double xLast = I[0];

            // iterate while mid point does not coincide with boundary numerically
            while (not(binaryEqual(x, xLast)))
            {
                ++n;

                // evaluate subdifferential
                DJ = J.subDifferential(x);
                ++count;

                if (verbosity>0)
                {
                    Dune::Solvers::Interval<double> DJ0 = J.subDifferential(I[0]);
                    Dune::Solvers::Interval<double> DJ1 = J.subDifferential(I[1]);

                    const std::streamsize p = std::cout.precision();

                    std::cout << std::setw(4) << n << "|"
                              << std::setprecision(8)
                              << std::setw(14) << I[0] << "|"
                              << std::setw(14) << x << "|"
                              << std::setw(14) << I[1] << "|"
                              << std::setw(14) << DJ0 << "|"
                              << std::setw(14) << DJ << "|"
                              << std::setw(14) << DJ1 << "|" << std::endl;

                    std::cout << std::setprecision(p);
                }

                // stop if at least 'factor' of full minimization step is realized
                // if DJ[0]<= 0 we have
                //
                //    x_old <= I[0] <= x <= x* <= I[1]
                //
                // and hence the criterion
                //
                //    alpha|x*-x_old| <= alpha|I[1]-x_old| <= |x-x_old| <= |x*-x_old|
                //
                // similar for DJ[1] >= 0
                if ((I[0]>=x_old) and (DJ[0]<=0.0) and (std::abs((x-x_old)/(I[1]-x_old)) >= acceptFactor_) and (-requiredResidual_ <= DJ[1]))
                    break;
                if ((I[1]<=x_old) and (DJ[1]>=0.0) and (std::abs((x-x_old)/(I[0]-x_old)) >= acceptFactor_) and (DJ[0] <= requiredResidual_))
                    break;

                // stop if error is less than acceptError_
                if (std::abs(I[1]-I[0]) <= acceptError_)
                    break;

                // stop if 0 in DJ numerically
                if (DJ.containsZero(safety_))
                    break;

                // adjust bounds and compute new mid point
                if (DJ[0] > 0.0)
                    I[1] = x;
                else
                    I[0] = x;

                xLast = x;

                x = (I[0] + I[1]) / 2.0;
            }

            return x;
        }


    private:

        /**
         * \brief Binary check for equality
         *
         * This method performs a binary comparison by per byte
         * checks for equality.
         *
         * When dealing with floating point numbers the normal check for
         * equality is useless even if you are aware of rounding errors
         * because the the result will depend on the compilers decisions
         * even in simple cases
         *
         * If you use the x87 floating point unit in 'normal' mode it uses
         * 80bit floats in 80 bit registers. This results in the fact that
         * the same computation yield different results depending on
         * whether its result is taken directly from the register or
         * from the memory. For example
         *
         * double c=3.0/7.0;
         * std::cout << c << std::endl;
         * std::cout << (c==3.0/7.0) << std::endl;
         *
         * may result in
         * 
         * 3.0
         * 0
         *
         * because c is forced to be written to memory and thus rounded to
         * 64bit due to the output while the result of 3.0/7.0 in the comparison
         * can be taken directly from the register in 80 bit.
         *
         * For more complicated algorithms the decision depends on which variables
         * are loaded from memory and which are used from the register.
         *
         * By accessing the single bytes the arguments are forced to be read from
         * memory or cache instead of the floating point units 80 bit registers.
         *
         * BTW:
         * This problem is not present in the MMX and SSE floating point
         * units since they use 64 bit only. You can also change the rounding
         * mode for the x87 unit using the proper inline assembler command
         * (see above) the change this fact.
         *
         * \tparam T Type of the arguments to be compared.
         *
         * \param a First argument to be compared.
         * \param b Second argument to be compared.
         */
        template<class T>
        static inline bool binaryEqual(const T& a, const T& b)
        {
            // These casts and accessing the bytes using the resulting pointer
            // are explicitly allowed by the standard and do NOT violate the
            // strict aliasing rule!
            const char* pA = reinterpret_cast<const char*>(&a);
            const char* pB = reinterpret_cast<const char*>(&b);
            for (size_t i=0; i<sizeof(T); ++i)
                if (pA[i] != pB[i])
                    return false;
            return true;
        }

        double acceptFactor_;
        double acceptError_;
        double requiredResidual_;
        double safety_;

};

#endif
