#ifndef SIMPLEX_CONSTRAINED_TNNMG_PROBLEM_HH
#define SIMPLEX_CONSTRAINED_TNNMG_PROBLEM_HH

#include <string>
#include <sstream>
#include <algorithm>

#include <dune/common/parametertree.hh>
#include <dune/common/bitsetvector.hh>
#include <dune/istl/bcrsmatrix.hh>

#include "dune/solvers/common/staticmatrixtools.hh"

#include "nonlinearity.hh"
#include "convexproblem.hh"
#include "directionalconvexfunction.hh"
#include "blocknonlineartnnmgproblem.hh"
#include "simplexconstrainedgsproblem.hh"
#include "simplexprojectionconvexfunction.hh"



/**
 * \brief Linearization for SimplexConstrainedTNNMG using the spanning system of all simplex edges
 *
 * Using this linearization type the coarse system is assembled in terms
 * of the spanning system consisting of all simplex edge vectors.
 * For an N-dimemsional (local) simplex this will result in an
 * M-dimensional (local) system with M=N*(N-1)/2.
 *
 * The restriction of the system to a locally smooth subspace is
 * achieved by setting rows and columns of the truncated edges
 * directions to zero.
 *
 * This can be viewed as a variant of the approach by Kornhuber&Krause
 * for the vector valued Allen-Cahn equation, but without coarse
 * grid constraints.
 */
template <class ConvexProblemType>
struct SimplexConstrainedTNNMGAllEdgeLinearization
{
    static const int base_block_size = ConvexProblemType::block_size;
    static const int block_size = base_block_size*(base_block_size-1)/2;

    typedef Dune::BCRSMatrix< Dune::FieldMatrix<double,block_size,block_size> > MatrixType;
    typedef Dune::BlockVector< Dune::FieldVector<double,block_size> > VectorType;
    typedef Dune::BitSetVector<block_size> BitVectorType;
    typedef Dune::FieldMatrix<signed char,base_block_size,block_size> LocalTransformationType;

    MatrixType A;
    VectorType b;
    BitVectorType ignore;

    std::vector<LocalTransformationType> T;

    void setupTruncation(const typename Dune::BitSetVector<base_block_size>& baseTruncation)
    {
        T.resize(baseTruncation.size());
        for(int i=0; i<T.size(); ++i)
        {
            T[i] = 0.0;
            int k=0;
            for(int j1=0; j1<(base_block_size-1); ++j1)
            {
                for(int j2=j1+1; j2<base_block_size; ++j2)
                {
                    if (not(baseTruncation[i][j1]) and not(baseTruncation[i][j2]))
                    {
                        T[i][j1][k] = 1;
                        T[i][j2][k] = -1;
                    }
                    ++k;
                }
            }
        }
    }
};



/**
 * \brief Linearization for SimplexConstrainedTNNMG using a projection to the constrained subspace
 *
 * Using this linearization type the coarse system is assembled in terms
 * of the basis of the surrounding space for an N-dimemsional (local)
 * simplex this will result in an N-dimensional (local) system.
 *
 * The restriction of the system to a locally smooth subspace is
 * achieved by projecting into the space spanned by all non-truntaced
 * edge directions.
 */
template <class ConvexProblemType>
struct SimplexConstrainedTNNMGProjectedLinearization
{
    static const int base_block_size = ConvexProblemType::block_size;
    static const int block_size = base_block_size;

    typedef Dune::BCRSMatrix< Dune::FieldMatrix<double,block_size,block_size> > MatrixType;
    typedef Dune::BlockVector< Dune::FieldVector<double,block_size> > VectorType;
    typedef Dune::BitSetVector<block_size> BitVectorType;
    typedef Dune::FieldMatrix<double,base_block_size,block_size> LocalTransformationType;

    MatrixType A;
    VectorType b;
    BitVectorType ignore;

    std::vector<LocalTransformationType> T;

    void setupTruncation(const typename Dune::BitSetVector<base_block_size>& baseTruncation)
    {
        T.resize(baseTruncation.size());
        for(int i=0; i<T.size(); ++i)
        {
            T[i] = 0.0;
            int count=base_block_size-baseTruncation[i].count();
            for(int j1=0; j1<base_block_size; ++j1)
            {
                if (not(baseTruncation[i][j1]))
                    T[i][j1][j1] = 1;
                for(int j2=0; j2<base_block_size; ++j2)
                {
                    if (not(baseTruncation[i][j1]) and not(baseTruncation[i][j2]))
                        T[i][j1][j2] -= 1.0/count;
                }
            }
        }
    }
};



/**
 * \brief Linearization for SimplexConstrainedTNNMG using a projection to and reduced basis of to the constrained subspace
 *
 * Using this linearization type the coarse system is assembled in terms
 * of a reduced basis that is capable to represent all possible subspaces.
 * Since the (1,..,1)-vector is orthogonal to all such subspaces
 * it is enough to use an (N-1)-dimensional (local) system for an
 * N-dimemsional (local) simplex.
 *
 * The restriction of the system to a locally smooth subspace is
 * achieved by projecting into the space spanned by all non-truntaced
 * edge directions and representing the system by the following
 * basis of the (N-1)-dimensional space spanned by all edges:
 *
 * (1,-1,0,...,0), (0,1,-1,0,...,0), ..., (0,...,0,1,-1)
 *
 */
template <class ConvexProblemType>
struct SimplexConstrainedTNNMGReducedProjectedLinearization
{
    static const int base_block_size = ConvexProblemType::block_size;
    static const int block_size = base_block_size-1;

    typedef Dune::BCRSMatrix< Dune::FieldMatrix<double,block_size,block_size> > MatrixType;
    typedef Dune::BlockVector< Dune::FieldVector<double,block_size> > VectorType;
    typedef Dune::BitSetVector<block_size> BitVectorType;
    typedef Dune::FieldMatrix<double,base_block_size,block_size> LocalTransformationType;

    MatrixType A;
    VectorType b;
    BitVectorType ignore;

    std::vector<LocalTransformationType> T;

    void setupTruncation(const typename Dune::BitSetVector<base_block_size>& baseTruncation)
    {
        T.resize(baseTruncation.size());
        for(int i=0; i<T.size(); ++i)
        {
            T[i] = 0.0;
            int count=base_block_size-baseTruncation[i].count();
            for(int j=0; j<block_size; ++j)
            {
                if (not(baseTruncation[i][j]))
                    T[i][j][j] = 1;
                if (not(baseTruncation[i][j+1]))
                    T[i][j+1][j] = -1;
                double factor = (T[i][j][j] + T[i][j+1][j]) / count;
                for(int k=0; k<base_block_size; ++k)
                    if (not(baseTruncation[i][k]))
                        T[i][k][j] -= factor;
            }
        }
    }
};







template <class ConvexProblemTypeTEMPLATE, template<class> class LinearizationTEMPLATE=SimplexConstrainedTNNMGAllEdgeLinearization >
class SimplexConstrainedTNNMGProblem :
    private BlockNonlinearTNNMGProblem<ConvexProblemTypeTEMPLATE>,
    public SimplexConstrainedGSProblem<ConvexProblemTypeTEMPLATE>
{
    private:
        // typedef to ease notation
        typedef BlockNonlinearTNNMGProblem<ConvexProblemTypeTEMPLATE> BaseClass;

        template<class Vector>
        static bool projectToSimplex(typename Vector::value_type sum, const Vector& x, Vector& y)
        {
            typedef typename Vector::value_type Field;

            y = x;
            std::sort(y.begin(), y.end(), std::greater<Field>());
            int N = x.size();

            Field S = y[0]-sum;
            Field lambda = S;

            // i will be the max index such that lambda <= y[i]
            int i=0;
            for(i=1; i<N; ++i)
            {
                if (y[i]<lambda)
                    break;
                S += y[i];
                lambda = S/(i+1);
            }
            for(int j=0; j<N; ++j)
                y[j] = std::max(x[j]-lambda, Field(0));

            return i < N;
        }

    public:
        typedef ConvexProblemTypeTEMPLATE ConvexProblemType;
        typedef typename ConvexProblemType::NonlinearityType NonlinearityType;
        typedef typename ConvexProblemType::VectorType VectorType;
        typedef typename ConvexProblemType::MatrixType MatrixType;
        typedef typename ConvexProblemType::LocalVectorType LocalVectorType;
        typedef typename ConvexProblemType::LocalMatrixType LocalMatrixType;

        static const int block_size = ConvexProblemType::block_size;

        typedef LinearizationTEMPLATE<ConvexProblemType> Linearization;

        static const int coarse_block_size = Linearization::block_size;


        SimplexConstrainedTNNMGProblem(const Dune::ParameterTree& _config, ConvexProblemType& problem) :
            BlockNonlinearTNNMGProblem<ConvexProblemTypeTEMPLATE>(_config, problem),
            SimplexConstrainedGSProblem<ConvexProblemTypeTEMPLATE>(_config, problem)
        {
            projectExactly = config.get("project_exactly", true);
            projectByLocalDamping = config.get("project_by_local_damping", false);
            projectionSafety_ = config.get("projectionSafety", 1e-15);
        };


        // we need to select the stuff for the generic GS since
        // this type and method appear also in BlockNonlinearGSProblem
        typedef typename SimplexConstrainedGSProblem<ConvexProblemTypeTEMPLATE>::IterateObject IterateObject;
        using SimplexConstrainedGSProblem<ConvexProblemTypeTEMPLATE>::getIterateObject;


        // include computeDampingParameter explcitly since
        // we derive privat from BlockNonlinearTNNMGProblem
        // to be sure to hide anything
        using BlockNonlinearTNNMGProblem<ConvexProblemTypeTEMPLATE>::computeDampingParameter;


        void assembleTruncate(const VectorType& u, Linearization& linearization, const Dune::BitSetVector<block_size>& ignore) const
        {
            // get reference to resolve ambiguity
            ConvexProblemType& problem = BlockNonlinearTNNMGProblem<ConvexProblemTypeTEMPLATE>::problem_;

            // we do not need to ignore components explicitly since they are already truncated by BlockNonlinearTNNMGProblem
            linearization.ignore.resize(problem.A.N());
            linearization.ignore.unsetAll();

            // assemble truncated hessian in canonical coordinates
            typename BaseClass::Linearization baseLinearization;
            BaseClass::setupTruncationPattern(u, baseLinearization, ignore);
            BaseClass::assembleCoarseSystem(u, baseLinearization, ignore);

            for(int j=0; j<BaseClass::Linearization::block_size; ++j)
                outStream << std::setw(9) << baseLinearization.truncation.countmasked(j);

            // copy sparsity pattern for linearization
            typename NonlinearityType::IndexSet indices(problem.A.N(), problem.A.M());
            indices.import(baseLinearization.A);

            // construct matrix from pattern and initialize it
            indices.exportIdx(linearization.A);
            linearization.A = 0.0;

            linearization.b.resize(u.size());
            linearization.b = 0.0;

            linearization.setupTruncation(baseLinearization.truncation);

            typename BaseClass::Linearization::MatrixType::row_type::Iterator it;
            typename BaseClass::Linearization::MatrixType::row_type::Iterator end;
            for(int row=0; row<baseLinearization.A.N(); ++row)
            {
                linearization.T[row].umtv(baseLinearization.b[row], linearization.b[row]);

                it = baseLinearization.A[row].begin();
                end = baseLinearization.A[row].end();
                for(; it!=end; ++it)
                {
                    int col = it.index();
                    StaticMatrix::addTransformedMatrix(linearization.A[row][col], linearization.T[row], *it, linearization.T[col]);
                }
            }
        };


        void projectCoarseCorrection(
            const VectorType& u,
            const typename Linearization::VectorType& v,
            VectorType& projected_v,
            const Linearization& linearization) const
        {
            // get reference to resolve ambiguity
            ConvexProblemType& problem = BlockNonlinearTNNMGProblem<ConvexProblemTypeTEMPLATE>::problem_;

            // transformation (contains truncation) of correction v = Tv*
            projected_v.resize(v.size());
            for(int i=0; i<u.size(); ++i)
                linearization.T[i].mv(v[i], projected_v[i]);

            Bisection bisection;

            // project correction in convex set
            int projectedCount = 0;
            for (int i = 0; i < projected_v.size(); ++i)
            {
                LocalVectorType defectObstacle;
                Dune::Solvers::Interval<double> domain;
                bool violation = false;
                for(int j=0; j<block_size; ++j)
                {
                    problem.phi.domain(i, domain, j);
                    defectObstacle[j] = domain[0] - u[i][j];
                    if (projected_v[i][j] < defectObstacle[j])
                        violation = true;
                }
                if (violation)
                {
                    double sum=0;
                    for(int j=0; j<block_size; ++j)
                        sum -= defectObstacle[j];
                    defectObstacle += projectionSafety_;
                    ++projectedCount;

                    projectToSimplex(sum, projected_v[i] - defectObstacle, projected_v[i]);

                    projected_v[i] += defectObstacle;
                }
//                LocalVectorType lower;
//                Dune::Solvers::Interval<double> domain;
//                bool violation = false;
//                for(int j=0; j<block_size; ++j)
//                {
//                    problem.phi.domain(i, domain, j);
//                    lower[j] = domain[0] - u[i][j];
//                    if (projected_v[i][j] < lower[j])
//                        violation = true;
//                }
//                if (violation)
//                {
//                    ++projectedCount;
//                    if (projectExactly)
//                    {
//                        SimplexProjectionConvexFunction<LocalVectorType> projectionSchurComplement(lower, projected_v[i], 0.0);

//                        int bisectionCounter = 0;
//                        double lambda = bisection.minimize(projectionSchurComplement, 0, 0, bisectionCounter);
//                        projectionSchurComplement.getProjection(lambda, projected_v[i]);
//                    }
//                    if (projectByLocalDamping)
//                    {
//                        double local_project_factor = 1.0;
//                        double component_project_factor = 1.0;
//                        for(int j=0; j<block_size; ++j)
//                        {
//                            if (projected_v[i][j] < lower[j])
//                                component_project_factor = lower[j] / projected_v[i][j];
//                            if (component_project_factor < local_project_factor)
//                                local_project_factor = component_project_factor;
//                        }
//                        projected_v[i] *= local_project_factor;
//                    }
//                }
            }
            outStream << std::setw(9) << projectedCount;
        };


        virtual std::string getOutput(bool header=false) const
        {
            if (header)
            {
                outStream.str("");
                for(int j=0; j<BaseClass::Linearization::block_size; ++j)
                    outStream << "  trunc" << std::setw(2) << j;
                outStream << "  project";
            }
            std::string s = outStream.str();
            outStream.str("");
            return s;
        }

        using BlockNonlinearTNNMGProblem<ConvexProblemTypeTEMPLATE>::computeEnergy;


    private:
        typedef typename Linearization::LocalTransformationType LocalTransformationType;

        bool projectExactly;
        bool projectByLocalDamping;
        double projectionSafety_;

    protected:
        // solver parameters
        using BlockNonlinearTNNMGProblem<ConvexProblemTypeTEMPLATE>::config;

        // store output data
        mutable std::ostringstream outStream;
};

#endif

