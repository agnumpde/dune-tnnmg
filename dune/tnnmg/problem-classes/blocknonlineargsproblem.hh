#ifndef BLOCK_NONLINEAR_GS_PROBLEM_HH
#define BLOCK_NONLINEAR_GS_PROBLEM_HH

#include <dune/common/parametertree.hh>
#include <dune/common/bitsetvector.hh>

#include "nonlinearity.hh"
#include "convexproblem.hh"
#include "bisection.hh"
#include "onedconvexfunction.hh"

/** \brief Base class for problems where each block can be solved with a scalar Gauss-Seidel method */
template <class ConvexProblemTypeTEMPLATE>
class BlockNonlinearGSProblem
{
    public:
        typedef ConvexProblemTypeTEMPLATE ConvexProblemType;
        typedef typename ConvexProblemType::NonlinearityType NonlinearityType;
        typedef typename ConvexProblemType::VectorType VectorType;
        typedef typename ConvexProblemType::MatrixType MatrixType;
        typedef typename ConvexProblemType::LowRankFactorType LowRankFactorType;
        typedef typename LowRankFactorType::block_type LowRankMatrixBlockType;
        typedef typename ConvexProblemType::LocalVectorType LocalVectorType;
        typedef typename ConvexProblemType::LocalMatrixType LocalMatrixType;

        static const unsigned int block_size = ConvexProblemType::block_size;

        /** \brief Does one iteration of a scalar Gauss-Seidel method for the local problem*/
        class IterateObject;


        BlockNonlinearGSProblem(const Dune::ParameterTree& config, ConvexProblemType& problem) :
            problem_(problem)
        {
            bisection = Bisection(0.0,
                    config.get("smoother.gs_acceptance", 1.0),
                    config.get("smoother.gs_tol", 1e-15),
                    config.get("smoother.safety", 1e-14));
        };

        /** \brief Constructs and returns an iterate object */
        IterateObject getIterateObject()
        {
            return IterateObject(bisection, problem_);
        }


    protected:
        // problem data
        ConvexProblemType& problem_;

        // commonly used minimization stuff
        Bisection bisection;
};



/** \brief Solves one local system using a scalar Gauss-Seidel method */
template <class ConvexProblemTypeTEMPLATE>
class BlockNonlinearGSProblem<ConvexProblemTypeTEMPLATE>::IterateObject
{
    public:

        /** \brief Constructor, protected so only friends can instantiate it
         *
         *  \param bisection The class used to do a scalar bisection
         *  \param problem The problem including quadratic part and nonlinear/nonsmooth part
         */
        IterateObject(const Bisection& bisection, ConvexProblemType& problem) :
            problem_(problem),
            bisection(bisection),
            local_J(1.0, 0.0, problem_.phi, 0, 0)
        {};


        /** \brief Set the current iterate */
        void setIterate(VectorType& u)
        {
            // only touch the lowrankFactor if its coefficient is not zero; it might not be initialized otherwise
            if (problem_.am != 0.0)
            {
                // s = problem_.Am*u;
                // so far we only allow rank block_size terms
                s.resize(problem_.lowRankFactor_.N());
                problem_.lowRankFactor_.mv(u,s);
            }

            problem_.phi.setVector(u);

            this->u = u;
        };

        /** \brief Update the i-th block of the current iterate */
        void updateIterate(const LocalVectorType& ui, int i)
        {
            // only touch the lowrankFactor if its coefficient is not zero; it might not be initialized otherwise
            if (problem_.am != 0.0)
            {
                // s += (ui-u[i]) * problem_.Am[i];
                for (size_t k=0; k<problem_.lowRankFactor_.N(); ++k)
                    problem_.lowRankFactor_[k][i].umv(ui-u[i],s[k]);
            }

            for(unsigned int j=0; j<block_size; ++j)
                problem_.phi.updateEntry(i, ui[j], j);

            u[i] = ui;
        };


        /** \brief Do one iteration of a scalar Gauss-Seidel method for the local problem
         *
         * \param[out] ui The solution
         * \param[in] i Block number
         * \param[in] ignore Set of degrees of freedom to leave untouched
         * 
         * \return The minimizer of the local functional in the variable ui
         */
        void solveLocalProblem(LocalVectorType& ui, int i, typename Dune::BitSetVector<block_size>::const_reference ignore)
        {
            /*  Here we compute the residual for index i as
             *
             *  b = f[i] - Ru^n - Lu^n+1
             *
             *  and extract the i-th block of the matrix.
             *  Note that the D in Du^n+1 = b is block-diagonal.
             *
             *  When considering the low rank term special care must be taken to sum up
             *  correctly, i.e. not include the diagonal entry in the residual.
             */
            const LocalMatrixType* Aii=0;
            LocalVectorType b(0.0);

            typename MatrixType::row_type::ConstIterator it = problem_.A[i].begin();
            typename MatrixType::row_type::ConstIterator end = problem_.A[i].end();
            for(; it !=end; ++it)
            {
                int col = it.index();
                if (col == i)
                    Aii = &(*it);
                else
                    it->mmv(u[col], b);
            }
            b *= problem_.a;

            if (problem_.am != 0.0)
            {
                VectorType sMinusDiagonalElement = s;
                problem_.lowRankFactor_[0][i].mmv(u[i], sMinusDiagonalElement[0]);
                problem_.lowRankFactor_[0][i].usmtv(-problem_.am, sMinusDiagonalElement[0], b);
            }
            b += problem_.f[i];
            /* - */

            ui = u[i];
            LocalVectorType ui_old = ui;
            local_J.i = i;

            for(unsigned int j=0; j<block_size; ++j)
            {
                if(ignore.test(j))
                    continue;

                local_J.A = problem_.a * (*Aii)[j][j];
                local_J.b = b[j];
                local_J.j = j;

                /* Do the same as above, just on the block level. We don't need to do this for the low rank term since its blocks are assumed to be diagonal anyway */
                typename LocalMatrixType::row_type::ConstIterator it = (*Aii)[j].begin();
                typename LocalMatrixType::row_type::ConstIterator end = (*Aii)[j].end();
                for(; it!=end; ++it)
                    if (it.index()!=j)
                        local_J.b -= (*it) * problem_.a * ui[it.index()];

                // only touch the lowrankFactor if its coefficient is not zero; it might not be initialized otherwise
                if (problem_.am != 0.0)
                    local_J.A += problem_.am*(problem_.lowRankFactor_[0][i].diagonal(j))*(problem_.lowRankFactor_[0][i].diagonal(j));

                // Use bisection if quadratic minimization fails
                ui[j] = local_J.domain().projectIn(local_J.b/local_J.A);
                if (not local_J.subDifferential(ui[j]).containsZero(1e-14))
                  ui[j] = bisection.minimize(local_J, ui[j], ui[j], bisectionsteps);

                // we need to update the intermediate local values ...
                problem_.phi.updateEntry(i, ui[j], j);
            }
            // ... and to restore the old ones after computation
            for(unsigned int j=0; j<block_size; ++j)
                problem_.phi.updateEntry(i, ui_old[j], j);
        };


    private:

        //! problem data
        ConvexProblemType& problem_;

        // commonly used minimization stuff
        Bisection bisection;
        OneDConvexFunction<NonlinearityType> local_J;

        /** state data for smoothing procedure used by:
          * setIterate, updateIterate, solveLocalProblem 
          */
        VectorType u;

        //! temporary for handling low rank terms
        VectorType s;

        /** \brief Keeps track of the total number of bisection steps that were performed */
        int bisectionsteps;
};

#endif

