#ifndef ONED_CONVEX_FUNCTION_HH
#define ONED_CONVEX_FUNCTION_HH

#include "nonlinearity.hh"

template<class NonlinearityType>
class OneDConvexFunction
{
	public:

        /** \brief Constructor
         * \param A Quadratic part
         * \param b linear part
         * \param phi nonlinear part
         * \param i Gets handed to the nonlinearity phi, and is used there as the global dof number in an FEM problem
         */
		OneDConvexFunction(double A, double b, const NonlinearityType& phi, int i, int j) :
			A(A),
			b(b),
			i(i),
			j(j),
			phi_(phi)
		{};
		
        /** \brief Return the quadratic part */
		double quadraticPart() const
		{
			return A;
		}
		
		/** \brief Return the linear part */
		double linearPart() const
		{
			return b;
		}
		
		/** \brief Get the subdifferential at x
         * \param x argument
         * \param[out] D The subdifferential, which may be an interval if the function is not differentiable at x
         */
		auto subDifferential(double x) const
		{
      Dune::Solvers::Interval<double> D;
			phi_.subDiff(i, x, D, j);
			D[0] += A*x - b;
			D[1] += A*x - b;
			return D;
		};
		
		auto domain() const
		{
      Dune::Solvers::Interval<double> domain;
			phi_.domain(i, domain, j);
			return domain;
		};
		
		double A;
		double b;
		int i;
		int j;
	
	private:
		const NonlinearityType& phi_;
};

#endif

