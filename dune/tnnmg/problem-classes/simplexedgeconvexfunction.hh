#ifndef SIMPLEX_EDGE_CONVEX_FUNCTION_HH
#define SIMPLEX_EDGE_CONVEX_FUNCTION_HH

#include "nonlinearity.hh"
#include <dune/solvers/common/interval.hh>

template<class NonlinearityType>
class SimplexEdgeConvexFunction
{
    public:
        typedef typename NonlinearityType::LocalVectorType LocalVectorType;

        SimplexEdgeConvexFunction(NonlinearityType& phi) :
            phi(phi)
        {};

        double quadraticPart() const
        {
            return A;
        }

        double linearPart() const
        {
            return b;
        }

        auto subDifferential(double x) const
        {
            Dune::Solvers::Interval<double> D;
            // set local values both directions
            phi.updateEntry(i, ui[j1] + x, j1);
            phi.updateEntry(i, ui[j2] - x, j2);

            // initialize by derivative of quadratic part
            D[0] = A*x-b;
            D[1] = D[0];

            Dune::Solvers::Interval<double> Dj;

            Dj[0] = 0.0;
            Dj[1] = 0.0;

            // incorporate first directional subdifferential
            phi.subDiff(i, ui[j1] + x, Dj, j1);
            D[0] += Dj[0];
            D[1] += Dj[1];

            // incorporate second directional subdifferential
            phi.subDiff(i, ui[j2] - x, Dj, j2);
            D[0] -= Dj[1];
            D[1] -= Dj[0];

            // restore local values both directions
            phi.updateEntry(i, ui[j1], j1);
            phi.updateEntry(i, ui[j2], j2);
            return D;
        };

        auto domain() const
        {
            Dune::Solvers::Interval<double> domain;
            // initialize by first relative directional domain
            phi.domain(i, domain, j1);
            domain[0] -= ui[j1];
            domain[1] -= ui[j1];

            // compute second relative directional domain
            // notice that the order is switched
            Dune::Solvers::Interval<double> domainj2;

            domainj2[0] = 0.0;
            domainj2[1] = 0.0;

            phi.domain(i, domainj2, j2);
            domainj2[0] = ui[j2]-domainj2[0];
            domainj2[1] = ui[j2]-domainj2[1];

            // check if second domain is more restrictive
            if (domain[0] < domainj2[1])
                domain[0] = domainj2[1];
            if (domain[1] > domainj2[0])
                domain[1] = domainj2[0];
            return domain;
        };

        double A;
        double b;
        int i;
        int j1;
        int j2;

        LocalVectorType ui;

    private:
        NonlinearityType& phi;
};

#endif

