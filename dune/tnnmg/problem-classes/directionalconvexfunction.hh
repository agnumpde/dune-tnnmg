#ifndef DIRECTIONAL_CONVEX_FUNCTION_HH
#define DIRECTIONAL_CONVEX_FUNCTION_HH

#include <dune/solvers/common/interval.hh>

/** \brief Class representing a convex function along a given direction
 *
 *  The function is assumed to be of the form \f$ f(x) = (A(u+xv),u+xv) - (b,u+xv)  + \phi(u+xv)\f$
 */
template<class NonlinearityType>
class DirectionalConvexFunction
{
    public:
        typedef typename NonlinearityType::VectorType VectorType;
        typedef typename NonlinearityType::MatrixType MatrixType;

        DirectionalConvexFunction(double A, double b, NonlinearityType& phi, const VectorType& u, const VectorType& v) :
            A(A),
            b(b),
            phi_(phi),
            u_(u),
            v_(v),
            temp_u_(u)
        {
            phi_.directionalDomain(u_, v_, dom_);
        }

        double operator()(double x) const
        {
            temp_u_ = u_;
            temp_u_.axpy(x, v_);
            return 0.5*A*x*x - b*x + phi_(temp_u_);
        }

        double quadraticPart() const
        {
            return A;
        }

        double linearPart() const
        {
            return b;
        }

        auto subDifferential(double x) const
        {
            Dune::Solvers::Interval<double> D;
            temp_u_ = u_;
            temp_u_.axpy(x, v_);
            phi_.directionalSubDiff(temp_u_, v_, D);
            D[0] += A*x - b;
            D[1] += A*x - b;
            return D;
        }

        auto domain() const
        {
			    return dom_;
        }

        double A;
        double b;

    private:
        NonlinearityType& phi_;
        const VectorType& u_;
        const VectorType& v_;

        mutable VectorType temp_u_;
        Dune::Solvers::Interval<double> dom_;
};

#endif

