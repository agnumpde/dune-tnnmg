#ifndef SIMPLEX_PROJECTION_CONVEX_FUNCTION_HH
#define SIMPLEX_PROJECTION_CONVEX_FUNCTION_HH

#include <dune/solvers/common/interval.hh>

template <class VectorType>
class SimplexProjectionConvexFunction
{
    public:

        SimplexProjectionConvexFunction(const VectorType& lowerObst, const VectorType& u, double sum) :
            lowerObst(lowerObst),
            u(u),
            sum(sum)
        {};

        double quadraticPart() const
        {
            return 0.0;
        }

        double linearPart() const
        {
            return sum;
        }

        auto subDifferential(double lambda) const
        {
            Dune::Solvers::Interval<double> D;
            D[0] = sum;
            for(int j=0; j<u.N(); ++j)
            {
                double Puj = u[j]-lambda;
                if (Puj<lowerObst[j])
                    Puj = lowerObst[j];
                D[0] -= Puj;
            }
            D[1] = D[0];
            return D;
        };

        auto domain() const
        {
            Dune::Solvers::Interval<double> domain;
            domain[1] =  std::numeric_limits<double>::max();
            domain[0] = -std::numeric_limits<double>::max();
            return domain;
        };

        void getProjection(double lambda, VectorType& Pu)
        {
            for(int j=0; j<u.N(); ++j)
            {
                Pu[j] = u[j]-lambda;
                if (Pu[j]<lowerObst[j])
                    Pu[j] = lowerObst[j];
            }
        };

    private:
        double sum;
        const VectorType& lowerObst;
        const VectorType& u;
};

#endif

