#ifndef SIMPLEX_CONSTRAINED_GS_PROBLEM_HH
#define SIMPLEX_CONSTRAINED_GS_PROBLEM_HH

#include <dune/common/parametertree.hh>
#include <dune/common/bitsetvector.hh>

#include "dune/solvers/common/staticmatrixtools.hh"

#include "nonlinearity.hh"
#include "convexproblem.hh"
#include "bisection.hh"
#include "simplexedgeconvexfunction.hh"

template <class ConvexProblemTypeTEMPLATE>
class SimplexConstrainedGSProblem
{
    public:
        typedef ConvexProblemTypeTEMPLATE ConvexProblemType;
        typedef typename ConvexProblemType::NonlinearityType NonlinearityType;
        typedef typename ConvexProblemType::VectorType VectorType;
        typedef typename ConvexProblemType::MatrixType MatrixType;
        typedef typename ConvexProblemType::LocalVectorType LocalVectorType;
        typedef typename ConvexProblemType::LocalMatrixType LocalMatrixType;

        static const int block_size = ConvexProblemType::block_size;

        class IterateObject;


        SimplexConstrainedGSProblem(const Dune::ParameterTree& config, ConvexProblemType& problem) :
            problem(problem)
        {
            bisection = Bisection(0.0,
                    config.get("smoother.gs_acceptance", 1.0),
                    config.get("smoother.gs_tol", 1e-15),
                    config.get("smoother.safety", 1e-14));
            localGSLoops_ = config.get("smoother.local_gs_loops", 1);
        };


        IterateObject getIterateObject()
        {
            return IterateObject(bisection, problem, localGSLoops_);
        }


    private:
        // problem data
        ConvexProblemType& problem;


    protected:
        // commonly used minimization stuff
        Bisection bisection;
        int localGSLoops_;
};



template <class ConvexProblemTypeTEMPLATE>
class SimplexConstrainedGSProblem<ConvexProblemTypeTEMPLATE>::IterateObject
{
    friend class SimplexConstrainedGSProblem;


    protected:
        IterateObject(const Bisection& bisection, ConvexProblemType& problem, int localGSLoops) :
            problem(problem),
            local_J(problem.phi),
            bisection(bisection),
            localGSLoops_(localGSLoops)
        {};


    public:
        void setIterate(VectorType& u)
        {
            // TODO How should the rank-1 term be handled ??????????????????????
            // s = problem.Am*u;
            problem.phi.setVector(u);

            this->u = u;
            return;
        };


        void updateIterate(const LocalVectorType& ui, int i)
        {
            // TODO How should the rank-1 term be handled ??????????????????????
            // s += (ui-u[i]) * problem.Am[i];

            for(int j=0; j<block_size; ++j)
                problem.phi.updateEntry(i, ui[j], j);

            u[i] = ui;
            return;
        };


        void solveLocalProblem(LocalVectorType& ui, int i, typename Dune::BitSetVector<block_size>::const_reference ignore)
        {
            LocalMatrixType Aii;
            Aii = 0.0;
            LocalVectorType b(0.0);

            // compute residual for minimization in iterates
            typename MatrixType::row_type::ConstIterator it = problem.A[i].begin();
            typename MatrixType::row_type::ConstIterator end = problem.A[i].end();
            for(; it !=end; ++it)
            {
                int col = it.index();
                if (col == i)
                    Aii = (*it);
                else
                    it->mmv(u[col], b);
            }
            b *= problem.a;
            b += problem.f[i];

            // since we use simplex edges as search directions we need the residual for corrections
            Aii.usmv(-problem.a, u[i], b);

            // TODO How should the rank-1 term be handled ??????????????????????

            LocalVectorType ui_old = u[i];
            ui = u[i];
            local_J.i = i;
            double alpha = 0.0;
            for(int l=0; l<localGSLoops_; ++l)
            {
                for(int j1=0; j1<(block_size-1); ++j1)
                {
                    if(ignore.test(j1))
                        continue;

                    for(int j2=j1+1; j2<block_size; ++j2)
                    {
                        if(ignore.test(j2))
                            continue;

                        // set edge components
                        local_J.j1 = j1;
                        local_J.j2 = j2;

                        // compute matrix entry for edge direction
                        local_J.A = StaticMatrix::simplexEdgeDiagonal(Aii, j1, j2) * problem.a;
                        local_J.b = b[j1] - b[j2];
                        local_J.ui[j1] = ui[j1];
                        local_J.ui[j2] = ui[j2];

                        // compute minimizer for correction
                        // Use bisection if quadratic minimization fails
                        alpha = local_J.domain().projectIn(local_J.b/local_J.A);
                        if (not local_J.subDifferential(alpha).containsZero(1e-14))
                          alpha = bisection.minimize(local_J, 0, 0, bisectionsteps);

                        // apply correction
                        ui[j1] += alpha;
                        ui[j2] -= alpha;

                        // update the local residual for corrections
                        b[j1] -= StaticMatrix::simplexEdgeResidual(Aii, j1, j2) * alpha * problem.a;
                        b[j2] -= -StaticMatrix::simplexEdgeResidual(Aii, j2, j1) * alpha * problem.a;

                        // update the intermediate local values ...
                        problem.phi.updateEntry(i, ui[j1], j1);
                        problem.phi.updateEntry(i, ui[j2], j2);
                    }
                }
            }
            // ... and to restore the old ones after computation
            updateIterate(ui_old, i);
            u[i] = ui_old;

//            local_J.A += problem.am*problem.Am[i]*problem.Am[i];
//            local_J.b = problem.f[i] + problem.a*local_J.b;
//            local_J.b -= (s-u[i]*problem.Am[i])*problem.am*problem.Am[i];

//            ui = bisection.minimize<OneDConvexFunction>(local_J, u[i], u[i], bisectionsteps);
            return;
        };


    private:
        // problem data
        ConvexProblemType& problem;

        // commonly used minimization stuff
        Bisection bisection;
        SimplexEdgeConvexFunction<NonlinearityType> local_J;

        // state data for smoothing procedure used by:
        // setIterate, updateIterate, solveLocalProblem
        VectorType u;
        int bisectionsteps;
        int localGSLoops_;
};

#endif

